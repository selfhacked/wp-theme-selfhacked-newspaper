jQuery(function($) {
    var post_id = $('#submit-dislike').attr('data-value');
    var container = $('.dislike-container');

    $('#rating_'+post_id+'_1').click(function () { container.css('display','block') });
    $('#rating_'+post_id+'_2').click(function () { container.css('display','block') });
    $('#rating_'+post_id+'_3').click(function () { container.css('display','block') });

    $('#other-checkbox').change(function () {
        if ($('#reason-input').hasClass('hidden')) {
            $('#reason-input').removeClass('hidden');
        } else {
            $('#reason-input').addClass('hidden');
        }
    });

    $('#dislike-close').click(function () {
        container.css('display','none')
    });

    $('#submit-dislike').click(function (e) {
        e.preventDefault();
        container.css('display','none')

        var reasons = [];
        reasons[0] = 0;
        reasons[1] = 0;
        reasons[2] = 0;
        reasons[3] = 0;
        reasons[4] = 0;
        reasons[5] = 0;
        reasons[6] = 0;
        reasons[7] = 0;
        reasons[8] = '';

        $.each($('#dislike-form').serializeArray(), function ( index, object ) {
            if ( object.name === '8' && object.value === 'on' ) {
                object.value = $('#reason-input').val();
            } else if (object.value === 'on') {
                object.value = 1;
            }
            reasons[object.name] = object.value;
        })

        $.ajax({
            url: common_var.ajax_url,
            type: 'post',
            data: {
                action: 'dislike_reason',
                post_id: post_id,
                reason_0: reasons[0],
                reason_1: reasons[1],
                reason_2: reasons[2],
                reason_3: reasons[3],
                reason_4: reasons[4],
                reason_5: reasons[5],
                reason_6: reasons[6],
                reason_7: reasons[7],
                other_reason: reasons[8]
            },

            success: function ( r ) {
                r = JSON.parse( r );
            },
            error: function (e) {
                console.error(e);
            }
        });
    })

});