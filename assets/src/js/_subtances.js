(function ($) {
    $(document).ready(() => {

        var filter = {
            target: '',
            search_string: '',
            active_section: '',
            active_categories: [],
            default_categories: $('[data-name="default"]').data('value'),
            reset: function() {
                this.search_string = '';
                this.active_section = '';
                this.active_categories = this.default_categories;
                $('[data-name="search"]').val(this.search_string);
                this.section();
                this.module();
            },
            module: function() {
                var search_string = this.search_string;
                var active_categories = this.active_categories;

                $('[data-name="search"]').data('value', active_categories);

                $('[data-name="module"]').each( function () {
                    var has_cat = false;
                    for ( var index in active_categories ) {
                        var category = parseInt(active_categories[index]);
                        var categories = $(this).data('categories');
                        var check = jQuery.inArray( category, categories );
                        if ( check !== -1 ) {
                            has_cat = true;
                            break;
                        }
                    }

                    if ( has_cat ) {
                        if ( search_string !== '' ) {
                            var search = new RegExp(search_string, 'i');
                            if ( search.test( $(this).find('a').text() ) ) {
                                $(this).removeClass('hidden')
                            } else {
                                $(this).addClass('hidden');
                            }

                        } else {
                            $(this).removeClass('hidden')
                        }
                    } else {
                        $(this).addClass('hidden');
                    }
                });

                if ( this.active_section === '' ) {
                    $('div[data-name="section"]').each(function() {
                        filter.letters($(this));
                    });
                }
            },
            section: function() {
                var active_section = this.active_section;
                if ( active_section === '' ) {
                    $('[data-name="section"]').each( function() {
                        filter.letters($(this));
                    });
                } else {
                    $('[data-name="section"]').each( function() {
                        var section = $(this).data('section');
                        if ( active_section.toString() === section.toString() ) {
                            if ( $(this).is(':hidden') ) {
                                $(this).slideDown();
                            }
                        } else {
                            if ( $(this).is(':visible') ) {
                                $(this).slideUp();
                            }
                        }
                    });
                }
            },
            end: function() {
                if ( this.active_categories !== this.default_categories || this.active_section !== ''|| this.search_string !== '' ) {
                    $('[data-target="reset"]').show();
                } else {
                    $('[data-target="reset"]').hide();
                }
            },
            borders: function() {
                $('[data-name="section"]:visible').each(function () {
                    var child_modules = $(this).find('[data-name="module"]:visible');
                    child_modules.each(function (i) {
                        var test = (i + 1) % 4;
                        if ( test === 0 ) {
                            $(this).addClass('no-border');
                        } else {
                            if ( child_modules.length === (i + 1) ) {
                                $(this).addClass('no-border');
                            } else {
                                $(this).removeClass('no-border');
                            }
                        }
                    });
                });
            },
            letters: function( module ) {
                var parent = $('[data-value="' + module.data('section') + '"]');
                var sub_modules = module.find('[data-name="module"]');
                var hidden_modules = sub_modules.filter('.hidden');
                if ( sub_modules.length === hidden_modules.length ) {
                    if ( module.is(':visible') ) {
                        module.slideUp();
                    }
                    parent.removeClass('letter-filter-active');
                } else {
                    if ( module.is(':hidden') ) {
                        module.slideDown();
                    }
                    parent.addClass('letter-filter-active');
                }
            }
        };

        $(document).on('click', '[data-target]', function () {
            var target = $(this).data('target');
            var value = $(this).data('value');

            if ( target === 'module' ) {
                if ( filter.active_categories === value ) {
                    if ( $(this).data('parent') ) {
                        filter.active_categories = $('[data-name="' + $(this).data('parent') + '"]').data('value');
                    } else {
                        filter.active_categories = filter.default_categories;
                    }
                } else {
                    filter.active_categories = value;
                }
                filter.module();
            }

            if ( target === 'section' ) {
                if ( $(this).hasClass('letter-filter-active') ) {
                    if ( filter.active_section === value ) {
                        filter.active_section = '';
                        $(this).removeClass('active');
                    } else {
                        filter.active_section = value;
                        $(this).siblings().removeClass('active');
                        $(this).addClass('active');
                    }
                    filter.section();
                } else {
                    if ( $(this).hasClass('active') ) {
                        filter.active_section = '';
                        $(this).removeClass('active');
                        filter.section();
                    }
                }
            }

            if ( target === 'reset' ) {
                $('.letter-filters .active').removeClass('active');
                filter.reset();
            }

            filter.borders();
            filter.end();
        });

        $(document).on('click', '[data-toggle]', function () {
            var is_active = $(this).hasClass('active');
            var toggle = $(this).data('toggle');
            var target = $('[data-name="' + toggle + '"]');

            if ( is_active ) {
                if ( toggle !== 'none' ) {
                    if ( !$(this).data('value') || filter.active_categories !== $(this).data('value') ) {
                        if ( target.is(':visible') ) {
                            target.slideUp();
                        }
                        $(this).removeClass('active');
                    }
                    target.find('.active').removeClass('active');
                    if (target.data('linked')) {
                        var linked = target.data('linked');
                        for (var index in linked) {
                            var sub_target = $('[data-name="' + linked[index] + '"]');
                            if ( sub_target.is(':visible') ) {
                                sub_target.slideUp();
                            }
                            sub_target.find('.active').removeClass('active');
                        }
                    }
                } else {
                    $(this).removeClass('active');
                }
            } else {
                if ( target.is(':hidden') ) {
                    target.slideDown();
                }
                $(this).addClass('active');
            }


            var target_group = $(this).data('toggle-group');
            $('[data-toggle-group="' + target_group + '"]').not(this).each(function() {
                if ( $(this).hasClass('active') ) {
                    var other_toggle = $(this).data('toggle');
                    var other_target = $('[data-name="' + other_toggle + '"]');
                    if ( other_target.is(':visible') ) {
                        other_target.slideUp();
                    }
                    other_target.find('.active').removeClass('active');
                    $(this).removeClass('active');
                }
            });

            if ( $(this).data('icon') && $(this).data('icon-active') ) {
                var icon = 'fa-' + $(this).data('icon');
                var icon_active = 'fa-' + $(this).data('icon-active');
                if ( $(this).hasClass('active') ) {
                    $(this).find('.' + icon).removeClass(icon).addClass(icon_active);
                } else {
                    $(this).find('.' + icon_active).removeClass(icon_active).addClass(icon);
                }
            }
        });

        var wto;

        $('[data-name="search"]').on('keydown', function ( event ) {
            var search = $(this);
            clearTimeout(wto);
            wto = setTimeout(function () {
                filter.active_categories = search.data('value');
                filter.search_string = search.val();
                filter.module();
                filter.borders();
                filter.end();
            }, 500);
        });
    });
})(jQuery);
