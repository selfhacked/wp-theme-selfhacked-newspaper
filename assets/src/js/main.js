(function ($) {
  $(document).ready(() => {
    // All extra js should go here

    $('.subscribe-form-shortcode .subscribe-form__form-item').on('submit', function (event) {
      submitSubscribeForm(this);
      event.preventDefault();
    });
  });

})(jQuery);
