(function ($) {
  function delayUntilStopTyping(callback, ms) {
    let timer = 0;

    return function () {
      let context = this, args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        callback.apply(context, args);
      }, ms || 0);
    };
  }

  $(document).ready(() => {
    const $tdHeaderSearch = $('#td-header-search');
    const $tdHeaderSearchMob = $('#td-header-search-mob');

    function processKeydown(event) {
      const $thisEl = $tdHeaderSearch;
      if (
        (event.which && 39 === event.which) ||
        (event.keyCode && 39 === event.keyCode) ||
        (event.which && 37 === event.which) ||
        (event.keyCode && 37 === event.keyCode)) {
        //do nothing on left and right arrows
        tdAjaxSearch.td_aj_search_input_focus();
        return;
      }

      if ((event.which && 13 === event.which) || (event.keyCode && 13 === event.keyCode)) {
        // on enter
        let td_aj_cur_element = $('.td-aj-cur-element');
        if (td_aj_cur_element.length > 0) {
          window.location = td_aj_cur_element.find('.entry-title a').attr('href');
        } else {
          $thisEl.parent().parent().submit();
        }
        return false; //redirect for search on enter

      } else if ((event.which && 40 === event.which) || (event.keyCode && 40 === event.keyCode)) {
        // down
        tdAjaxSearch.move_prompt_down();
        return false; //disable the envent

      } else if ((event.which && 38 === event.which) || (event.keyCode && 38 === event.keyCode)) {
        //up
        tdAjaxSearch.move_prompt_up();
        return false; //disable the envent

      } else {
        //for backspace we have to check if the search query is empty and if so, clear the list
        if ((event.which && 8 === event.which) || (event.keyCode && 8 === event.keyCode)) {
          //if we have just one character left, that means it will be deleted now and we also have to clear the search results list
          let search_query = $thisEl.val();
          if (1 === search_query.length) {
            $('#td-aj-search').empty();
          }
        }

        //various keys
        tdAjaxSearch.td_aj_search_input_focus();

        tdAjaxSearch.do_ajax_call();
      }

      return true;
    }

    function processKeydownMob(event) {
      const $thisEl = $tdHeaderSearchMob;

      if ( ( event.which && 13 === event.which ) || ( event.keyCode && 13 === event.keyCode ) ) {
        // on enter
        var td_aj_cur_element = $('.td-aj-cur-element');
        if (td_aj_cur_element.length > 0) {
          window.location = td_aj_cur_element.find( '.entry-title a' ).attr( 'href' );
        } else {
          $thisEl.parent().parent().submit();
        }
        return false; //redirect for search on enter
      } else {

        //for backspace we have to check if the search query is empty and if so, clear the list
        if ( ( event.which && 8 === event.which ) || ( event.keyCode && 8 === event.keyCode ) ) {
          //if we have just one character left, that means it will be deleted now and we also have to clear the search results list
          var search_query = $thisEl.val();
          if ( 1 === search_query.length ) {
            $('#td-aj-search-mob').empty();
          }
        }

        tdAjaxSearch.do_ajax_call_mob();

        return true;
      }
    }

    // Override keydown event and fire action after stop typing for 300ms for desktop and 400ms for mobile
    $tdHeaderSearch.off('keydown').keydown(delayUntilStopTyping(function (event) {
      processKeydown(event);
    }, 400));

    $tdHeaderSearchMob.off('keydown').keydown(delayUntilStopTyping(function (event) {
      processKeydownMob(event);
    }, 500));

    // Override the clicking event on search icon
    // show and hide the drop down on the search icon
    jQuery( '#td-header-search-button' ).off('click').click(function(event){
      jQuery( 'body' ).addClass( 'td-search-opened' );

      var search_input = jQuery('#td-header-search-mob');

      /**
       * Note: the autofocus does not work for iOS and windows phone devices as it's considered bad user experience
       */
      //autofocus
      setTimeout(function(){
        search_input.focus();
      }, 1300);

      var current_query = search_input.val().trim();
      if ( current_query.length > 0 ) {
        tdAjaxSearch.do_ajax_call_mob();
      }
    });
  });
})(jQuery);
