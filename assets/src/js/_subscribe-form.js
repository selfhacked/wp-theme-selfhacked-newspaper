let subscribeForms = document.querySelectorAll('.subscribe-form');

function submitSubscribeForm(form) {
  let formData = new FormData(form);
  let ajax_url = common_var.ajax_url;

  let parsedData = {};
  for (let name of formData) {
    if ("undefined" === typeof(parsedData[name[0]])) {
      let tempdata = formData.getAll(name[0]);
      if (tempdata.length > 1) {
        parsedData[name[0]] = tempdata;
      } else {
        parsedData[name[0]] = tempdata[0];
      }
    }
  }

  parsedData['action'] = 'ac_subscribe';

  let bodyData = [];

  for (let key in parsedData) {
    if (parsedData.hasOwnProperty(key)) {
      bodyData.push(key + '=' + encodeURIComponent(parsedData[key]));
    }
  }

  let options = {};
  switch (form.method.toLowerCase()) {
    case 'post':
      options.method = 'post';
      options.body = bodyData.join('&');
      options.headers = {
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
      };
      break;
    default:
      options.method = 'get';
      options.headers = {'Content-Type': 'application/json'};
      break;
  }

  if (subscribeForms) {
    subscribeForms.forEach((currentItem, currentIndex, listObj) => {
      currentItem.classList.toggle('loading');
    });
  }

  fetch(ajax_url, options).then((resp) => {
    return resp.json();
  }).then((data) => {
    if (data.status === "success") {
      if (subscribeForms) {
        subscribeForms.forEach((currentItem, currentIndex, listObj) => {
          currentItem.classList.toggle('loading');
          currentItem.classList.add('success');
        });
      }
    }
  }).catch((error) => {
    console.log(error);
  });
}
