<?php

isset( $logo_id ) || ( $logo_id = 0 );
isset( $title ) || ( $title = '' );
isset( $description ) || ( $description = '' );
isset( $post_link ) || ( $post_link = '' );
isset( $post_title ) || ( $post_title = '' );
isset( $comment ) || ( $comment = '' );
isset( $reply_link ) || ( $reply_link = '' );
isset( $reply_text ) || ( $reply_text = '' );
isset( $unsubscribe_header ) || ( $unsubscribe_header = '' );
isset( $unsubscribe_link ) || ( $unsubscribe_link = '' );
isset( $unsubscribe_text ) || ( $unsubscribe_text = '' );

$image     = wp_get_attachment_metadata( $logo_id );
$format    = '<img src="%1$s" width="%2$d" height="%3$d" style="display: block; width: %2$dpx; height: %3$dpx;" />';
$upload    = wp_upload_dir();
$image_url = $upload['baseurl'] . '/' . $image['file'];
$img_html  = sprintf( $format, $image_url, $image['width'], $image['height'] );

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Demystifying Email Design</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="background-color: #FBF6FE; margin: 0; padding: 0;">
<table align="center" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
    <tr>
        <td align="center" style="padding-top: 50px;">
			<?php if ( $logo_id ) { echo $img_html; } ?>
        </td>
    </tr>
    <tr>
        <td>
            <div style="background-color: #fff; border-radius: 10px; overflow: hidden; padding: 40px 66px; margin: 50px 0;">
                <h2 style="display: block; margin: 0; color: #674987; font-size: 26px; font-weight: bold;">
					<?php echo $title; ?>
                </h2>
                <span style="display: block; margin-top: 25px; margin-bottom: 10px; color: #4C4653; font-size: 14px;">
			        <?php echo $description; ?>
		        </span>
                <span style="display: block; margin: 15px 0;">
			        <a href="<?php echo $post_link; ?>" style="font-size: 16px; font-weight: bold;">
				        <?php echo $post_title; ?>
			        </a>
		        </span>
                <div style="background-color: #FBF6FE; padding: 25px; border-radius: 5px; border-left: 5px #674987 solid; margin: 15px 0; overflow: hidden;">
			        <span style="color: #4C4653; font-size: 14px;">
				        <?php echo $comment; ?>
			        </span>
                </div>
                <a href="<?php echo $reply_link; ?>" style="padding: 15px 0; font-size: 14px; text-decoration: none;">
					<?php echo $reply_text; ?>
                </a>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <span style="display: block; color: #7C7287; font-size: 12px; margin: 10px 0;">
		        <?php echo $unsubscribe_header; ?>
	        </span>
            <a href="<?php echo $unsubscribe_link; ?>"
               style="display: block; text-transform: uppercase; font-size: 12px;">
				<?php echo $unsubscribe_text; ?>
            </a>
        </td>
    </tr>
</table>
</body>
</html>