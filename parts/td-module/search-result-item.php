<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 11/12/18
 * Time: 12:16 PM
 */

use Enpii\Wp\EnpiiBase\Wp;

isset( $td_module ) || ( $td_module = null );
isset( $post ) || ( $post = null );

/* @var td_module_mx2 $td_module */
/* @var WP_Post $post */
if ( ! $td_module || ! $post ) {
	return null;
}

$title_length = $td_module->get_shortcode_att('m16_tl');
$excerpt_length = $td_module->get_shortcode_att('m16_el');
?>

<div class="<?php echo $td_module->get_module_classes();?>">
	<?php echo $td_module->get_image('thumbnail');?>

	<div class="item-details search-result">
		<?php
		$buffy = '';
		$buffy .= '<h3 class="entry-title td-module-title">';
		$buffy .='<a href="' . $td_module->href . '" rel="bookmark" title="' . $td_module->title_attribute . '">'.Wp::esc_editor_field($post->post_title).'</a>';
		$buffy .= '</h3>';
		echo $buffy;
		?>

		<div class="td-module-meta-info">
			<?php if (td_util::get_option('tds_category_module_16') == 'yes') { echo $td_module->get_category(); }?>
			<?php echo $td_module->get_author();?>
			<?php echo $td_module->get_date();?>
			<?php echo $td_module->get_comments();?>
		</div>

		<div class="search-result-with-highlight-keywords">
			<?php echo $td_module->get_excerpt($excerpt_length);?>
		</div>
	</div>

</div>
