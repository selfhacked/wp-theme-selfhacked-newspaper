<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 12/5/18
 * Time: 12:24 PM
 */

isset( $text_domain ) || ( $text_domain = null );
isset( $post ) || ( $post = null );
isset( $css_class ) || ( $css_class = null );
isset( $post_title ) || ( $post_title = null );
?>
<div class="<?php echo $css_class; ?> mega-menu-post-item">
	<div class="mega-menu-post-item__details">
		<?php echo $post_title; ?>
	</div>
</div>
