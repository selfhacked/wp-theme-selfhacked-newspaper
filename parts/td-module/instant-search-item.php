<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 11/9/18
 * Time: 3:57 PM
 */

use Enpii\Wp\EnpiiBase\Wp;

isset( $td_module ) || ( $td_module = null );
isset( $post ) || ( $post = null );

/* @var td_module_mx2 $td_module */
/* @var WP_Post $post */
if ( ! $td_module || ! $post ) {
	return null;
}

$title_length = $td_module->get_shortcode_att( 'mx2_tl' );
?>

<div class="<?php echo $td_module->get_module_classes(); ?>">

	<?php echo $td_module->get_image( 'td_80x60' ); ?>

	<div class="item-details search-result">
		<?php
		$buffy = '';
		$buffy .= '<h3 class="entry-title td-module-title">';
		$buffy .='<a href="' . $td_module->href . '" rel="bookmark" title="' . $td_module->title_attribute . '">'.Wp::esc_editor_field($post->post_title).'</a>';
		$buffy .= '</h3>';
		echo $buffy;
		?>
		<div class="editor-content search-result-with-highlight-keywords">
			<?php echo $td_module->get_excerpt(); ?>
		</div>
		<div class="td-module-meta-info">
			<?php echo $td_module->get_category(); ?>
			<?php echo $td_module->get_date(); ?>
		</div>
	</div>

</div>
