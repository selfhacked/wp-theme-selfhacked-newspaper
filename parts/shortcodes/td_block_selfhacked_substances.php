<?php

isset( $text_domain ) || ( $text_domain = '' );
isset( $block_js ) || ( $block_js = '' );
isset( $block_css ) || ( $block_css = '' );
isset( $block_class ) || ( $block_class = '' );
isset( $block_uid ) || ( $block_uid = '' );
isset( $block_inner ) || ( $block_inner = '' );
isset( $data_store ) || ( $data_store = [] );
isset( $atts ) || ( $atts = [] );

$category_filter_title   = __( 'Filter by category', $text_domain );
$search_placeholder_text = __( 'Search substances', $text_domain );

$all_cat_1         = array_merge( $atts['cat_1_children'], [ $atts['cat_1'] ] );
$all_cat_2         = array_merge( $atts['cat_2_children'], [ $atts['cat_2'] ] );
$cat_1_data_value  = SelfHacked\SelfHackedNewspaper\Util::build_data_attr( 'value', $all_cat_1 );
$cat_2_data_value  = SelfHacked\SelfHackedNewspaper\Util::build_data_attr( 'value', $all_cat_2 );
$default_cat_value = SelfHacked\SelfHackedNewspaper\Util::build_data_attr( 'value', array_merge( $all_cat_1, $all_cat_2 ) );
$data_linked       = SelfHacked\SelfHackedNewspaper\Util::build_data_attr( 'linked', array_merge( [ 'sub-category-1' ], [ 'sub-category-2' ] ) );

function generate_sub_cat_button( $cat, $parent ) {
	$data_value = SelfHacked\SelfHackedNewspaper\Util::build_data_attr( 'value', [ $cat ] );
	echo '<div class="td-block-span3">';
	echo '<button class="subcategory-filter" data-toggle="none" data-toggle-group="subcategories" data-target="module" data-parent="' . $parent . '" ' . $data_value . '>' . get_cat_name( $cat ) . '</button>';
	echo '</div>';
}

echo $block_js;
echo $block_css;
?>
<div class="<?php echo $block_class; ?>">
    <div class="td-block-row substance-filters" data-name="default" <?php echo $default_cat_value; ?>>
        <div class="td-block-span12">
            <div class="category-filters">
                <h3 class="category-filter-header" data-toggle="category-filters" data-icon="plus" data-icon-active="minus">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    <?php echo $category_filter_title; ?>
                    <span class="reset-filter" data-target="reset"><?php _e('Clear filter', $text_domain ); ?></span>
                </h3>
                <div class="category-filter-wrapper" data-name="category-filters" <?php echo $data_linked; ?>>
                    <div class="td-block-span6">
                        <button class="category-filter" data-name="cat_1" data-toggle="sub-category-1" data-toggle-group="categories"
                                data-target="module" <?php echo $cat_1_data_value; ?>><?php echo get_cat_name( $atts['cat_1'] ); ?></button>
                    </div>
                    <div class="td-block-span6">
                        <button class="category-filter" data-name="cat_2" data-toggle="sub-category-2" data-toggle-group="categories"
                                data-target="module" <?php echo $cat_2_data_value; ?>><?php echo get_cat_name( $atts['cat_2'] ); ?></button>
                    </div>
                </div>
                <div class="subcategory-wrapper" data-name="sub-category-1">
                    <?php foreach ( $atts['cat_1_children'] as $cat ):
                        generate_sub_cat_button( $cat, 'cat_1' );
                    endforeach; ?>
                </div>
                <div class="subcategory-wrapper" data-name="sub-category-2">
                    <?php foreach ( $atts['cat_2_children'] as $cat ):
                        generate_sub_cat_button( $cat, 'cat_2' );
                    endforeach; ?>
                </div>
            </div>
        </div>
        <div class="td-block-span8 letter-filters">
            <?php

            foreach ( $data_store as $k => $v ) {
                if ( ! empty( $v ) ) {
                    echo '<button id="letter-filter" class="letter-filter letter-filter-active" data-target="section" data-value="' . $k . '">' . $k . '</button>';
                } else {
                    echo '<button class="letter-filter">' . $k . '</button>';
                }
            }

            ?>
        </div>

        <div class="td-block-span4 search-filter">
            <input type="search" class="search-field substance-search-field"
                   data-name="search" <?php echo $default_cat_value; ?>
                   placeholder="<?php echo $search_placeholder_text; ?>" value="" name="s"/>
            <i class="fa fa-search" aria-hidden="true"></i>
        </div>

    </div>
    <div id="<?php echo $block_uid; ?>" class="td_block_inner">
        <?php echo $block_inner; ?>
    </div>
</div>
