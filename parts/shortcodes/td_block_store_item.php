<?php

isset( $block_css ) || ( $block_css = '' );
isset( $block_classes ) || ( $block_classes = '' );
isset( $button_link ) || ( $button_link = '' );
isset( $button_link_target ) || ( $button_link_target = '' );
isset( $button_text ) || ( $button_text = '' );
isset( $image_url ) || ( $image_url = '' );
isset( $title ) || ( $title = '' );
isset( $price ) || ( $price = '' );
isset( $description ) || ( $description = '' );

echo $block_css;
?>
<div class="<?php echo $block_classes; ?>">
	<div class="content-wrapper">
		<div class="left-wrapper">
			<img src="<?php echo $image_url; ?>">
		</div>
		<div class="details-wrapper">
			<div class="center-wrapper">
				<h3 class="title"><?php echo $title; ?></h3>
				<div class="description"><?php echo $description; ?></div>
			</div>
			<div class="right-wrapper">
				<div class="price"><?php echo $price; ?></div>
				<a href="<?php echo $button_link; ?>" <?php echo ($button_link_target ? 'target="' . esc_attr( $button_link_target ) . '"' : '') ?>><button class="button"><?php echo $button_text; ?></button></a>
			</div>
		</div>
	</div>
</div>
