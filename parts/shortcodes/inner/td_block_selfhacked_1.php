<?php

isset( $posts ) || ( $posts = [] );
isset( $image_link ) || ( $image_link = '' );
isset( $image_url ) || ( $image_url );

$post_count = 0;
?>
<div class="td-block-row">
    <div class="td-block-span6">
        <a href="<?php echo $image_link; ?>"><img src="<?php echo $image_url; ?>"></a>
    </div>
	<?php
    foreach ( (array) $posts as $post ):
        if ( $post_count == 0 || $post_count == 1 ):
			$selfhacked_module = new td_module_selfhacked_1( $post ); ?>
            <div class="td-block-span6">
                <?php echo $selfhacked_module->render(); ?>
            </div>
        <?php
		else:
            $selfhacked_module = new td_module_selfhacked_2( $post );?>
            <div class="td-block-span4">
                <?php echo $selfhacked_module->render(); ?>
            </div>
        <?php
        endif;
        $post_count++;
        endforeach;
        ?>
</div>
