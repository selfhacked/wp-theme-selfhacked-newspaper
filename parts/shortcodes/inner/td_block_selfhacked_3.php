<?php

isset( $posts ) || ( $posts = [] );

?>
<div class="td-block-row">
	<?php foreach ( (array) $posts as $post ): $selfhacked_module = new td_module_selfhacked_2( $post ); ?>
		<div class="td-block-span4">
			<?php echo $selfhacked_module->render(); ?>
		</div>
	<?php endforeach; ?>
</div>