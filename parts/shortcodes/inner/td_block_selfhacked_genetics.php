<?php

isset( $text_domain ) || ( $text_domain = '' );
isset( $posts ) || ( $posts = [] );

?>
<div class="td-block-row">
	<?php foreach ( (array) $posts as $post ): $selfhacked_module = new td_module_genetics( $post ); ?>
	<div class="td-block-span6">
		<?php echo $selfhacked_module->render(); ?>
	</div>
	<?php endforeach; ?>
</div>
