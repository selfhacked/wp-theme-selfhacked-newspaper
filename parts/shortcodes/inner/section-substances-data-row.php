<?php

use Enpii\Wp\EnpiiBase\Wp;

isset( $data_store ) || ( $data_store = [] );
?>

        <?php foreach ( $data_store as $k => $v ) : ?>
            <?php if ( ! empty( $v ) ) : ?>
                <div data-name="section" data-section="<?php echo $k; ?>">
                    <div class="td-block-row">
                        <div class="td-block-span12 title-section">
                            <h3 class="td-section-title"><span><?php echo $k; ?></span></h3>
                        </div>
                        <?php echo Wp::get_template_part( 'parts/shortcodes/inner/section-substances-data-item', [ 'posts'  => $v ] ); ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>
