<?php

isset( $posts ) || ( $posts = [] );

$i = 0;

/** @var WP_Post $post */
foreach ( $posts as $post ):
	$selfhacked_module = new td_module_substances( $post );
	$classes           = 'td-block-span3';

	if ( $i == 3 ) {
		$i = 0;
		$classes .= ' no-border';
	} else {
		$i++;
	}

	$data_categories  = SelfHacked\SelfHackedNewspaper\Util::build_data_attr( 'categories', wp_get_post_categories( $post->ID ) );
	?>
	<div class="<?php echo $classes; ?>" data-name="module" <?php echo $data_categories; ?>>
	    <?php echo $selfhacked_module->render(); ?>
	</div>
	<?php
endforeach;