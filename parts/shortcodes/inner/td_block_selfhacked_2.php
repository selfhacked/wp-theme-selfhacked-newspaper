<?php

isset( $posts ) || ( $posts = [] );

$post_count = 0;
?>
<div class="td-block-row">
    <?php
    foreach ( (array) $posts as $post ):
		if ( $post_count == 0 ):
			$selfhacked_module = new td_module_selfhacked_3( $post ); ?>
			<div class="td-block-span6">
				<?php echo $selfhacked_module->render(); ?>
			</div>
		<?php
        elseif ( $post_count == 1 || $post_count == 2 ):
            $selfhacked_module = new td_module_selfhacked_1( $post ); ?>
			<div class="td-block-span6">
				<?php echo $selfhacked_module->render(); ?>
			</div>
		<?php
        else:
			$selfhacked_module = new td_module_selfhacked_2( $post ); ?>
			<div class="td-block-span4">
				<?php echo $selfhacked_module->render(); ?>
			</div>
		<?php
        endif;
        $post_count++;
		endforeach; ?>
</div>
