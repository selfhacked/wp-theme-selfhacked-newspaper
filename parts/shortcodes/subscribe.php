<?php

isset( $text_domain ) || ( $text_domain = null );

$title = __( 'Join Self Hacked today and start the journey of improving your life', $text_domain );

$shortcode = \SelfHacked\SelfHackedNewspaper\Util::generate_shortcode(
	'vc_row',
	[
		'full_width' => 'stretch_row_1200 td-stretch-content',
		'el_class'   => 'subscribe-cta-row',
		'tdc_css'    => [
			'all' =>
				[
					'background-color' => '#7c7287',
					'display'          => '',
				],
		]
	],
	\SelfHacked\SelfHackedNewspaper\Util::generate_shortcode(
		'vc_column',
		[
			'width' => '5',
		],
		\SelfHacked\SelfHackedNewspaper\Util::generate_shortcode(
			'tdm_block_inline_text',
			[
				'description'         => $title,
				'display_inline'      => 'no',
				'f_descr_font_size'   => '24px',
				'f_descr_font_weight' => '600',
				'f_descr_font_family' => '702',
				'description_color'   => '#ffffff',
				'tdc_css'             => [
					'all' =>
						[
							'padding-top' => '40',
							'display'     => '',
						],
				]
			]
		)
	)
	. \SelfHacked\SelfHackedNewspaper\Util::generate_shortcode(
		'vc_column',
		[
			'width' => '7',
			'tdc_css'             => [
				'all' =>
					[
						'padding-top' => '40',
						'display'     => '',
					],
			]
		],
		\SelfHacked\SelfHackedNewspaper\Util::generate_shortcode( 'selfhacked_subscribe_form' )
	)
);

echo do_shortcode( $shortcode );