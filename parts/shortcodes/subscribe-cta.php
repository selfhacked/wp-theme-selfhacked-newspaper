<?php

isset( $text_domain ) || ( $text_domain = null );

$bg_img_url = 'https://stg2010.selfhacked.com/app/uploads/2018/12/bg.jpg';
$title = __( 'Cutting-Edge Solid Science-Based Information for Health Hackers', $text_domain );
$list_icon = '<i class="fa fa-check-square-o" aria-hidden="true"></i>';
$item_1 = $list_icon . __( 'The SelfHacked Lectin Avoidance Diet Shopping List', $text_domain );
$item_2 = $list_icon . __( 'Top Genes & Lab Tests to Watch Out For to Optimize Your Health & Premium Newsletter', $text_domain );
$item_3 = $list_icon . __( 'The SelfHacked Stress Management Guide', $text_domain );
$consent = __( 'Yes, sign me up for marketing emails from SelfHacked. For more information on how we use your information check out our Privacy Policy. You can change your mind anytime by unsubscribing.', $text_domain );

$shortcode = \SelfHacked\SelfHackedNewspaper\Util::generate_shortcode(
	'vc_row',
	[
		'full_width' => 'stretch_row_1200 td-stretch-content',
		'el_class'   => 'subscribe-row',
		'tdc_css'    => [
			'all' =>
				[
					'background-color'    => '#f5f1fa',
					'background-image'    => 'url("' . $bg_img_url . '")',
					'background-style'    => 'contain',
					'background-position' => 'right center',
					'display'             => '',
				],
		]
	],
	\SelfHacked\SelfHackedNewspaper\Util::generate_shortcode(
		'vc_column',
		[
			'tdc_css' => [
				'all' =>
					[
						'padding-top'    => 82,
						'padding-bottom' => 64,
						'display'        => '',
					],
			]
		],
		\SelfHacked\SelfHackedNewspaper\Util::generate_shortcode(
			'tdm_block_inline_text',
			[
				'description' => $title,
				'display_inline' => 'no',
				'f_descr_font_size' => '36px',
				'f_descr_font_weight' => 700,
				'f_descr_font_family' => 702,
				'description_color' => '#00b0c0',
				'f_descr_font_line_height' => '50px',
				'tdc_css' => [
					'all' =>
						[
							'padding-bottom' => 88,
							'width' => '70%',
							'display' => '',
						],
				]
			]
		)
		. \SelfHacked\SelfHackedNewspaper\Util::generate_shortcode(
			'tdm_block_inline_text',
			[
				'description' => $item_1,
				'description_color' => '#7c7287',
				'f_descr_font_size' => '18px',
				'f_descr_font_family' => '702',
				'f_descr_font_weight' => '500',
				'el_class' => 'subscribe-hero-details',
				'tdc_css' => 'eyJhbGwiOnsicGFkZGluZy1ib3R0b20iOiIxMCIsImRpc3BsYXkiOiIifX0='
			]
		)
		. \SelfHacked\SelfHackedNewspaper\Util::generate_shortcode(
			'tdm_block_inline_text',
			[
				'description' => $item_2,
				'description_color' => '#7c7287',
				'f_descr_font_size' => '18px',
				'f_descr_font_family' => '702',
				'f_descr_font_weight' => '500',
				'el_class' => 'subscribe-hero-details',
				'tdc_css' => 'eyJhbGwiOnsicGFkZGluZy10b3AiOiIxMCIsInBhZGRpbmctYm90dG9tIjoiMTAiLCJkaXNwbGF5IjoiIn19'
			]
		)
		. \SelfHacked\SelfHackedNewspaper\Util::generate_shortcode(
			'tdm_block_inline_text',
			[
				'description' => $item_3,
				'description_color' => '#7c7287',
				'f_descr_font_size' => '18px',
				'f_descr_font_family' => '702',
				'f_descr_font_weight' => '500',
				'el_class' => 'subscribe-hero-details',
				'tdc_css' => 'eyJhbGwiOnsicGFkZGluZy10b3AiOiIxMCIsImRpc3BsYXkiOiIifX0='
			]
		)
		. \SelfHacked\SelfHackedNewspaper\Util::generate_shortcode( 'selfhacked_subscribe_form' )
		. \SelfHacked\SelfHackedNewspaper\Util::generate_shortcode(
			'tdm_block_inline_text',
			[
				'description' => $consent,
				'f_descr_font_size' => '12px',
				'f_descr_font_family' => '702',
				'description_color' => '#8c8693',
				'tdc_css' => 'eyJhbGwiOnsid2lkdGgiOiI2MCUiLCJkaXNwbGF5IjoiIn19'
			]
		)
	)
);

echo do_shortcode( $shortcode );