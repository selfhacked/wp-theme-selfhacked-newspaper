<?php
isset( $text_domain ) || ( $text_domain = null );
isset( $block_limit ) || ( $block_limit = 6 );
isset( $display_name ) || ( $display_name = null );
isset( $author_id ) || ( $author_id = 0 );
isset( $block_offset ) || ( $block_offset = 0 );

$custom_title = sprintf( __( 'Articles by %s', $text_domain ), $display_name );
$shortcode    = \SelfHacked\SelfHackedNewspaper\Util::generate_shortcode(
    'td_block_selfhacked_2',
    [
        'limit'             => $block_limit,
        'block_template_id' => 'td_block_template_19',
        'custom_title'      => $custom_title,
        'header_text_color' => '#674987',
        'border_color'      => '#7C7287',
        'tdc_css'           => [
            'all' => [
                'padding-top' => '40',
                'display' => ''
            ]
        ],
        'autors_id'         => $author_id,
        'offset'            => $block_offset
    ]
);

echo do_shortcode( $shortcode );