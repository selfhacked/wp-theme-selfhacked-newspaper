<?php

isset( $text_domain ) || ( $text_domain = null );
isset( $id ) || ( $id = null );
isset( $css_class ) || ( $css_class = null );
isset( $shortcode_data ) || ( $shortcode_data = null );
?>
<div <?php echo $id ? 'id="' . $id . '"' : '' ?> class="shortcode__selfhacked-cta <?php echo $css_class ?>">
	<div class="shortcode__selfhacked-cta-inner">
		<?php
		if ( isset( $shortcode_data['image']['ID'] ) ) {
			?>
			<div class="shortcode__selfhacked-cta__image">
				<div class="shortcode__selfhacked-cta__image--parallelogram">
					<?php
					echo wp_get_attachment_image( $shortcode_data['image']['ID'], 'thumbnail' );
					?>
				</div>
			</div>
			<?php
		}
		?>

		<?php
		if ( isset( $shortcode_data['content'] ) ) {
			?>
			<div class="shortcode__selfhacked-cta__content">
				<div class="editor-content">
					<?php
					echo \Enpii\Wp\EnpiiBase\Wp::get_post_content( $shortcode_data['content'] );
					?>
				</div>
			</div>
			<?php
		}
		?>

		<?php
		if ( $shortcode_data['link_url'] && $shortcode_data['link_text'] ) {
			?>
			<div class="shortcode__selfhacked-cta__link">
				<?php
				echo '<a href="' . $shortcode_data['link_url'] . '" title="' . esc_attr( $shortcode_data['link_text'] ) . '" target="' . $shortcode_data['link_target'] . '">' . $shortcode_data['link_text'] . '</a>';
				?>
			</div>
			<?php
		}
		?>
	</div>
</div>
