<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 11/26/18
 * Time: 4:29 PM
 */

isset( $text_domain ) || ( $text_domain = null );
isset( $email ) || ( $email = null );
isset( $submit_url ) || ( $submit_url = 'https://selfhacked.activehosted.com/f/42' );

$form_id = rand( 100, 999 );
?>

<div class="subscribe-form subscribe-form-shortcode">
	<form id="form-<?php echo $form_id ?>" method="POST" action="<?php echo $submit_url ?>"
	      class="subscribe-form__form-item"
	      target="_blank">
		<div class="_form-content subscribe-form__form-content">
			<?php
			$interest_data = [
				''                                         => __( "I'm most interested in...", $text_domain ),
				'Overcoming brain fog'                     => __( "Overcoming brain fog", $text_domain ),
				'Boosting energy and overcoming fatigue'   => __( "Boosting energy and overcoming fatigue", $text_domain ),
				'Overcoming inflammation and autoimmunity' => __( "Overcoming inflammation and autoimmunity", $text_domain ),
				'Optimal health and longevity'             => __( "Optimal health and longevity", $text_domain ),
				'Balancing hormones and metabolism'        => __( "Balancing hormones and metabolism", $text_domain ),
				'Biohacking/Becoming superhuman'           => __( "Biohacking/Becoming superhuman", $text_domain ),
				'All of the above'                                => __( "All of the above", $text_domain ),
			];
			?>
			<div class="selfhacked-selectbox-style-1-wrapper">
				<select title="<?php __( "I'm most interested in...", $text_domain ) ?>"
				        name="subscribe_form_data[interest][]" class="subscribe-form__interest selfhacked-selectbox-style-1" required>
					<?php
					$index = 0;
					foreach ( $interest_data as $interest_value => $interest_label ) {
						echo '<option value="' . esc_attr( $interest_value ) . '">' . esc_html( $interest_label ) . '</option>';

						$index ++;
					}
					?>
				</select>
			</div>
			<input id="subscribe-form__email-<?php echo $form_id ?>" class="subscribe-form__email"
			       type="email" name="subscribe_form_data[email]"
			       pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" required
			       placeholder="<?php echo __( 'Email', $text_domain ) ?>" value="<?php echo $email ?>">
			<button type="submit" form="form-<?php echo $form_id ?>" class="_form-submit subscribe-form__submit-button">
				<i class="fa fa-angle-right"></i><label><?php echo __( 'SUBSCRIBE', $text_domain ) ?></label></button>
		</div>
	</form>

	<div class="subscribe-form__thank-you-message">
		<?php echo __( 'Thank you for subscribing!', $text_domain ) ?>
	</div>

	<div class="subscribe-form__processing">
		<?php echo __( 'Processing...', $text_domain ) ?>
	</div>
</div>
