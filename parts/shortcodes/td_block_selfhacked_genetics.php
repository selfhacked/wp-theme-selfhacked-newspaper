<?php

isset( $block_js ) || ( $block_js = '' );
isset( $block_css ) || ( $block_css = '' );
isset( $block_classes ) || ( $block_classes = '' );
isset( $block_title ) || ( $block_title = '' );
isset( $block_uid ) || ( $block_uid = '' );
isset( $block_inner ) || ( $block_inner = '' );
isset( $block_pagination ) || ( $block_pagination = '' );

echo $block_js;
echo $block_css;
?>

<div class="<?php echo $block_classes; ?>">
	<div class="td-block-title-wrap">
		<?php echo $block_title; ?>
	</div>

	<div id="<?php echo $block_uid; ?>" class="td_block_inner">
		<?php echo $block_inner; ?>
	</div>

	<?php echo $block_pagination; ?>
</div>
