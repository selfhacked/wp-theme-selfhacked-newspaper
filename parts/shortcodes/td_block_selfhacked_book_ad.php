<?php

isset( $block_css ) || ( $block_css = '' );
isset( $block_classes ) || ( $block_classes = '' );
isset( $button_link ) || ( $button_link = '' );
isset( $image_url ) || ( $image_url = '' );
isset( $title ) || ( $title = '' );
isset( $price ) || ( $price = '' );
isset( $description ) || ( $description = '' );

echo $block_css;
?>
<a class="button" href="<?php echo $button_link; ?>">
	<div class="<?php echo $block_classes; ?>">
		<div class="content-wrapper">
			<img src="<?php echo $image_url; ?>">
			<div class="details-wrapper">
				<h3 class="title">
					<?php echo $title; ?>
				</h3>
				<div class="price">
					<?php echo $price; ?>
				</div>
				<div class="description">
					<?php echo $description; ?>
				</div>
			</div>
		</div>
	</div>
</a>