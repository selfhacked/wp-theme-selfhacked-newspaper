<?php

isset( $block_css ) || ( $block_css = '' );
isset( $block_classes ) || ( $block_classes = '' );
isset( $block_title ) || ( $block_title = '' );
isset( $image_url_1 ) || ( $image_url_1 = '' );
isset( $image_url_2 ) || ( $image_url_2 = '' );
isset( $image_url_3 ) || ( $image_url_3 = '' );
isset( $image_url_4 ) || ( $image_url_4 = '' );
isset( $description_1 ) || ( $description_1 = '' );
isset( $description_2 ) || ( $description_2 = '' );
isset( $description_3 ) || ( $description_3 = '' );
isset( $description_4 ) || ( $description_4 = '' );

echo $block_css;
?>

<div class="<?php echo $block_classes; ?>">
    <div class="td-block-title-wrap">
		<?php echo $block_title; ?>
    </div>
    <div class="td-block-row">
        <div class="td-block-span3">
            <div class="column">
                <img src="<?php echo $image_url_1; ?>" width="130px" height="130px">
                <div class="description">
                    <?php echo $description_1; ?>
                </div>
                <div class="circle">1</div>
            </div>
        </div>
        <div class="td-block-span3">
            <div class="column">
                <img src="<?php echo $image_url_2; ?>" width="130px" height="130px">
                <div class="description description-bold">
	                <?php echo $description_2; ?>
                </div>
                <div class="circle">2</div>
            </div>
        </div>
        <div class="td-block-span3">
            <div class="column">
                <img src="<?php echo $image_url_3; ?>" width="130px" height="130px">
                <div class="description">
	                <?php echo $description_3; ?>
                </div>
                <div class="circle">3</div>
            </div>
        </div>
        <div class="td-block-span3">
            <div class="column">
                <img src="<?php echo $image_url_4; ?>" width="130px" height="130px">
                <div class="description description-bold">
	                <?php echo $description_4; ?>
                </div>
                <div class="circle">4</div>
            </div>
        </div>
    </div>
</div>
