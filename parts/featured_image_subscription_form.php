<?php

isset( $text_domain ) || ( $text_domain = '' );
isset( $image_url ) || ( $image_url = false );
isset( $width ) || ( $width = 0 );
isset( $height ) || ( $height = 0 );

if ( $image_url ) : ?>
	<div class="featured-image" style="background:  linear-gradient(180deg, rgba(124, 114, 135, 0) 0%, rgba(69, 64, 75, 1) 100%), url('<?php echo $image_url; ?>'), no-repeat fixed; background-size: cover; height: <?php echo $height; ?>px;">
        <div class="subscribe-form-wrapper">
            <h3><?php _e( 'Sign up to our mailing list', $text_domain ); ?></h3>
		    <?php echo do_shortcode('[selfhacked_subscribe_form]'); ?>
        </div>
	</div>
<?php endif; ?>