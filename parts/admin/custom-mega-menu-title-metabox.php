<?php

isset( $text_domain ) || ( $text_domain = '' );

$label = __( 'Custom Mega Menu Title', $text_domain );

?>

<div id="custom-mega-menu-title-metabox" class="inline-edit-row">
	<div class="inline-edit-col-left">
		<label for="custom-mega-menu-title"><?php echo $label; ?></label>
		<input type="text" id="custom-mega-menu-title" name="custom-mega-menu-title" value="">
	</div>
</div>
