<?php

isset( $text_domain ) || ( $text_domain = '' );
isset( $module_classes ) || ( $module_classes = '' );
isset( $module_title ) || ( $module_title = '' );

?>

<div class="<?php echo $module_classes; ?>">
    <div class="title">
        <div class="title-prefix"></div>
	    <?php echo $module_title; ?>
    </div>
</div>
