<?php

isset( $text_domain ) || ( $text_domain = '' );
isset( $breadcrumbs ) || ( $breadcrumbs = '' );
isset( $title ) || ( $title );
isset( $description ) || ( $description );
isset( $categories ) || ( $categories = [] );
isset( $pull_down ) || ( $pull_down = '' );
isset( $category_id ) || ( $category_id = 0 );
isset( $parent_id ) || ( $parent_id = 0 );

?>
<div class="td_category_template_9">
	<div class="td-category-header td-container-wrap">
		<div class="td-container">
			<div class="td-pb-row">
				<div class="td-pb-span12">
					<div class="td-crumb-container"><?php echo $breadcrumbs; ?></div>
					<div class="td-ss-main-content">
						<div class="header-block">
							<h1 class="td-page-title td-block-title">
                                <?php if ( $parent_id == 0 ): ?>
                                    <span><?php echo $title; ?></span>
                                <?php else: ?>
                                    <a href="<?php echo get_category_link( $parent_id ); ?>"><?php echo $title; ?></a>
                                <?php endif; ?>
							</h1>
							<?php echo $description; ?>
						</div>
						<?php
						/** @var WP_Term $category */
						foreach ( (array) $categories as $category ) :
							$classes = 'category-filter';

							if ( $category->term_id == $category_id ) {
								$classes .= ' category-filter-active';
							}
							?>
							<a href="<?php echo get_category_link( $category->term_id ); ?>">
                                <button id="category-<?php echo $category->term_id; ?>" class="<?php echo $classes; ?>">
                                    <?php echo $category->name; ?>
                                </button>
                            </a>
                        <?php endforeach; ?>
						<div class="header-block">
							<h1 class="entry-title">
								<span><?php _e( 'Posts', $text_domain ); ?></span>
							</h1>
							<?php echo $pull_down; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
