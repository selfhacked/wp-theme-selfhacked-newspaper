<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 10/23/18
 * Time: 3:37 PM
 */

use Enpii\Wp\EnpiiBase\Base\WpApp;

$wp_theme = WpApp::instance()->get_wp_theme();

if ( td_util::get_option( 'tds_sub_footer' ) != 'no' ) { ?>
	<div class="td-sub-footer-container td-container-wrap <?php echo td_util::get_option( 'td_full_footer' ); ?>">
        <div class="tdc-content-wrap">
            <div class="tdc-row tdc-row stretch_row_1200 td-stretch-content">
                <div class="td-pb-row">
                    <div class="td-pb-span6 sub-footer__logo">
                        <?php
                        $sub_footer_copyright_logo = get_field( 'shnews_sub_footer_logo', 'options' );

                        echo get_image_tag( $sub_footer_copyright_logo['ID'], __( 'SelfHacked Footer Logo', $wp_theme->text_domain ), __( 'SelfHacked Footer Logo', $wp_theme->text_domain ), null, 'thumbnail' );
                        ?>
                    </div>

                    <div class="td-pb-span sub-footer__copyright-text">
                        <?php
                        $sub_footer_copyright_text = str_replace( '{{year}}', date( 'Y' ), get_field( 'shnews_sub_footer_copyright_text', 'options' ) );

                        echo $wp_theme->process_html( $sub_footer_copyright_text );
                        ?>
                    </div>
                </div>
            </div>
		</div>
	</div>
<?php } ?>
