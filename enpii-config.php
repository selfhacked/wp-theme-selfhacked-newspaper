<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 10/19/18
 * Time: 11:58 AM
 */

$text_domain = 'selfhacked';
$config      = [
	'id'         => 'selfhacked-newspaper',
	'basePath'   => WP_CONTENT_DIR,
	'components' => [
		'wp_theme'                  => [
			'class'            => SelfHacked\SelfHackedNewspaper\Component\WpTheme::class,
			'version'          => '0.01',
			'text_domain'      => $text_domain,
			'use_cdn'          => false,
			'base_path'        => get_template_directory(),
			'base_url'         => get_template_directory_uri(),

			// only set when child theme using
			'child_base_path'  => get_template_directory() === get_stylesheet_directory() ? null : get_stylesheet_directory(),
			'child_base_url'   => get_template_directory_uri() === get_stylesheet_directory_uri() ? null : get_stylesheet_directory_uri(),

			// Extra params
			'subscribe_url'    => home_url('newsletter-subscription').'?skin=iframe',

			// Components to be infected to this component
			'newspaper_module' => 'wp_theme_newspaper_module',
            'dislike_reason'   => 'wp_theme_dislike_reason',
			'admin'            => 'wp_theme_admin',
			'widget'           => 'wp_theme_widget',
			'shortcode'        => 'wp_theme_shortcode',
			'acf'              => 'wp_theme_acf',
		],
		'wp_theme_newspaper_module' => [
			'class'       => \SelfHacked\SelfHackedNewspaper\Component\WpTheme\NewspaperModule::class,
			'text_domain' => $text_domain,
		],
        'wp_theme_dislike_reason' => [
            'class'       => \SelfHacked\SelfHackedNewspaper\Component\WpTheme\DislikeReason::class,
            'text_domain' => $text_domain,
        ],
		'wp_theme_admin'            => [
			'class'       => \SelfHacked\SelfHackedNewspaper\Component\WpTheme\Admin::class,
			'text_domain' => $text_domain,
		],
		'wp_theme_acf'              => [
			'class'       => \SelfHacked\SelfHackedNewspaper\Component\WpTheme\Acf::class,
			'text_domain' => $text_domain,
		],
		'wp_theme_widget'           => [
			'class'       => \SelfHacked\SelfHackedNewspaper\Component\WpTheme\Widget::class,
			'text_domain' => $text_domain,
		],
		'wp_theme_shortcode'           => [
			'class'       => \SelfHacked\SelfHackedNewspaper\Component\WpTheme\Shortcode::class,
			'text_domain' => $text_domain,
		],
	],
];

return $config;
