<?php
/**
 * single Post template 2
 **/

if (have_posts()) {
    the_post();

    $td_mod_single = new td_module_single_custom($post);

    ?>

    <div class="td-post-content">
        <?php echo $td_mod_single->get_image(td_global::$load_featured_img_from_template); ?>

        <?php echo $td_mod_single->get_content();?>
    </div>
    <div class="post-ratings-wrapper">
        <div class="post-ratings-hr"></div>
        <h3 class="post-ratings-header"><?php _e('RATE THIS ARTICLE', 'selfhacked'); ?></h3>
        <?php if(is_plugin_active('wp-postratings/wp-postratings.php')): echo do_shortcode( '[ratings]'); endif; ?>
        <div class="post-ratings-hr"></div>
        <?php

        $disclaimer = get_field('disclaimer', 'options' );
        echo '<div class="disclaimer-wrapper">';
        echo '<h3 class="title">' . $disclaimer['title'] . '</h3>';
        echo '<span class="description">' . $disclaimer['description'] . '</span>';
        echo '</div>';
			?>
    </div>

    <footer>
        <?php echo $td_mod_single->get_post_pagination();?>
        <?php echo $td_mod_single->get_review();?>

        <div class="td-post-source-tags">
            <?php echo $td_mod_single->get_source_and_via();?>
            <?php echo $td_mod_single->get_the_tags();?>
        </div>

        <?php echo $td_mod_single->get_next_prev_posts();?>
        <?php echo $td_mod_single->get_author_box();?>
        <?php echo $td_mod_single->get_item_scope_meta();?>
    </footer>

    <?php
} else {
    //no posts
    echo td_page_generator::no_posts();
}