<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 12/7/18
 * Time: 12:10 PM
 */

namespace SelfHacked\SelfHackedNewspaper\Component\WpTheme;


use Enpii\Wp\EnpiiBase\Base\BaseComponent;

class AcfPHP2Json extends BaseComponent {

	/**
	 * @var string $text_domain
	 */
	public $text_domain = null;

	/**
	 * Initialize all ACF fields
	 */
	public function initialize() {
		add_action( 'admin_menu', [ $this, 'admin_menu' ], 20 );
	}

	/**
	 * Add submenu item under 'Custom Fields'
	 */
	public function admin_menu() {
		add_submenu_page( 'edit.php?post_type=acf-field-group', __( 'Convert PHP fields to JSON', $this->text_domain ), __( 'PHP to JSON', $this->text_domain ), 'manage_options', 'acf-php-to-json', [
			$this,
			'admin_page'
		] );
	}


	/**
	 * Output the admin page
	 */
	public function admin_page() {
		?>
		<div class="wrap">
			<h1><?php echo __( 'Convert PHP fields to JSON', $this->text_domain ) ?></h1>
			<?php

			if ( ! isset( $_GET['continue'] ) || $_GET['continue'] !== 'true' ) {
				$this->admin_page_intro();
			} else {
				$this->admin_page_convert();
			}
			?>
		</div>
		<?php
	}

	/**
	 * Output the introductory page
	 */
	public function admin_page_intro() {
		$groups = $this->get_groups_to_convert();

		if ( empty( $groups ) ) {
			echo '<p>' . __( 'No PHP field group configuration found. Nothing to convert.', $this->text_domain ) . '</p>';

			return;
		} else {
			echo sprintf( '<p>' . __( '%d field groups will be converted from PHP to JSON configuration.', $this->text_domain ) . '</p>', count( $groups ) );
			echo '<a href="edit.php?post_type=acf-field-group&page=acf-php-to-json&continue=true" class="button button-primary">' . __( 'Convert Field Groups', $this->text_domain ) . '</a>';
		}
	}

	/**
	 * Convert the field groups and output the conversion page
	 */
	function admin_page_convert() {
		$groups = $this->get_groups_to_convert();

		echo sprintf( '<p>' . __( 'Converting %d field groups from PHP to JSON configuration...', $this->text_domain ) . '</p>', count( $groups ) );

		echo '<ol>';
		foreach ( $groups as $group ) {
			if ( $this->convert_group( $group ) ) {
				echo sprintf( '<li>' . __( 'Converted: <strong>%s</strong> (%s)', $this->text_domain ) . '</li>', $group['title'], $group['key'] );
			} else {
				echo sprintf( '<li><strong>' . __( 'Failed to convert: %s', $this->text_domain ) . '</strong> (%s)</li>', $group['title'], $group['key'] );
			}
		}
		echo '</ol>';

		echo '<p>' . __( 'Done. Now remove the PHP field group configuration.', $this->text_domain ) . '</p>';
	}

	/**
	 * Get the PHP field groups which will be converted.
	 *
	 * @return array
	 */
	public function get_groups_to_convert() {
		$groups = acf_get_local_field_groups();
		if ( ! $groups ) {
			return [];
		}

		return array_filter( $groups, function ( $group ) {
			return $group['local'] == 'php';
		} );
	}

	/**
	 * Convert a field group to JSON
	 *
	 * @param array $group
	 *
	 * @return bool
	 */
	public function convert_group( $group ) {
		$group['fields'] = acf_get_local_fields( $group['key'] );

		return acf_write_json_field_group( $group );
	}

}
