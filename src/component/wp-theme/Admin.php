<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 10/23/18
 * Time: 9:48 PM
 */

namespace SelfHacked\SelfHackedNewspaper\Component\WpTheme;

use Enpii\Wp\EnpiiBase\Wp;
use Enpii\Wp\EnpiiBase\Base\BaseComponent;

class Admin extends BaseComponent {
	/**
	 * @var string $text_domain
	 */
	public $text_domain = null;

	/**
	 * Initialize all Admin hooks
	 */
	public function initialize() {
		$this->init_hooks();
	}

	/**
	 * Initialize all hooks needed
	 */
	public function init_hooks() {
		add_action( 'admin_menu', [ $this, 'reorder_newspaper_menu_item' ], 10001 );
		add_filter( 'manage_post_posts_columns', [ $this, 'add_custom_mega_menu_title_column' ] );
		add_action( 'manage_post_posts_custom_column', [ $this, 'custom_mega_menu_title_display' ], 10, 2 );
		add_action( 'quick_edit_custom_box', [ $this, 'custom_mega_menu_title_metabox' ], 10, 2 );
		add_action( 'save_post', [ $this, 'custom_mega_menu_title_save' ], 10, 2 );
		add_action( 'admin_print_footer_scripts-edit.php', [ $this, 'custom_mega_menu_title_javascript' ] );
		add_filter( 'post_row_actions', [ $this, 'custom_mega_menu_title_set_data' ], 10, 2 );

		add_action( 'restrict_manage_posts', [ $this, 'posts_filter_dropdown' ], 100 );
	}

	/**
	 * Reorder Newspaper Admin Menu Item to have it close to Appearance Options
	 */
	public function reorder_newspaper_menu_item() {
		global $menu;

		$menu_item_details_newspaper                           = null;
		$menu_position_newspaper                               = null;
		$menu_position_selfhacked_newspaper_appearance_options = null;
		foreach ( $menu as $menu_position => $menu_item_details ) {
			if ( $menu_item_details[2] === 'td_theme_welcome' ) {
				$menu_position_newspaper     = $menu_position;
				$menu_item_details_newspaper = $menu_item_details;
			}

			if ( $menu_item_details[2] === 'selfhacked-newspaper-appearance-options' ) {
				$menu_position_selfhacked_newspaper_appearance_options = $menu_position;
			}
		}

		if ( $menu_position_newspaper ) {
			unset( $menu[ $menu_position_newspaper ] );
		}

		if ( $menu_item_details_newspaper && $menu_position_selfhacked_newspaper_appearance_options ) {
			$menu[ (string) ( $menu_position_selfhacked_newspaper_appearance_options - 0.001 ) ] = $menu_item_details_newspaper;
		}
	}

	public function custom_mega_menu_title_metabox( $column_name, $post_type ) {
		if ( $column_name != 'custom_mega_menu_title' ) {
			return;
		}
		echo Wp::get_template_part( 'parts/admin/custom-mega-menu-title-metabox', [
			'text_domain' => $this->text_domain
		] );
	}

	public function add_custom_mega_menu_title_column( $post_columns ) {
		return array_merge( $post_columns, [ 'custom_mega_menu_title' => __( 'Custom Mega Menu Title', $this->text_domain ) ] );
	}

	public function custom_mega_menu_title_display( $column_name, $post_id ) {
		if ( $column_name == 'custom_mega_menu_title' ) {
			$custom_mega_menu_title = get_post_meta( $post_id, 'custom_mega_menu_title', true );

			if ( $custom_mega_menu_title ) {
				echo esc_html( $custom_mega_menu_title );
			} else {
				echo '';
			}
		}
	}

	public function custom_mega_menu_title_save( $post_id, $post ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( $post->post_type != 'post' ) {
			return;
		}

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		if ( filter_has_var( INPUT_POST, 'custom-mega-menu-title' ) ) {
			$custom_title = filter_input( INPUT_POST, 'custom-mega-menu-title' );
			update_post_meta( $post_id, 'custom_mega_menu_title', $custom_title );
		}
	}

	public function custom_mega_menu_title_javascript() {
		$current_screen = get_current_screen();

		if ( $current_screen->id != 'edit-post' || $current_screen->post_type != 'post' ) {
			return;
		}

		wp_enqueue_script( 'jquery' );
		echo '<script type="text/javascript">jQuery(function($){$("#the-list").on("click","a.editinline",function(e){e.preventDefault();var t=$(this).data("custom-title");inlineEditPost.revert();$("#custom-mega-menu-title").val(t?t:"")})})</script>';
	}

	public function custom_mega_menu_title_set_data( $actions, $post ) {
		$found_value = get_post_meta( $post->ID, 'custom_mega_menu_title', true );

		if ( $found_value ) {
			if ( isset( $actions['inline hide-if-no-js'] ) ) {
				$new_attribute                   = sprintf( 'data-custom-title="%s"', esc_attr( $found_value ) );
				$actions['inline hide-if-no-js'] = str_replace( 'class=', "$new_attribute class=", $actions['inline hide-if-no-js'] );
			}
		}

		return $actions;
	}

	/**
	 * Print out the dropdown to choose to apply ElasticSearch or not
	 */
	public function posts_filter_dropdown() {
		$es = isset($_GET['es']) ? $_GET['es'] : null;
		echo '<select name="es" id="sh-es-filter">';

		echo '<option value="">' . __( 'Default Query Method', $this->text_domain ) . '</option>';
		echo '<option value="1" '.($es ? 'selected="selected"' : '').'>' . __( 'Use ElasticSearch', $this->text_domain ) . '</option>';

		echo '</select>';
	}
}
