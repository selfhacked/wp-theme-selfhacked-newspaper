<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 11/26/18
 * Time: 4:23 PM
 */

namespace SelfHacked\SelfHackedNewspaper\Component\WpTheme;


use Enpii\Wp\EnpiiBase\Base\BaseComponent;
use Enpii\Wp\EnpiiBase\Wp;
use SelfHacked\SelfHackedNewspaper\Component\WpTheme;

class Shortcode extends BaseComponent {
	public $text_domain = null;

	public function initialize() {
		add_shortcode( 'selfhacked_subscribe_form', [ $this, 'subscribe_form' ] );
        add_shortcode( 'selfhacked_subscribe_cta', [ $this, 'subscribe_cta' ] );
		add_shortcode( 'selfhacked_subscribe_row', [ $this, 'subscribe_row' ] );
	}

	public function subscribe_form() {
		/** @var WpTheme $wp_theme */
		$wp_theme = $this->get_app_instance()->get_wp_theme();

		return Wp::get_template_part( 'parts/shortcodes/subscribe-form', [
			'text_domain' => $this->text_domain,
			'submit_url'  => $wp_theme->subscribe_url,
		] );
	}

	public function subscribe_cta() {
		return Wp::get_template_part( 'parts/shortcodes/subscribe-cta', [ 'text_domain' => $this->text_domain ] );
	}

	public function subscribe_row() {
		return Wp::get_template_part( 'parts/shortcodes/subscribe', [ 'text_domain' => $this->text_domain ] );
	}
}
