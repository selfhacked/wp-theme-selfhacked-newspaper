<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 11/13/18
 * Time: 2:05 PM
 */

namespace SelfHacked\SelfHackedNewspaper\Component\WpTheme;

use Enpii\Wp\EnpiiBase\Base\BaseComponent;
use SelfHacked\SelfHackedNewspaper\Component\WpTheme;

class NewspaperModule extends BaseComponent {
	/**
	 * @var string $text_domain
	 */
	public $text_domain = null;

	/** @var WpTheme|null $wp_theme */
	public $wp_theme = null;

	/**
	 * Initialize all Extra Modules
	 */
	public function initialize() {
		$this->wp_theme = $this->get_app_instance()->get_wp_theme();
		$this->init_hooks();
	}

	/**
	 * Initialize all hooks needed
	 */
	public function init_hooks() {
		add_action( 'td_global_after', [ $this, 'add_selfhacked_templates' ] );
		add_action( 'td_global_after', [ $this, 'update_templates' ] );
	}

	public function add_selfhacked_templates() {
		$this->add_sub_footer_templates();
		$this->add_category_templates();
		$this->add_footer_templates();
		$this->add_block_templates();
		$this->add_thumbnails();
		$this->add_modules();
		$this->add_blocks();
	}

	public function update_templates() {
		$this->update_blocks();
		$this->update_modules();
	}

	protected function add_modules() {
		\td_api_module::add( 'td_module_selfhacked_1',
			[
				'file'                         => $this->wp_theme->child_base_path . "/includes/modules/td_module_selfhacked_1.php",
				'text'                         => __( 'Selfhacked Module 1', $this->text_domain ),
				'excerpt_title'                => 12,
				'excerpt_content'              => 25,
				'enabled_on_more_articles_box' => true,
				'enabled_on_loops'             => true,
				'uses_columns'                 => true,
				'category_label'               => true,
				'class'                        => 'td_module_wrap td-animation-stack',
				'text_domain' => $this->text_domain
			]
		);

		\td_api_module::add( 'td_module_selfhacked_2',
			[
				'file'                         => $this->wp_theme->child_base_path . "/includes/modules/td_module_selfhacked_2.php",
				'text'                         => __( 'Selfhacked Module 2', $this->text_domain ),
				'excerpt_title'                => 12,
				'excerpt_content'              => 25,
				'enabled_on_more_articles_box' => true,
				'enabled_on_loops'             => true,
				'uses_columns'                 => true,
				'category_label'               => true,
				'class'                        => 'td_module_wrap td-animation-stack',
				'text_domain' => $this->text_domain
			]
		);

		\td_api_module::add( 'td_module_selfhacked_3',
			[
				'file'                         => $this->wp_theme->child_base_path . "/includes/modules/td_module_selfhacked_3.php",
				'text'                         => __( 'Selfhacked Module 3', $this->text_domain ),
				'excerpt_title'                => 12,
				'excerpt_content'              => 25,
				'enabled_on_more_articles_box' => true,
				'enabled_on_loops'             => true,
				'uses_columns'                 => true,
				'category_label'               => true,
				'class'                        => 'td_module_wrap td-animation-stack',
				'text_domain' => $this->text_domain
			]
		);

		\td_api_module::add( 'td_module_substances',
			[
				'file'                         => $this->wp_theme->child_base_path . "/includes/modules/td_module_substances.php",
				'text'                         => __( 'Selfhacked Module Substances', $this->text_domain ),
				'enabled_on_more_articles_box' => true,
				'enabled_on_loops'             => true,
				'uses_columns'                 => true,
				'category_label'               => true,
				'class'                        => 'td_module_wrap td-animation-stack',
				'text_domain' => $this->text_domain
			]
		);

		\td_api_module::add( 'td_module_top_substances',
			[
				'file'                         => $this->wp_theme->child_base_path . "/includes/modules/td_module_top_substances.php",
				'text'                         => __( 'Selfhacked Module Top Substances', $this->text_domain ),
				'enabled_on_more_articles_box' => true,
				'enabled_on_loops'             => true,
				'uses_columns'                 => true,
				'category_label'               => true,
				'class'                        => 'td_module_wrap td-animation-stack',
				'text_domain' => $this->text_domain
			]
		);

		\td_api_module::add( 'td_module_genetics',
			[
				'file'                         => $this->wp_theme->child_base_path . "/includes/modules/td_module_genetics.php",
				'text'                         => __( 'Selfhacked Module Genetics', $this->text_domain ),
				'enabled_on_more_articles_box' => true,
				'enabled_on_loops'             => true,
				'uses_columns'                 => true,
				'category_label'               => true,
				'class'                        => 'td_module_wrap td-animation-stack',
				'text_domain'                  => $this->text_domain
			]
		);

		\td_api_module::add( 'td_module_single_custom',
			[
				'file'                         => $this->wp_theme->child_base_path . "/includes/td_module_single_custom.php",
				'text'                         => __( 'Selfhacked Single Post Module', $this->text_domain ),
				'enabled_on_more_articles_box' => true,
				'enabled_on_loops'             => true,
				'uses_columns'                 => true,
				'category_label'               => true,
				'class'                        => 'td_module_wrap td-animation-stack',
				'text_domain'                  => $this->text_domain
			]
		);
	}

	protected function add_blocks() {
		// get the default block_general atts
		$block_general = \td_config::get_map_block_general_array();

		// add a new param for slider_alias
		array_push( $block_general,
			[
				"param_name" => "separator",
				"type"       => "text_separator",
				'heading'    => __( 'Ad settings', $this->text_domain ),
				"value"      => "",
				"class"      => "",
			],
			[
				'type'        => 'attach_image',
				'heading'     => __( 'Image', $this->text_domain ),
				'param_name'  => 'image',
				'value'       => '',
				'description' => __( 'Choose an image from the media library', $this->text_domain ),
				'dependency'  => [
					'element' => 'source',
					'value'   => 'media_library',
				],
				'admin_label' => true,
			],
			[
				'param_name'  => "link",
				'type'        => "textfield",
				'value'       => "",
				'heading'     => __( "Ad link:", $this->text_domain ),
				'description' => __( "The ad link", $this->text_domain ),
				'holder'      => "div",
				'class'       => "tdc-textfield-extrabig"
			]
		);

		\td_api_block::add( 'td_block_selfhacked_1',
			[
				'map_in_visual_composer' => true,
				'map_in_td_composer'     => true,
				"name"                   => __( 'Selfhacked Block 1', $this->text_domain ),
				"base"                   => 'td_block_selfhacked_1',
				"class"                  => 'td_block_selfhacked_1',
				"controls"               => "full",
				"category"               => 'Blocks',
				'tdc_category'           => 'Blocks',
				'file'                   => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_selfhacked_1.php',
				'text_domain' => $this->text_domain,
				"params"                 => array_merge(
					$block_general,
					\td_config::get_map_filter_array(),
					\td_config::get_map_block_ajax_filter_array(),
					\td_config::get_map_block_pagination_array()
				)
			]
		);

		\td_api_block::add( 'td_block_selfhacked_2',
			[
				'map_in_visual_composer' => true,
				'map_in_td_composer'     => true,
				"name"                   => __( 'Selfhacked Block 2', $this->text_domain ),
				"base"                   => 'td_block_selfhacked_2',
				"class"                  => 'td_block_selfhacked_2',
				"controls"               => "full",
				"category"               => 'Blocks',
				'tdc_category'           => 'Blocks',
				'file'                   => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_selfhacked_2.php',
				'text_domain' => $this->text_domain,
				"params"                 => array_merge(
					\td_config::get_map_block_general_array(),
					\td_config::get_map_filter_array(),
					\td_config::get_map_block_ajax_filter_array(),
					\td_config::get_map_block_pagination_array()
				)
			]
		);

		\td_api_block::add( 'td_block_selfhacked_3',
			[
				'map_in_visual_composer' => true,
				'map_in_td_composer'     => true,
				"name"                   => __( 'Selfhacked Block 3', $this->text_domain ),
				"base"                   => 'td_block_selfhacked_3',
				"class"                  => 'td_block_selfhacked_3',
				"controls"               => "full",
				"category"               => 'Blocks',
				'tdc_category'           => 'Blocks',
				'file'                   => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_selfhacked_3.php',
				'text_domain' => $this->text_domain,
				"params"                 => array_merge(
					\td_config::get_map_block_general_array(),
					\td_config::get_map_filter_array(),
					\td_config::get_map_block_ajax_filter_array(),
					\td_config::get_map_block_pagination_array()
				)
			]
		);

		$store_params = [
			[
				'type'        => 'attach_image',
				'heading'     => __( 'Image', $this->text_domain ),
				'param_name'  => 'image',
				'value'       => '',
				'description' => __( 'Choose an image from the media library', $this->text_domain ),
				'dependency'  => [
					'element' => 'source',
					'value'   => 'media_library',
				],
				'admin_label' => true,
			],
			[
				'param_name'  => "title",
				'type'        => "textfield",
				'value'       => "",
				'heading'     => __( "Title:", $this->text_domain ),
				'description' => __( "The product title", $this->text_domain ),
				'holder'      => "div",
				'class'       => "tdc-textfield-extrabig"
			],
			[
				'param_name'  => "description",
				'type'        => "textfield",
				'value'       => "",
				'heading'     => __( "Description:", $this->text_domain ),
				'description' => __( "The product description", $this->text_domain ),
				'holder'      => "div",
				'class'       => "tdc-textfield-extrabig"
			],
			[
				'param_name'  => "price",
				'type'        => "textfield",
				'value'       => "",
				'heading'     => __( "Price:", $this->text_domain ),
				'description' => __( "The product price", $this->text_domain ),
				'holder'      => "div",
				'class'       => "tdc-textfield-extrabig"
			],
			[
				"type"        => 'colorpicker',
				"holder"      => 'div',
				"class"       => '',
				"heading"     => __( 'Title Color:', $this->text_domain ),
				"param_name"  => 'title_color',
				"value"       => '',
				"description" => __( 'Optional - The title font color', $this->text_domain ),
				'td_type'     => 'block_template',
			],
			[
				"type"        => 'colorpicker',
				"holder"      => 'div',
				"class"       => '',
				"heading"     => __( 'Description Color:', $this->text_domain ),
				"param_name"  => 'description_color',
				"value"       => '',
				"description" => __( 'Optional - The description font color', $this->text_domain ),
				'td_type'     => 'block_template',
			],
			[
				"type"        => 'colorpicker',
				"holder"      => 'div',
				"class"       => '',
				"heading"     => __( 'Price Color:', $this->text_domain ),
				"param_name"  => 'price_color',
				"value"       => '',
				"description" => __( 'Optional - The price font color', $this->text_domain ),
				'td_type'     => 'block_template',
			]
		];

		\td_api_block::add( 'td_block_store_item',
			[
				'map_in_visual_composer' => true,
				'map_in_td_composer'     => true,
				"name"                   => __( 'Selfhacked Store Item', $this->text_domain ),
				"base"                   => 'td_block_store_item',
				"class"                  => 'td_block_store_item',
				"controls"               => "full",
				"category"               => 'Blocks',
				'tdc_category'           => 'Blocks',
				'file'                   => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_store_item.php',
				'text_domain' => $this->text_domain,
				"params"                 => array_merge(
					$store_params,
					[
						[
							'param_name'  => 'button_text',
							'type'        => 'textfield',
							'value'       => 'Buy Now',
							'heading'     => __( "Button Text:", $this->text_domain ),
							'description' => __( "The product's button text", $this->text_domain ),
							'holder'      => 'div',
							'class'       => 'tdc-textfield-extrabig'
						],
						[
							'param_name'  => 'button_link',
							'type'        => 'textfield',
							'value'       => '',
							'heading'     => __( "Button link:", $this->text_domain ),
							'description' => __( "The product button's link", $this->text_domain ),
							'holder'      => 'div',
							'class'       => 'tdc-textfield-extrabig'
						],
						[
							'param_name'  => 'button_link_target',
							"type" => "dropdown",
							"value" => array(
								'_self' => 'Current',
								'_blank' => 'New Tab',
							),
							'heading'     => __( "Button link Target:", $this->text_domain ),
							'description' => __( "Choose the way to open the link.", $this->text_domain ),
							'holder'      => 'div',
							'class'       => 'tdc-textfield-extrabig',
						],
						[
							"type"        => 'colorpicker',
							"holder"      => 'div',
							"class"       => '',
							"heading"     => __( 'Button Font Color:', $this->text_domain ),
							"param_name"  => 'button_font_color',
							"value"       => '',
							"description" => __( 'Optional - The button font color', $this->text_domain ),
							'td_type'     => 'block_template',
						],
						[
							'type'        => 'colorpicker',
							'holder'      => 'div',
							'class'       => '',
							'heading'     => __( 'Button BG Color:', $this->text_domain ),
							'param_name'  => 'button_bg_color',
							'value'       => '',
							'description' => __( 'Optional - The button background color', $this->text_domain ),
							'td_type'     => 'block_template',
						],
						[
							'type'        => 'colorpicker',
							'holder'      => 'div',
							'class'       => '',
							'heading'     => __( 'Content BG Color:', $this->text_domain ),
							'param_name'  => 'content_bg_color',
							'value'       => '',
							'description' => __( 'Optional - The background color of the content', $this->text_domain ),
							'td_type'     => 'block_template',
						]
					]
				)
			]
		);

		$block_general = [
			[
				'param_name' => 'separator',
				'type'       => 'text_separator',
				'heading'    => __( 'Category 1 Settings', $this->text_domain ),
				'value'      => '',
				'class'      => '',
			],
			[
				'param_name'  => 'cat_1',
				'type'        => 'dropdown',
				'value'       => \td_util::get_category2id_array(),
				'heading'     => __( 'Parent Category ID', $this->text_domain ),
				'description' => __( 'Inset the parent category ID', $this->text_domain ),
				'holder'      => 'div',
				'class'       => 'tdc-textfield-small'
			],
			[
				'param_name'  => 'cat_1_children',
				'type'        => 'textfield',
				'value'       => '',
				'heading'     => __( 'Children:', $this->text_domain ),
				'description' => __( 'List of child category ID\'s separated by commas. E.g. 1, 2, 3', $this->text_domain ),
				'holder'      => 'div',
				'class'       => 'tdc-textfield-extrabig'
			],
			[
				'param_name' => 'separator',
				'type'       => 'text_separator',
				'heading'    => __( 'Category 2 Settings', $this->text_domain ),
				'value'      => '',
				'class'      => '',
			],
			[
				'param_name'  => 'cat_2',
				'type'        => 'dropdown',
				'value'       => \td_util::get_category2id_array(),
				'heading'     => __( 'Parent Category ID', $this->text_domain ),
				'description' => __( 'Inset the parent category ID', $this->text_domain ),
				'holder'      => 'div',
				'class'       => 'tdc-textfield-small'
			],
			[
				'param_name'  => 'cat_2_children',
				'type'        => 'textfield',
				'value'       => '',
				'heading'     => __( 'Children:', $this->text_domain ),
				'description' => __( 'List of child category ID\'s separated by commas. E.g. 1, 2, 3', $this->text_domain ),
				'holder'      => 'div',
				'class'       => 'tdc-textfield-extrabig'
			]
		];

		\td_api_block::add( 'td_block_selfhacked_substances',
			[
				'map_in_visual_composer' => true,
				'map_in_td_composer'     => true,
				"name"                   => __( 'Selfhacked Substances', $this->text_domain ),
				"base"                   => 'td_block_selfhacked_substances',
				"class"                  => 'td_block_selfhacked_substances',
				"controls"               => 'full',
				"category"               => 'Blocks',
				'tdc_category'           => 'Blocks',
				'file'                   => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_selfhacked_substances.php',
				'text_domain'            => $this->text_domain,
				'params'                 => array_merge(
					\td_config::get_map_block_general_array(),
					$block_general,
					\td_config::get_map_filter_array()
				)
			]
		);

		$block_general = [
			[
				'param_name' => 'separator',
				'type'       => 'text_separator',
				'heading'    => __( 'Category Settings', $this->text_domain ),
				'value'      => '',
				'class'      => '',
			],
			[
				'param_name'  => 'cat_1',
				'type'        => 'dropdown',
				'value'       => \td_util::get_category2id_array(),
				'heading'     => __( 'Parent Category ID', $this->text_domain ),
				'description' => __( 'Inset the parent category ID', $this->text_domain ),
				'holder'      => 'div',
				'class'       => 'tdc-textfield-small'
			],
			[
				'param_name'  => 'cat_2',
				'type'        => 'dropdown',
				'value'       => \td_util::get_category2id_array(),
				'heading'     => __( 'Parent Category ID', $this->text_domain ),
				'description' => __( 'Inset the parent category ID', $this->text_domain ),
				'holder'      => 'div',
				'class'       => 'tdc-textfield-small'
			]
		];

		\td_api_block::add( 'td_block_top_substances',
			[
				'map_in_visual_composer' => true,
				'map_in_td_composer'     => true,
				"name"                   => __( 'Selfhacked Top Substances', $this->text_domain ),
				"base"                   => 'td_block_top_substances',
				"class"                  => 'td_block_top_substances',
				"controls"               => 'full',
				"category"               => 'Blocks',
				'tdc_category'           => 'Blocks',
				'file'                   => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_top_substances.php',
				'text_domain'            => $this->text_domain,
				'params'                 => array_merge(
					\td_config::get_map_block_general_array(),
					$block_general,
					\td_config::get_map_filter_array()
				)
			]
		);

		\td_api_block::add( 'td_block_selfhacked_start_here',
			[
				'map_in_visual_composer' => false,
				'map_in_td_composer'     => true,
				'name'                   => 'Selfhacked' . ' - ' . __( 'Start Here', $this->text_domain ),
				'base'                   => 'td_block_selfhacked_start_here',
				'tdc_category'           => 'Blocks',
				'is_container'           => false,
				'class'                  => '',
				'controls'               => 'full',
				'category'               => 'Blocks',
				'file'                   => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_selfhacked_start_here.php',
				'text_domain'            => $this->text_domain,
				'params' => [
					[
						'param_name' => 'extra_css_class',
						'type' => 'textfield',
						'value' => '',
						'heading' => 'Extra Css Class',
						'description' => '',
						'class' => '',
					],
				],
			]
		);

		\td_api_block::add( 'td_block_selfhacked_check_out_lta',
			[
				'map_in_visual_composer' => false,
				'map_in_td_composer'     => true,
				'name'                   => 'Selfhacked' . ' - ' . __( 'Check out Lat Test Analyzer', $this->text_domain ),
				'base'                   => 'td_block_selfhacked_check_out_lta',
				'tdc_category'           => 'Blocks',
				'is_container'           => false,
				'class'                  => '',
				'controls'               => 'full',
				'category'               => 'Blocks',
				'file'                   => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_selfhacked_check_out_lta.php',
				'text_domain'            => $this->text_domain,
				'params' => [
					[
						'param_name' => 'extra_css_class',
						'type' => 'textfield',
						'value' => '',
						'heading' => 'Extra Css Class',
						'description' => '',
						'class' => '',
					],
				],
			]
		);

		\td_api_block::add( 'td_block_selfhacked_check_out_sd',
			[
				'map_in_visual_composer' => false,
				'map_in_td_composer'     => true,
				'name'                   => 'Selfhacked' . ' - ' . __( 'Check out Self Decode', $this->text_domain ),
				'base'                   => 'td_block_selfhacked_check_out_sd',
				'tdc_category'           => 'Blocks',
				'is_container'           => false,
				'class'                  => '',
				'controls'               => 'full',
				'category'               => 'Blocks',
				'file'                   => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_selfhacked_check_out_sd.php',
				'text_domain'            => $this->text_domain,
				'params' => [
					[
						'param_name' => 'extra_css_class',
						'type' => 'textfield',
						'value' => '',
						'heading' => 'Extra Css Class',
						'description' => '',
						'class' => '',
					],
				],
			]
		);

		\td_api_block::add( 'td_block_selfhacked_contact_us',
			[
				'map_in_visual_composer' => false,
				'map_in_td_composer'     => true,
				'name'                   => 'Selfhacked' . ' - ' . __( 'Contact Us', $this->text_domain ),
				'base'                   => 'td_block_selfhacked_contact_us',
				'tdc_category'           => 'Blocks',
				'is_container'           => false,
				'class'                  => '',
				'controls'               => 'full',
				'category'               => 'Blocks',
				'file'                   => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_selfhacked_contact_us.php',
				'text_domain'            => $this->text_domain,
				'params' => [
					[
						'param_name' => 'extra_css_class',
						'type' => 'textfield',
						'value' => '',
						'heading' => 'Extra Css Class',
						'description' => '',
						'class' => '',
					],
				],
			]
		);

		\td_api_block::add( 'td_block_selfhacked_book_ad',
			[
				'map_in_visual_composer' => false,
				'map_in_td_composer'     => true,
				'name'                   => 'Selfhacked' . ' - ' . __( 'Book Ad', $this->text_domain ),
				'base'                   => 'td_block_selfhacked_book_ad',
				'tdc_category'           => 'Blocks',
				'is_container'           => false,
				'class'                  => '',
				'controls'               => 'full',
				'category'               => 'Blocks',
				'file'                   => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_selfhacked_book_ad.php',
				'text_domain'            => $this->text_domain,
				'params' => $store_params
			]
		);

		\td_api_block::add( 'td_block_selfhacked_cf7',
			[
				'map_in_visual_composer' => false,
				'map_in_td_composer'     => true,
				'name'                   => 'Selfhacked' . ' - ' . __( 'Contact form 7', $this->text_domain ),
				'base'                   => 'td_block_selfhacked_cf7',
				'tdc_category'           => 'Blocks',
				'is_container'           => false,
				'class'                  => '',
				'controls'               => 'full',
				'category'               => 'Blocks',
				'file'                   => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_selfhacked_cf7.php',
				'text_domain'            => $this->text_domain,
				'params' => [
					[
						'param_name' => 'id',
						'type' => 'textfield',
						'value' => '',
						'heading' => 'Contact form 7 ID',
						'description' => 'ID of the form from Contact Form 7 to appear',
						'class' => '',
					],
				],
			]
		);

		\td_api_block::add( 'td_block_selfhacked_genetics',
			[
				'map_in_visual_composer' => true,
				'map_in_td_composer'     => true,
				"name"                   => __( 'Selfhacked Genetics', $this->text_domain ),
				"base"                   => 'td_block_selfhacked_genetics',
				"class"                  => 'td_block_selfhacked_genetics',
				"controls"               => "full",
				"category"               => 'Blocks',
				'tdc_category'           => 'Blocks',
				'file'                   => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_selfhacked_genetics.php',
				'text_domain'            => $this->text_domain,
				"params"                 => array_merge(
					\td_config::get_map_block_general_array(),
					\td_config::get_map_filter_array(),
					\td_config::get_map_block_ajax_filter_array(),
					\td_config::get_map_block_pagination_array()
				)
			]
		);



		\td_api_block::add( 'td_block_description_1',
			[
				'map_in_visual_composer' => true,
				'map_in_td_composer'     => true,
				'name'         => __( 'Selfhacked Description 1', $this->text_domain ),
				'base'         => 'td_block_description_1',
				'class'        => 'td_block_description_1',
				'controls'     => 'full',
				'category'     => 'Blocks',
				'tdc_category' => 'Blocks',
				'file'         => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_description_1.php',
				'text_domain'  => $this->text_domain,
				'params'       => array_merge(
					\td_config::get_map_block_general_array(),
					[
						[
							'param_name' => 'separator',
							'type'       => 'text_separator',
							'heading'    => __( 'General settings', $this->text_domain ),
							'value'      => '',
							'class'      => '',
						],
						[
							'type'        => 'attach_image',
							'heading'     => __( 'Column 1 Image', $this->text_domain ),
							'param_name'  => 'image_1',
							'value'       => '',
							'description' => __( 'Choose an image from the media library', $this->text_domain ),
							'dependency'  => [
								'element' => 'source',
								'value'   => 'media_library',
							],
							'admin_label' => true,
						],
						[
							'param_name'  => 'description_1',
							'type'        => 'textfield',
							'value'       => '',
							'heading'     => __( 'Column 1 Text:', $this->text_domain ),
							'description' => __( 'The text in the first column', $this->text_domain ),
							'holder'      => 'div',
							'class'       => 'tdc-textfield-extrabig'
						],
						[
							'type'        => 'attach_image',
							'heading'     => __( 'Column 2 Image', $this->text_domain ),
							'param_name'  => 'image_2',
							'value'       => '',
							'description' => __( 'Choose an image from the media library', $this->text_domain ),
							'dependency'  => [
								'element' => 'source',
								'value'   => 'media_library',
							],
							'admin_label' => true,
						],
						[
							'param_name'  => 'description_2',
							'type'        => 'textfield',
							'value'       => '',
							'heading'     => __( 'Column 2 Text:', $this->text_domain ),
							'description' => __( 'The text in the second column', $this->text_domain ),
							'holder'      => 'div',
							'class'       => 'tdc-textfield-extrabig'
						],
						[
							'type'        => 'attach_image',
							'heading'     => __( 'Column 3 Image', $this->text_domain ),
							'param_name'  => 'image_3',
							'value'       => '',
							'description' => __( 'Choose an image from the media library', $this->text_domain ),
							'dependency'  => [
								'element' => 'source',
								'value'   => 'media_library',
							],
							'admin_label' => true,
						],
						[
							'param_name'  => 'description_3',
							'type'        => 'textfield',
							'value'       => '',
							'heading'     => __( 'Column 3 Text:', $this->text_domain ),
							'description' => __( 'The text in the third column', $this->text_domain ),
							'holder'      => 'div',
							'class'       => 'tdc-textfield-extrabig'
						],
						[
							'type'        => 'attach_image',
							'heading'     => __( 'Column 4 Image', $this->text_domain ),
							'param_name'  => 'image_4',
							'value'       => '',
							'description' => __( 'Choose an image from the media library', $this->text_domain ),
							'dependency'  => [
								'element' => 'source',
								'value'   => 'media_library',
							],
							'admin_label' => true,
						],
						[
							'param_name'  => 'description_4',
							'type'        => 'textfield',
							'value'       => '',
							'heading'     => __( 'Column 4 Text:', $this->text_domain ),
							'description' => __( 'The text in the fourth column', $this->text_domain ),
							'holder'      => 'div',
							'class'       => 'tdc-textfield-extrabig'
						]
					]
				)
			]
		);
	}

	protected function add_thumbnails() {
		\td_api_thumb::add( 'td_540x170',
			[
				'name'                  => 'td_540x170',
				'width'                 => 540,
				'height'                => 170,
				'crop'                  => [ 'center', 'top' ],
				'post_format_icon_size' => 'small',
				'no_image_path'         => $this->wp_theme->child_base_path . '/assets/img/post_grid_blue_bg.png',
				'used_on'               => [
					'Selfhacked'
				]
			]
		);
		\td_api_thumb::add( 'td_540x365',
			[
				'name'                  => 'td_540x365',
				'width'                 => 540,
				'height'                => 365,
				'crop'                  => [ 'center', 'center' ],
				'post_format_icon_size' => 'small',
				'no_image_path'         => $this->wp_theme->child_base_path . '/assets/img/post_grid_blue_bg.png',
				'used_on'               => [
					'Selfhacked'
				]
			]
		);
		\td_api_thumb::add( 'td_240x360',
			[
				'name'                  => 'td_240x360',
				'width'                 => 240,
				'height'                => 360,
				'crop'                  => [ 'center', 'center' ],
				'post_format_icon_size' => 'small',
				'no_image_path'         => $this->wp_theme->child_base_path . '/assets/img/post_grid_blue_bg.png',
				'used_on'               => [
					'Selfhacked'
				]
			]
		);
	}

	protected function add_block_templates() {

		$text_color_heading       = __( 'Title text color:', $this->text_domain );
		$text_color_description   = __( 'Optional - Choose a custom title text color for this header', $this->text_domain );
		$border_color_heading     = __( 'Border color:', $this->text_domain );
		$border_color_description = __( 'Optional - Choose a custom border color for this header', $this->text_domain );
		$accent_color_heading     = __( 'Accent hover color:', $this->text_domain );
		$accent_color_description = __( 'Optional - Choose a custom accent hover color for this block', $this->text_domain );

		\td_api_block_template::add( 'td_block_template_18',
			[
				'text'   => __( 'Block Header 18', $this->text_domain ) . ' - ' . __( 'Most Recent', $this->text_domain ),
				'file'   => $this->wp_theme->child_base_path . '/includes/block_templates/td_block_template_18.php',
				'text_domain' => $this->text_domain,
				'params' => [
					[
						"type"        => "colorpicker",
						"holder"      => "div",
						"class"       => "",
						"heading"     => $text_color_heading,
						"param_name"  => "header_text_color",
						"value"       => '',
						"description" => $text_color_description,
						'td_type'     => 'block_template',
					],
					[
						"type"        => "colorpicker",
						"holder"      => "div",
						"class"       => "",
						"heading"     => $border_color_heading,
						"param_name"  => "border_color",
						"value"       => '',
						"description" => $border_color_description,
						'td_type'     => 'block_template',
					],
					[
						"type"        => "colorpicker",
						"holder"      => "div",
						"class"       => "",
						"heading"     => $accent_color_heading,
						"param_name"  => "accent_text_color",
						"value"       => '',
						"description" => $accent_color_description,
						'td_type'     => 'block_template',
					]
				]
			]
		);

		\td_api_block_template::add( 'td_block_template_19',
			[
				'text'   => __( 'Block Header 19', $this->text_domain ) . ' - ' . __( 'Custom', $this->text_domain ),
				'file'   => $this->wp_theme->child_base_path . '/includes/block_templates/td_block_template_19.php',
				'text_domain' => $this->text_domain,
				'params' => [
					[
						"type"        => "colorpicker",
						"holder"      => "div",
						"class"       => "",
						"heading"     => $text_color_heading,
						"param_name"  => "header_text_color",
						"value"       => '',
						"description" => $text_color_description,
						'td_type'     => 'block_template',
					],
					[
						"type"        => "colorpicker",
						"holder"      => "div",
						"class"       => "",
						"heading"     => $border_color_heading,
						"param_name"  => "border_color",
						"value"       => '',
						"description" => $border_color_description,
						'td_type'     => 'block_template',
					],
					[
						"type"        => "colorpicker",
						"holder"      => "div",
						"class"       => "",
						"heading"     => $accent_color_heading,
						"param_name"  => "accent_text_color",
						"value"       => '',
						"description" => $accent_color_description,
						'td_type'     => 'block_template',
					]
				]
			]
		);

		\td_api_block_template::add( 'td_block_template_20',
			[
				'text'   => __( 'Block Header 20', $this->text_domain ) . ' - ' . __( 'Trending', $this->text_domain ),
				'file'   => $this->wp_theme->child_base_path . '/includes/block_templates/td_block_template_20.php',
				'text_domain' => $this->text_domain,
				'params' => [
					[
						"type"        => "colorpicker",
						"holder"      => "div",
						"class"       => "",
						"heading"     => $text_color_heading,
						"param_name"  => "header_text_color",
						"value"       => '',
						"description" => $text_color_description,
						'td_type'     => 'block_template',
					],
					[
						"type"        => "colorpicker",
						"holder"      => "div",
						"class"       => "",
						"heading"     => $border_color_heading,
						"param_name"  => "border_color",
						"value"       => '',
						"description" => $border_color_description,
						'td_type'     => 'block_template',
					],
					[
						"type"        => "colorpicker",
						"holder"      => "div",
						"class"       => "",
						"heading"     => $accent_color_heading,
						"param_name"  => "accent_text_color",
						"value"       => '',
						"description" => $accent_color_description,
						'td_type'     => 'block_template',
					]
				]
			]
		);

		\td_api_block_template::add( 'td_block_template_21',
			[
				'text'   => __( 'Block Header 21', $this->text_domain ) . ' - ' . __( 'Custom', $this->text_domain ),
				'file'   => $this->wp_theme->child_base_path . '/includes/block_templates/td_block_template_21.php',
				'text_domain' => $this->text_domain,
				'params' => [
					[
						"type"        => "colorpicker",
						"holder"      => "div",
						"class"       => "",
						"heading"     => $text_color_heading,
						"param_name"  => "header_text_color",
						"value"       => '',
						"description" => $text_color_description,
						'td_type'     => 'block_template',
					],
					[
						"type"        => "colorpicker",
						"holder"      => "div",
						"class"       => "",
						"heading"     => $border_color_heading,
						"param_name"  => "border_color",
						"value"       => '',
						"description" => $border_color_description,
						'td_type'     => 'block_template',
					],
					[
						"type"        => "colorpicker",
						"holder"      => "div",
						"class"       => "",
						"heading"     => $accent_color_heading,
						"param_name"  => "accent_text_color",
						"value"       => '',
						"description" => $accent_color_description,
						'td_type'     => 'block_template',
					]
				]
			]
		);
	}

	protected function add_category_templates() {
		\td_api_category_template::add( 'td_category_template_9',
			[
				'text'        => __( 'Style 9 - Selfhacked', $this->text_domain ),
				'file'        => $this->wp_theme->child_base_path . '/includes/category_templates/td_category_template_9.php',
				'text_domain' => $this->text_domain
			]
		);
	}

	protected function add_footer_templates() {
		\td_api_footer_template::add( 'td_footer_template_15',
			[
				'text'        => __( 'Style 15 - Selfhacked', $this->text_domain ),
				'file'        => $this->wp_theme->child_base_path . '/parts/footer/td_footer_template_15.php',
				'text_domain' => $this->text_domain
			]
		);
	}

	protected function add_sub_footer_templates() {
		\td_api_sub_footer_template::add( 'td_sub_footer_template_2',
			[
				'text'        => __( 'Style 2 - Selfhacked', $this->text_domain ),
				'file'        => $this->wp_theme->child_base_path . '/parts/footer/td_sub_footer_template_2.php',
				'text_domain' => $this->text_domain
			]
		);
	}

	protected function update_blocks() {
		\td_api_block::update( 'td_block_title',
			[
				'map_in_visual_composer' => false,
				'map_in_td_composer'     => true,
				"base"                   => "td_block_title",
				'name'                   => __( 'Title', $this->text_domain ),
				"class"                  => "",
				"controls"               => "full",
				"category"               => 'Blocks',
				'tdc_category'           => 'Extended',
				'icon'                   => 'icon-pagebuilder-title',
				'text_domain'            => $this->text_domain,
				'file'                   => \td_global::$get_template_directory . '/includes/shortcodes/td_block_title.php',
				'tdc_style_params'       => [
					'custom_title',
					'custom_url',
					'el_class'
				],
				'tdc_start_values'       => base64_encode(
					json_encode(
						[
							[
								'title_tag' => 'h4',
							]
						]
					)
				),
				"params"                 => array_merge(
					\td_config::get_map_block_general_array(),
					\td_config_helper::get_map_block_font_array( 'f_header', true, 'Block header' ),
					[
						[
							"param_name" => "separator",
							"type"       => "horizontal_separator",
							"value"      => "",
							"class"      => ""
						],
						[
							"param_name"  => "title_tag",
							"type"        => "dropdown",
							"value"       => [
								'H1'           => 'h1',
								'H2'           => 'h2',
								'H3'           => 'h3',
								'H4 - Default' => 'h4',
							],
							"heading"     => 'Title tag (SEO)',
							"description" => "",
							"holder"      => "div",
							"class"       => "tdc-dropdown-big",
						],
						[
							"param_name"          => "content_align_horizontal",
							"type"                => "dropdown",
							"value"               => [
								'Default' => 'layout-default',
								'Left'    => 'content-horiz-left',
								'Center'  => 'content-horiz-center',
								'Right'   => 'content-horiz-right'
							],
							"heading"             => 'Horizontal align',
							"description"         => "",
							"holder"              => "div",
							'tdc_dropdown_images' => true,
							"class"               => "tdc-visual-selector tdc-add-class",
						],
						[
							"param_name" => "separator",
							"type"       => "horizontal_separator",
							"value"      => "",
							"class"      => ""
						],
						[
							'param_name'  => 'el_class',
							'type'        => 'textfield',
							'value'       => '',
							'heading'     => 'Extra class',
							'description' => 'Style particular content element differently - add a class name and refer to it in custom CSS',
							'class'       => 'tdc-textfield-extrabig',
						],
						[
							'param_name' => 'css',
							'value'      => '',
							'type'       => 'css_editor',
							'heading'    => 'Css',
							'group'      => 'Design options',
						],
						[
							'param_name' => 'tdc_css',
							'value'      => '',
							'type'       => 'tdc_css_editor',
							'heading'    => '',
							'group'      => 'Design options',
						],
					]
				)
			]
		);

		\td_api_block::update( 'td_block_mega_menu',
			[
				'text_domain' => $this->text_domain,
				'map_in_visual_composer' => false,
				'file'                   => $this->wp_theme->child_base_path . '/includes/shortcodes/td_block_selfhacked_mega_menu.php',

				// key added to supply 'show_child_cat' differently for each theme
				'render_atts'            => [
					'show_child_cat' => 30,
				]
			]
		);
	}

	/**
	 * For updating Newspaper Modules
	 */
	protected function update_modules() {
		\td_api_module::update( 'td_module_mega_menu',
			[
				'text_domain' => $this->text_domain,
				'file'                         => $this->wp_theme->child_base_path . '/includes/modules/td_module_selfhacked_mega_menu.php',
				'text'                         => 'Mega menu module',
				'img'                          => '',
				'used_on_blocks'               => [ 'td_block_mega_menu' ],
				'excerpt_title'                => '',
				'excerpt_content'              => '',
				'enabled_on_more_articles_box' => false,
				'enabled_on_loops'             => false,
				'uses_columns'                 => false,
				// if the module uses columns on the page template + loop
				'category_label'               => true,
				'class'                        => '',
				'group'                        => ''
				// '' - main theme, 'mob' - mobile theme, 'woo' - woo theme
			]
		);
	}
}
