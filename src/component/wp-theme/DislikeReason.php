<?php
/**
 * Created by PhpStorm.
 * User: allhe
 * Date: 11/30/2018
 * Time: 03:55
 */

namespace SelfHacked\SelfHackedNewspaper\Component\WpTheme;

use Enpii\Wp\EnpiiBase\Base\BaseComponent;
use SelfHacked\SelfHackedNewspaper\Component\WpTheme;

class DislikeReason extends BaseComponent {
    /**
     * @var string $text_domain
     */
    public $text_domain = null;

    /** @var WpTheme|null $wp_theme */
    public $wp_theme = null;

    /**
     * Initialize all Extra Modules
     */
    public function initialize() {
        $this->wp_theme = $this->get_app_instance()->get_wp_theme();
        $this->init_hooks();
    }

    /**
     * Initialize all hooks needed
     */
    public function init_hooks() {
        add_action( 'wp_ajax_dislike_reason', [ $this, 'dislike_reason_ajax'] );
        add_action( 'wp_ajax_nopriv_dislike_reason', [ $this, 'dislike_reason_ajax'] );
    }

    public function dislike_reason_ajax() {
        if ( !$_REQUEST['post_id'] ) {
            die();
        }



        $post_id = (int) $_REQUEST['post_id'];
        $reason_0 = $_REQUEST['reason_0'] ? (bool) $_REQUEST['reason_0']: false;
        $reason_1 = $_REQUEST['reason_1'] ? (bool) $_REQUEST['reason_1']: false;
        $reason_2 = $_REQUEST['reason_2'] ? (bool) $_REQUEST['reason_2']: false;
        $reason_3 = $_REQUEST['reason_3'] ? (bool) $_REQUEST['reason_3']: false;
        $reason_4 = $_REQUEST['reason_4'] ? (bool) $_REQUEST['reason_4']: false;
        $reason_5 = $_REQUEST['reason_5'] ? (bool) $_REQUEST['reason_5']: false;
        $reason_6 = $_REQUEST['reason_6'] ? (bool) $_REQUEST['reason_6']: false;
        $reason_7 = $_REQUEST['reason_7'] ? (bool) $_REQUEST['reason_7']: false;
        $other_reason = $_REQUEST['other_reason'] ? (string) $_REQUEST['other_reason']: '';

        global $wpdb;

        $table_name = $wpdb->prefix . 'dislike_reasons';
        if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            $charset_collate = $wpdb->get_charset_collate();

            $sql = 'CREATE TABLE ' . $table_name . ' ('
                   . '    id INT NOT NULL,'
                   . '    post_id INT NOT NULL,'
                   . '    reason_0 bool DEFAULT 0 NOT NULL,'
                   . '    reason_1 bool DEFAULT 0 NOT NULL,'
                   . '    reason_2 bool DEFAULT 0 NOT NULL,'
                   . '    reason_3 bool DEFAULT 0 NOT NULL,'
                   . '    reason_4 bool DEFAULT 0 NOT NULL,'
                   . '    reason_5 bool DEFAULT 0 NOT NULL,'
                   . '    reason_6 bool DEFAULT 0 NOT NULL,'
                   . '    reason_7 bool DEFAULT 0 NOT NULL,'
                   . '    other_reason text DEFAULT NULL,'
                   . '    PRIMARY KEY  (id)'
                   . ') ' . $charset_collate . ';';

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        }

        $ip_address = null;

        if (empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip_address = $_SERVER["REMOTE_ADDR"];
        } else {
            $ip_address = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        if(strpos($ip_address, ',') !== false) {
            $ip_address = explode(',', $ip_address);
            $ip_address = $ip_address[0];
        }

        $rating_id = $wpdb->get_var($wpdb->prepare('SELECT rating_id FROM ' . $wpdb->ratings . ' WHERE rating_postid = %d AND rating_ip = %s', $post_id, esc_attr($ip_address) ));

        $wpdb->insert(
            $table_name,
            [
                'id'           => $rating_id,
                'post_id'      => $post_id,
                'reason_0'     => $reason_0,
                'reason_1'     => $reason_1,
                'reason_2'     => $reason_2,
                'reason_3'     => $reason_3,
                'reason_4'     => $reason_4,
                'reason_5'     => $reason_5,
                'reason_6'     => $reason_6,
                'reason_7'     => $reason_7,
                'other_reason' => $other_reason
            ],
            [ '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%s' ]
        );
        die();
    }

    public static function display_page() {
        if ( ! current_user_can( 'manage_ratings' ) ) {
            wp_die( __( 'Access Denied', 'wp-postratings' ) );
        }

        global $wpdb;


        $base_name                  = plugin_basename( 'wp-postratings/postratings-reasons.php' );
        $base_page                  = admin_url( 'admin.php?page=' . urlencode( $base_name ) );
        $postratings_sort_url       = '';
        $postratings_sortby_text    = '';
        $postratings_sortorder_text = '';
        $ratings_image              = esc_attr( get_option( 'postratings_image' ) );
        $ratings_max                = intval( get_option( 'postratings_max' ) );

// Handle $_GET values
        $postratings_filterid     = isset( $_GET['id'] )           ? intval( $_GET['id'] )                : 0;
        $postratings_filterrating = isset( $_GET['rating'] )       ? intval( $_GET['rating'] )            : 0;
        $postratings_filteruser   = isset( $_GET['user'] )         ? sanitize_text_field( $_GET['user'] ) : '';
        $postratings_log_perpage  = isset( $_GET['perpage'] )      ? intval( $_GET['perpage'] )           : 20;
        $postratings_page         = ! empty( $_GET['ratingpage'] ) ? intval( $_GET['ratingpage'] )        : 1;
        $postratings_sortby       = 'rating_timestamp';
        $postratings_sortorder    = 'DESC';

// For BY and ORDER, only accept data from a finite list of known and trusted values.
        if ( isset( $_GET['by'] ) && in_array( $_GET['by'], array( 'date', 'host', 'id', 'ip', 'postid', 'posttitle', 'rating', 'username', ) ) )
            $postratings_sortby = $_GET['by'];

        if ( isset( $_GET['order'] ) && in_array( $_GET['order'], array( 'asc', 'desc', ) ) )
            $postratings_sortorder = $_GET['order'];


### Form Processing
        if ( ! empty( $_POST['do'] ) ) {

            check_admin_referer('wp-postratings_logs');
            $delete_datalog   = isset( $_POST['delete_datalog'] ) ? (int) $_POST['delete_datalog'] : 1;
            $post_ids         = '';
            $post_ids_list    = array();
            $ratings_postmeta = array( 'ratings_users', 'ratings_score', 'ratings_average', );

            // delete_postid is either a comma-separated list of integers, or "all".
            if ( ! empty( $_POST['delete_postid'] ) ) {

                // "all" is the only string value accepted
                if ( $_POST['delete_postid'] != 'all' ) {
                    $post_ids_list = wp_parse_id_list( $_POST['delete_postid'] );
                    $post_ids      = implode( ',', $post_ids_list );
                }
                else {
                    $post_ids = 'all';
                }
            }

            switch($delete_datalog) {
                case 1:
                    if($post_ids == 'all') {
                        $delete_logs = $wpdb->query("DELETE FROM $wpdb->ratings");
                        if($delete_logs) {
                            $text = '<p style="color: green;">'.esc_html__('All Rating Logs Have Been Deleted.', 'wp-postratings').'</p>';
                        } else {
                            $text = '<p style="color: red;">'.esc_html__('An Error Has Occured While Deleting All Rating Logs.', 'wp-postratings').'</p>';
                        }

                    } else {
                        $delete_logs = $wpdb->query( "DELETE FROM {$wpdb->ratings} WHERE rating_postid IN (" . $post_ids . ')' );
                        if($delete_logs) {
                            $text = '<p style="color: green;">'.sprintf(esc_html__('All Rating Logs For Post ID(s) %s Have Been Deleted.', 'wp-postratings'), $post_ids).'</p>';
                        } else {
                            $text = '<p style="color: red;">'.sprintf(esc_html__('An Error Has Occured While Deleting All Rating Logs For Post ID(s) %s.', 'wp-postratings'), $post_ids).'</p>';
                        }
                    }
                    break;

                case 2:
                    // @todo Deleting meta records like will not clear the post's object cache
                    if($post_ids == 'all') {
                        foreach($ratings_postmeta as $postmeta) {
                            $wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->postmeta} WHERE meta_key = %s", $postmeta ) );
                            $text .= '<p style="color: green;">'.sprintf(esc_html__('Rating Data "%s" Has Been Deleted.', 'wp-postratings'), "<strong><em>$postmeta</em></strong>").'</p>';
                        }

                    } else {
                        foreach ( $post_ids_list as $the_post_id ) {
                            foreach( $ratings_postmeta as $meta_key ) {
                                delete_post_meta( $the_post_id, $meta_key );

                                $text .= '<p style="color: green;">'.sprintf(esc_html__('Rating Data "%s" For Post ID(s) %s Has Been Deleted.', 'wp-postratings'), "<strong><em>$meta_key</em></strong>", $post_ids).'</p>';
                            }
                        }
                    }
                    break;

                case 3:
                    if($post_ids == 'all') {
                        $delete_logs = $wpdb->query("DELETE FROM $wpdb->ratings");
                        if($delete_logs) {
                            $text = '<p style="color: green;">'.esc_html__('All Rating Logs Have Been Deleted.', 'wp-postratings').'</p>';
                        } else {
                            $text = '<p style="color: red;">'.esc_html__('An Error Has Occured While Deleting All Rating Logs.', 'wp-postratings').'</p>';
                        }

                        // @todo Deleting meta records like will not clear the post's object cache
                        foreach($ratings_postmeta as $postmeta) {
                            $wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->postmeta} WHERE meta_key = %s", $postmeta ) );
                            $text .= '<p style="color: green;">'.sprintf(esc_html__('Rating Data "%s" Has Been Deleted.', 'wp-postratings'), "<strong><em>$postmeta</em></strong>").'</p>';
                        }

                    } else {
                        $delete_logs = $wpdb->query( "DELETE FROM {$wpdb->ratings} WHERE rating_postid IN (" . $post_ids . ')' );
                        if($delete_logs) {
                            $text = '<p style="color: green;">'.sprintf(esc_html__('All Rating Logs For Post ID(s) %s Have Been Deleted.', 'wp-postratings'), $post_ids).'</p>';
                        } else {
                            $text = '<p style="color: red;">'.sprintf(esc_html__('An Error Has Occured While Deleting All Rating Logs For Post ID(s) %s.', 'wp-postratings'), $post_ids).'</p>';
                        }

                        foreach ( $post_ids_list as $the_post_id ) {
                            foreach( $ratings_postmeta as $meta_key ) {
                                delete_post_meta( $the_post_id, $meta_key );

                                $text .= '<p style="color: green;">'.sprintf(esc_html__('Rating Data "%s" For Post ID(s) %s Has Been Deleted.', 'wp-postratings'), "<strong><em>$meta_key</em></strong>", $post_ids).'</p>';
                            }
                        }
                    }
                    break;
            }
        }  // if ( ! empty( $_POST['do'] ) )

### Form Sorting URL
// @todo Use add_query_arg() for these
        if(!empty($postratings_filterid)) {
            $postratings_sort_url .= '&amp;id='.$postratings_filterid;
        }
        if(!empty($postratings_filteruser)) {
            $postratings_sort_url .= '&amp;user='.urlencode( $postratings_filteruser );
        }
        if ( ! empty( $postratings_filterrating ) ) {
            $postratings_sort_url .= '&amp;rating='.$postratings_filterrating;
        }
        if(!empty($postratings_sortby)) {
            $postratings_sort_url .= '&amp;by='.urlencode( $postratings_sortby );
        }
        if(!empty($postratings_sortorder)) {
            $postratings_sort_url .= '&amp;order='.urlencode( $postratings_sortorder );
        }
        if(!empty($postratings_log_perpage)) {
            $postratings_sort_url .= '&amp;perpage='.$postratings_log_perpage;
        }


### Get Order By
        switch($postratings_sortby) {
            case 'id':
                $postratings_sortby = 'rating_id';
                $postratings_sortby_text = __('ID', 'wp-postratings');
                break;
            case 'username':
                $postratings_sortby = 'rating_username';
                $postratings_sortby_text = __('Username', 'wp-postratings');
                break;
            case 'rating':
                $postratings_sortby = 'rating_rating';
                $postratings_sortby_text = __('Rating', 'wp-postratings');
                break;
            case 'postid':
                $postratings_sortby = 'rating_postid';
                $postratings_sortby_text = __('Post ID', 'wp-postratings');
                break;
            case 'posttitle':
                $postratings_sortby = 'rating_posttitle';
                $postratings_sortby_text = __('Post Title', 'wp-postratings');
                break;
            case 'ip':
                $postratings_sortby = 'rating_ip';
                $postratings_sortby_text = __('IP', 'wp-postratings');
                break;
            case 'host':
                $postratings_sortby = 'rating_host';
                $postratings_sortby_text = __('Host', 'wp-postratings');
                break;
            case 'date':
            default:
                $postratings_sortby = 'rating_timestamp';
                $postratings_sortby_text = __('Date', 'wp-postratings');
        }


### Get Sort Order
        if ( $postratings_sortorder == 'ASC' )
            $postratings_sortorder_text = __('Ascending', 'wp-postratings');
        else
            $postratings_sortorder_text = __('Descending', 'wp-postratings');

// Where
        $postratings_where = '';
        if ( $postratings_filterid )
            $postratings_where = $wpdb->prepare( "AND rating_postid = %d", $postratings_filterid );;

        if ( ! empty( $postratings_filteruser ) )
            $postratings_where .= $wpdb->prepare( " AND rating_username = %s", $postratings_filteruser );

        if ( ! empty( $postratings_filterrating ) )
            $postratings_where .= $wpdb->prepare( " AND rating_rating = %d", $postratings_filterrating );

// Get Post Ratings Logs Data
        $total_ratings = intval($wpdb->get_var("SELECT COUNT(rating_id) FROM $wpdb->ratings WHERE 1=1 $postratings_where"));
        $total_users = intval($wpdb->get_var("SELECT SUM(meta_value) FROM $wpdb->postmeta WHERE meta_key = 'ratings_users'"));
        $total_score = intval($wpdb->get_var("SELECT SUM((meta_value+0.00)) FROM $wpdb->postmeta WHERE meta_key = 'ratings_score'"));
        $ratings_custom = intval(get_option('postratings_customrating'));
        if($total_users == 0) {
            $total_average = 0;
        } else {
            $total_average = $total_score/$total_users;
        }

// Determin $offset
        $offset = ($postratings_page-1) * $postratings_log_perpage;

// Determine Max Number Of Ratings To Display On Page
        if(($offset + $postratings_log_perpage) > $total_ratings) {
            $max_on_page = $total_ratings;
        } else {
            $max_on_page = ($offset + $postratings_log_perpage);
        }

// Determine Number Of Ratings To Display On Page
        if (($offset + 1) > ($total_ratings)) {
            $display_on_page = $total_ratings;
        } else {
            $display_on_page = ($offset + 1);
        }

// Determing Total Amount Of Pages
        $total_pages = ceil($total_ratings / $postratings_log_perpage);

// Get The Logs
        $postratings_logs = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->ratings} WHERE 1=1 {$postratings_where} ORDER BY {$postratings_sortby} {$postratings_sortorder} LIMIT %d, %d", $offset, $postratings_log_perpage ) );
        ?>
        <?php if(!empty($text)) { echo '<!-- Last Action --><div id="message" class="updated fade"><p>'.$text.'</p></div>'; } ?>
        <!-- Manage Post Ratings -->
        <div class="wrap">
            <h1><?php esc_html_e('Manage Ratings', 'wp-postratings'); ?></h1>
            <h2><?php esc_html_e('Rating Logs', 'wp-postratings'); ?></h2>
            <p><?php printf(
                /* translators: 1: Number of ratings on page 2: Max on page 3: Total rating */
                    esc_html__('Displaying %1$s to %2$s of %3$s rating log entries.', 'wp-postratings'),
                    '<strong>' . number_format_i18n( $display_on_page ) . '</strong>',
                    '<strong>' . number_format_i18n( $max_on_page ) . '</strong>',
                    '<strong>' . number_format_i18n( $total_ratings ) . '</strong>'
                );
                ?></p>
            <p><?php printf(
                /* translators: 1: Sort by text 2: Sort order text */
                    esc_html__('Sorted by %1$s in %2$s order.', 'wp-postratings'),
                    '<strong>' . $postratings_sortby_text . '</strong>',
                    '<strong>' . $postratings_sortorder_text . '</strong>'
                );
                ?></p>
            <table class="widefat">
                <thead>
                <tr>
                    <th width="2%"><?php esc_html_e('ID', 'wp-postratings'); ?></th>
                    <th width="10%"><?php esc_html_e('Username', 'wp-postratings'); ?></th>
                    <th width="10%"><?php esc_html_e('Rating', 'wp-postratings'); ?></th>
                    <th width="8%"><?php esc_html_e('Post ID', 'wp-postratings'); ?></th>
                    <th width="25%"><?php esc_html_e('Post Title', 'wp-postratings'); ?></th>
                    <th width="45%">Reason:</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if($postratings_logs) {
                    $i = 0;
                    foreach($postratings_logs as $postratings_log) {
                        if($i%2 == 0) {
                            $style = 'class="alternate"';
                        }  else {
                            $style = '';
                        }
                        $postratings_id = intval($postratings_log->rating_id);
                        $postratings_username = esc_html( stripslashes($postratings_log->rating_username) );
                        $postratings_rating = intval($postratings_log->rating_rating);
                        $postratings_postid = intval($postratings_log->rating_postid);
                        $postratings_posttitle = esc_html( stripslashes($postratings_log->rating_posttitle) );
                        $postratings_date = esc_html(mysql2date(sprintf(__('%s @ %s', 'wp-postratings'), get_option('date_format'), get_option('time_format')), gmdate('Y-m-d H:i:s', $postratings_log->rating_timestamp)));
                        $postratings_ip = esc_html( $postratings_log->rating_ip );
                        $postratings_host = esc_html( $postratings_log->rating_host );
                        $postratings_post_url = get_permalink( $postratings_postid ); //URL to Post
                        echo "<tr $style>\n";
                        echo '<td>'.$postratings_id.'</td>'."\n";
                        echo "<td>$postratings_username</td>\n";
                        echo '<td nowrap="nowrap">';
                        if($ratings_custom && $ratings_max == 2) {
                            if($postratings_rating > 0) {
                                $postratings_rating = '+'.$postratings_rating;
                            }
                            echo $postratings_rating;
                        } else {
                            if(is_rtl() && file_exists(WP_PLUGIN_DIR.'/wp-postratings/images/'.$ratings_image.'/rating_start-rtl.'.RATINGS_IMG_EXT)) {
                                echo '<img src="'.plugins_url('wp-postratings/images/'.$ratings_image.'/rating_start-rtl.'.RATINGS_IMG_EXT).'" alt="" class="post-ratings-image" />';
                            } elseif(file_exists(WP_PLUGIN_DIR.'/wp-postratings/images/'.$ratings_image.'/rating_start.'.RATINGS_IMG_EXT)) {
                                echo '<img src="'.plugins_url('wp-postratings/images/'.$ratings_image.'/rating_start.'.RATINGS_IMG_EXT).'" alt="" class="post-ratings-image" />';
                            }
                            if($ratings_custom) {
                                for($j=1; $j <= $ratings_max; $j++) {
                                    if($j <= $postratings_rating) {
                                        echo '<img src="'.plugins_url('wp-postratings/images/'.$ratings_image.'/rating_'.$j.'_on.'.RATINGS_IMG_EXT).'" alt="'.sprintf(_n('User Rate This %s Star', 'User Rate This %s Stars', $postratings_rating, 'wp-postratings'), $postratings_rating).__(' Out Of ', 'wp-postratings').$ratings_max.'" title="'.sprintf(_n('User Rate This %s Star', 'User Rate This %s Stars', $postratings_rating, 'wp-postratings'), $postratings_rating).__(' Out Of ', 'wp-postratings').$ratings_max.'" class="post-ratings-image" />';
                                    } else {
                                        echo '<img src="'.plugins_url('wp-postratings/images/'.$ratings_image.'/rating_'.$j.'_off.'.RATINGS_IMG_EXT).'" alt="'.sprintf(_n('User Rate This %s Star', 'User Rate This %s Stars', $postratings_rating, 'wp-postratings'), $postratings_rating).__(' Out Of ', 'wp-postratings').$ratings_max.'" title="'.sprintf(_n('User Rate This %s Star', 'User Rate This %s Stars', $postratings_rating, 'wp-postratings'), $postratings_rating).__(' Out Of ', 'wp-postratings').$ratings_max.'" class="post-ratings-image" />';
                                    }
                                }
                            } else {
                                for($j=1; $j <= $ratings_max; $j++) {
                                    if($j <= $postratings_rating) {
                                        echo '<img src="'.plugins_url('wp-postratings/images/'.$ratings_image.'/rating_on.'.RATINGS_IMG_EXT).'" alt="'.sprintf(_n('User Rate This %s Star', 'User Rate This %s Stars', $postratings_rating, 'wp-postratings'), $postratings_rating).__(' Out Of ', 'wp-postratings').$ratings_max.'" title="'.sprintf(_n('User Rate This %s Star', 'User Rate This %s Stars', $postratings_rating, 'wp-postratings'), $postratings_rating).__(' Out Of ', 'wp-postratings').$ratings_max.'" class="post-ratings-image" />';
                                    } else {
                                        echo '<img src="'.plugins_url('wp-postratings/images/'.$ratings_image.'/rating_off.'.RATINGS_IMG_EXT).'" alt="'.sprintf(_n('User Rate This %s Star', 'User Rate This %s Stars', $postratings_rating, 'wp-postratings'), $postratings_rating).__(' Out Of ', 'wp-postratings').$ratings_max.'" title="'.sprintf(_n('User Rate This %s Star', 'User Rate This %s Stars', $postratings_rating, 'wp-postratings'), $postratings_rating).__(' Out Of ', 'wp-postratings').$ratings_max.'" class="post-ratings-image" />';
                                    }
                                }
                            }
                            if(is_rtl() && file_exists(WP_PLUGIN_DIR.'/wp-postratings/images/'.$ratings_image.'/rating_end-rtl.'.RATINGS_IMG_EXT)) {
                                echo '<img src="'.plugins_url('wp-postratings/images/'.$ratings_image.'/rating_end-rtl.'.RATINGS_IMG_EXT).'" alt="" class="post-ratings-image" />';
                            } elseif(file_exists(WP_PLUGIN_DIR.'/wp-postratings/images/'.$ratings_image.'/rating_end.'.RATINGS_IMG_EXT)) {
                                echo '<img src="'.plugins_url('wp-postratings/images/'.$ratings_image.'/rating_end.'.RATINGS_IMG_EXT).'" alt="" class="post-ratings-image" />';
                            }
                        }
                        echo '</td>'."\n";
                        echo '<td>'.number_format_i18n($postratings_postid).'</td>'."\n";
                        echo '<td><a href="'.$postratings_post_url.'" target="_new">'.$postratings_posttitle.'</a></td>'."\n";

                        $reasons = $wpdb->get_row('SELECT * FROM ' . $wpdb->prefix . 'dislike_reasons WHERE id = ' . $postratings_id . ' AND post_id = ' . $postratings_postid, ARRAY_A );

                        echo '<td>';

                        if ( isset($reasons) && is_array($reasons) ) {
                            foreach ( $reasons as $key => $value ) {
                                if ( preg_match("/^reason_\d$/",$key) && $value ) {
                                    switch ( $key ) {
                                        case 'reason_0':
                                            echo 'Science is not accurate. ';
                                            break;
                                        case 'reason_1':
                                            echo 'Not interesting. ';
                                            break;
                                        case 'reason_2':
                                            echo 'Not what I was looking for. ';
                                            break;
                                        case 'reason_3':
                                            echo 'Not written well. ';
                                            break;
                                        case 'reason_4':
                                            echo 'Not thorough. ';
                                            break;
                                        case 'reason_5':
                                            echo 'Grammar/Spelling. ';
                                            break;
                                        case 'reason_6':
                                            echo 'Too difficult to understand. ';
                                            break;
                                        case 'reason_7':
                                            echo 'Titles/Headings are misleading. ';
                                            break;
                                        default:
                                            break;
                                    }
                                } elseif ( $key == 'other_reason' && !empty($value) ) {
                                    echo 'Other: ' . $value .' ';
                                }
                            }
                        }
                        echo '</tr>';
                        $i++;
                    }
                } else {
                    echo '<tr><td colspan="7" align="center"><strong>'.esc_html__('No Rating Logs Found', 'wp-postratings').'</strong></td></tr>';
                }
                ?>
                </tbody>
            </table>
            <!-- <Paging> -->
            <?php
            if($total_pages > 1) {
                ?>
                <br />
                <table class="widefat">
                    <tr>
                        <td align="<?php echo is_rtl() ? 'right' : 'left'; ?>" width="50%">
                            <?php
                            if($postratings_page > 1 && ((($postratings_page*$postratings_log_perpage)-($postratings_log_perpage-1)) <= $total_ratings)) {
                                echo '<strong>&laquo;</strong> <a href="'.$base_page.'&amp;ratingpage='.($postratings_page-1).$postratings_sort_url.'" title="&laquo; '.esc_html__('Previous Page', 'wp-postratings').'">'.esc_html__('Previous Page', 'wp-postratings').'</a>';
                            } else {
                                echo '&nbsp;';
                            }
                            ?>
                        </td>
                        <td align="<?php echo is_rtl() ? 'left' : 'right'; ?>" width="50%">
                            <?php
                            if($postratings_page >= 1 && ((($postratings_page*$postratings_log_perpage)+1) <=  $total_ratings)) {
                                echo '<a href="'.$base_page.'&amp;ratingpage='.($postratings_page+1).$postratings_sort_url.'" title="'.esc_html__('Next Page', 'wp-postratings').' &raquo;">'.esc_html__('Next Page', 'wp-postratings').'</a> <strong>&raquo;</strong>';
                            } else {
                                echo '&nbsp;';
                            }
                            ?>
                        </td>
                    </tr>
                    <tr class="alternate">
                        <td colspan="2" align="center">
                            <?php printf(esc_html__('Pages (%s): ', 'wp-postratings'), number_format_i18n($total_pages)); ?>
                            <?php
                            if ($postratings_page >= 4) {
                                echo '<strong><a href="'.$base_page.'&amp;ratingpage=1'.$postratings_sort_url.$postratings_sort_url.'" title="'.esc_html__('Go to First Page', 'wp-postratings').'">&laquo; '.esc_html__('First', 'wp-postratings').'</a></strong> ... ';
                            }
                            if($postratings_page > 1) {
                                echo ' <strong><a href="'.$base_page.'&amp;ratingpage='.($postratings_page-1).$postratings_sort_url.'" title="&laquo; '.esc_html__('Go to Page', 'wp-postratings').' '.number_format_i18n($postratings_page-1).'">&laquo;</a></strong> ';
                            }
                            for($i = $postratings_page - 2 ; $i  <= $postratings_page +2; $i++) {
                                if ($i >= 1 && $i <= $total_pages) {
                                    if($i == $postratings_page) {
                                        echo '<strong>['.number_format_i18n($i).']</strong> ';
                                    } else {
                                        echo '<a href="'.$base_page.'&amp;ratingpage='.($i).$postratings_sort_url.'" title="'.esc_html__('Page', 'wp-postratings').' '.number_format_i18n($i).'">'.number_format_i18n($i).'</a> ';
                                    }
                                }
                            }
                            if($postratings_page < $total_pages) {
                                echo ' <strong><a href="'.$base_page.'&amp;ratingpage='.($postratings_page+1).$postratings_sort_url.'" title="'.esc_html__('Go to Page', 'wp-postratings').' '.number_format_i18n($postratings_page+1).' &raquo;">&raquo;</a></strong> ';
                            }
                            if (($postratings_page+2) < $total_pages) {
                                echo ' ... <strong><a href="'.$base_page.'&amp;ratingpage='.($total_pages).$postratings_sort_url.'" title="'.esc_html__('Go to Last Page', 'wp-postratings').'">'.esc_html__('Last', 'wp-postratings').' &raquo;</a></strong>';
                            }
                            ?>
                        </td>
                    </tr>
                </table>
                <!-- </Paging> -->
                <?php
            }
            ?>
            <br />
            <form action="<?php echo esc_url( $base_page ); ?>" method="get">
                <input type="hidden" name="page" value="<?php echo $base_name; ?>" />
                <table class="widefat">
                    <tr>
                        <th><?php esc_html_e('Filter Options:', 'wp-postratings'); ?></th>
                        <td>
                            <?php esc_html_e('Post ID:', 'wp-postratings'); ?>&nbsp;<input type="text" name="id" value="<?php echo $postratings_filterid; ?>" size="7" maxlength="10" />
                            &nbsp;&nbsp;&nbsp;
                            <select name="user" size="1">
                                <option value=""></option>
                                <?php
                                $filter_users = $wpdb->get_results( $wpdb->prepare( "SELECT DISTINCT rating_username, rating_userid FROM {$wpdb->ratings} WHERE rating_username != %s ORDER BY rating_userid ASC, rating_username ASC", __(' Guest', 'wp-postratings' ) ) );
                                if($filter_users) {
                                    foreach($filter_users as $filter_user) {
                                        $rating_username = esc_html(stripslashes($filter_user->rating_username));
                                        $rating_userid = intval($filter_user->rating_userid);
                                        if($rating_userid > 0) {
                                            $prefix = esc_html__('Registered User: ', 'wp-postratings');
                                        } else {
                                            $prefix = esc_html__('Comment Author: ', 'wp-postratings');
                                        }
                                        if($rating_username == $postratings_filteruser) {
                                            echo '<option value="'.esc_attr($rating_username).'" selected="selected">'.$prefix.' '.$rating_username.'</option>'."\n";
                                        } else {
                                            echo '<option value="'.esc_attr($rating_username).'">'.$prefix.' '.$rating_username.'</option>'."\n";
                                        }
                                    }
                                }
                                ?>
                            </select>
                            &nbsp;&nbsp;&nbsp;
                            <select name="rating" size="1">
                                <option value=""></option>
                                <?php
                                $filter_ratings = $wpdb->get_results("SELECT DISTINCT rating_rating FROM $wpdb->ratings ORDER BY rating_rating ASC");
                                if($filter_ratings) {
                                    foreach($filter_ratings as $filter_rating) {
                                        $rating_rating = $filter_rating->rating_rating;
                                        $prefix = esc_html__('Rating: ', 'wp-postratings');
                                        if($rating_rating == $postratings_filterrating) {
                                            echo '<option value="'.$rating_rating.'" selected="selected">'.$prefix.' '.number_format_i18n($rating_rating).'</option>'."\n";
                                        } else {
                                            echo '<option value="'.$rating_rating.'">'.$prefix.' '.number_format_i18n($rating_rating).'</option>'."\n";
                                        }
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr class="alternate">
                        <th><?php esc_html_e('Sort Options:', 'wp-postratings'); ?></th>
                        <td>
                            <select name="by" size="1">
                                <option value="id"<?php if($postratings_sortby == 'rating_id') { echo ' selected="selected"'; }?>><?php esc_html_e('ID', 'wp-postratings'); ?></option>
                                <option value="username"<?php if($postratings_sortby == 'rating_username') { echo ' selected="selected"'; }?>><?php esc_html_e('Username', 'wp-postratings'); ?></option>
                                <option value="rating"<?php if($postratings_sortby == 'rating_rating') { echo ' selected="selected"'; }?>><?php esc_html_e('Rating', 'wp-postratings'); ?></option>
                                <option value="postid"<?php if($postratings_sortby == 'rating_postid') { echo ' selected="selected"'; }?>><?php esc_html_e('Post ID', 'wp-postratings'); ?></option>
                                <option value="posttitle"<?php if($postratings_sortby == 'rating_posttitle') { echo ' selected="selected"'; }?>><?php esc_html_e('Post Title', 'wp-postratings'); ?></option>
                                <option value="date"<?php if($postratings_sortby == 'rating_timestamp') { echo ' selected="selected"'; }?>><?php esc_html_e('Date', 'wp-postratings'); ?></option>
                                <option value="ip"<?php if($postratings_sortby == 'rating_ip') { echo ' selected="selected"'; }?>><?php esc_html_e('IP', 'wp-postratings'); ?></option>
                                <option value="host"<?php if($postratings_sortby == 'rating_host') { echo ' selected="selected"'; }?>><?php esc_html_e('Host', 'wp-postratings'); ?></option>
                            </select>
                            &nbsp;&nbsp;&nbsp;
                            <select name="order" size="1">
                                <option value="asc"<?php if($postratings_sortorder == 'asc') { echo ' selected="selected"'; }?>><?php esc_html_e('Ascending', 'wp-postratings'); ?></option>
                                <option value="desc"<?php if($postratings_sortorder == 'desc') { echo ' selected="selected"'; } ?>><?php esc_html_e('Descending', 'wp-postratings'); ?></option>
                            </select>
                            &nbsp;&nbsp;&nbsp;
                            <select name="perpage" size="1">
                                <?php
                                for($i=10; $i <= 100; $i+=10) {
                                    if($postratings_log_perpage == $i) {
                                        echo "<option value=\"$i\" selected=\"selected\">".esc_html__('Per Page', 'wp-postratings').": ".number_format_i18n($i)."</option>\n";
                                    } else {
                                        echo "<option value=\"$i\">".esc_html__('Per Page', 'wp-postratings').": ".number_format_i18n($i)."</option>\n";
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"><input type="submit" value="<?php esc_html_e('Go', 'wp-postratings'); ?>" class="button" /></td>
                    </tr>
                </table>
            </form>
        </div>
        <?php
    }
}