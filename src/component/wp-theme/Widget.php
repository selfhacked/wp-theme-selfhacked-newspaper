<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 11/6/18
 * Time: 5:05 PM
 */

namespace SelfHacked\SelfHackedNewspaper\Component\WpTheme;


use Enpii\Wp\EnpiiBase\Base\BaseComponent;
use SelfHacked\SelfHackedNewspaper\Widget\Custom_TOC_Widget;

class Widget extends BaseComponent {
	public $text_domain = null;

	public function initialize() {
		register_widget( Custom_TOC_Widget::class );

		// Remove plugin's widget
		unregister_widget( \toc_widget::class );
	}
}
