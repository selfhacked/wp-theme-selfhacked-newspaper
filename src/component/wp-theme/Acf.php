<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 10/23/18
 * Time: 4:16 PM
 */

namespace SelfHacked\SelfHackedNewspaper\Component\WpTheme;


use Enpii\Wp\EnpiiBase\Base\BaseComponent;

class Acf extends BaseComponent {
	/**
	 * @var string $text_domain
	 */
	public $text_domain = null;

	/**
	 * Initialize all ACF fields
	 */
	public function initialize() {

		$this->add_option_pages();
		$this->add_option_fields();

		$this->update_user_profile_fields();

		$acfPHP2Json = new AcfPHP2Json( [
			'text_domain' => $this->text_domain,
		] );
		$acfPHP2Json->initialize();
	}

	/**
	 * Add Options page to the site
	 */
	public function add_option_pages() {
		if ( function_exists( 'acf_add_options_page' ) ) {

			// Add options For SelfHacked Newspaper theme
			acf_add_options_page( [
				'page_title' => __( 'Newspaper SelfHacked - Appearance Options', $this->text_domain ),
				'menu_title' => __( 'Newspaper SelfHacked - Appearance Options', $this->text_domain ),
				'menu_slug'  => 'selfhacked-newspaper-appearance-options',
				'capability' => 'manage_options',
				'redirect'   => false
			] );
		}
	}

	/**
	 * Add ACF options field
	 */
	public function add_option_fields() {

		if ( function_exists( 'acf_add_local_field_group' ) ):

			acf_add_local_field_group( array(
				'key'                   => 'group_5bceec3840f7f',
				'title'                 => 'Options - Appearance Main',
				'fields'                => array(
					array(
						'key'               => 'field_5bceed63462df',
						'label'             => 'Footer',
						'name'              => '',
						'type'              => 'tab',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'placement'         => 'top',
						'endpoint'          => 0,
					),
					array(
						'key'               => 'field_5bceed84462e1',
						'label'             => 'Sub Footer Logo',
						'name'              => 'shnews_sub_footer_logo',
						'type'              => 'image',
						'instructions'      => 'Small logo stay at the bottom left corner, size should be 45x45',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'return_format'     => 'array',
						'preview_size'      => 'medium',
						'library'           => 'all',
						'min_width'         => '',
						'min_height'        => '',
						'min_size'          => '',
						'max_width'         => '',
						'max_height'        => '',
						'max_size'          => '',
						'mime_types'        => '',
					),
					array(
						'key'               => 'field_5bcf2973e32be',
						'label'             => 'Sub Footer Copyright Text',
						'name'              => 'shnews_sub_footer_copyright_text',
						'type'              => 'text',
						'instructions'      => 'Copyright text at bottom right, use {{year}} to display current year',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
						'prepend'           => '',
						'append'            => '',
						'maxlength'         => '',
					),
					array(
						'key'               => 'field_5c0a0607fgr45',
						'label'             => 'Single Post',
						'name'              => '',
						'type'              => 'tab',
						'instructions'      => 'Settings for single post globally',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'placement'         => 'top',
						'endpoint'          => 0,
					),
					array(
						'key'               => 'field_5c0dead0267b8',
						'label'             => 'Disclaimer',
						'name'              => 'disclaimer',
						'type'              => 'group',
						'instructions'      => 'This appears at the bottom of all posts',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'layout'            => 'block',
						'sub_fields'        => array(
							array(
								'key'               => 'field_5c0deafc267b9',
								'label'             => 'Title',
								'name'              => 'title',
								'type'              => 'text',
								'instructions'      => '',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'default_value'     => '',
								'placeholder'       => '',
								'prepend'           => '',
								'append'            => '',
								'maxlength'         => '',
							),
							array(
								'key'               => 'field_5c0deb2b267ba',
								'label'             => 'Description',
								'name'              => 'description',
								'type'              => 'text',
								'instructions'      => '',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'default_value'     => '',
								'placeholder'       => '',
								'prepend'           => '',
								'append'            => '',
								'maxlength'         => '',
							),
						),
					),
					array(
						'key'               => 'field_5c0f828a05d09',
						'label'             => 'Comment Reply Email',
						'name'              => 'comment_reply_email',
						'type'              => 'group',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'layout'            => 'block',
						'sub_fields'        => array(
							array(
								'key'               => 'field_5c0f829c05d0a',
								'label'             => 'Logo',
								'name'              => 'logo',
								'type'              => 'image',
								'instructions'      => '',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'return_format'     => 'array',
								'preview_size'      => 'thumbnail',
								'library'           => 'all',
								'min_width'         => '',
								'min_height'        => '',
								'min_size'          => '',
								'max_width'         => '',
								'max_height'        => '',
								'max_size'          => '',
								'mime_types'        => '',
							),
						),
					),
					array(
						'key'               => 'field_5c0a0607ce62d',
						'label'             => 'Shortcodes',
						'name'              => '',
						'type'              => 'tab',
						'instructions'      => 'Content for sections with shortcodes on the site like Start Here, SelfDecode ...',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'placement'         => 'top',
						'endpoint'          => 0,
					),
					array(
						'key'               => 'field_5c0a069bce62e',
						'label'             => 'Start Here',
						'name'              => 'shortcode_start_here',
						'type'              => 'group',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'layout'            => 'block',
						'sub_fields'        => array(
							array(
								'key'               => 'field_5c0a06e7ce62f',
								'label'             => 'Image',
								'name'              => 'image',
								'type'              => 'image',
								'instructions'      => 'The image to appear on the left',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'return_format'     => 'array',
								'preview_size'      => 'thumbnail',
								'library'           => 'all',
								'min_width'         => '',
								'min_height'        => '',
								'min_size'          => '',
								'max_width'         => '',
								'max_height'        => '',
								'max_size'          => '',
								'mime_types'        => '',
							),
							array(
								'key'               => 'field_5c0a0731ce631',
								'label'             => 'Content',
								'name'              => 'content',
								'type'              => 'wysiwyg',
								'instructions'      => '',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'default_value'     => '',
								'tabs'              => 'all',
								'toolbar'           => 'full',
								'media_upload'      => 1,
								'delay'             => 0,
							),
							array(
								'key'               => 'field_5c0a078cce632',
								'label'             => 'Link Url',
								'name'              => 'link_url',
								'type'              => 'text',
								'instructions'      => 'Url of the link to be redirected',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'default_value'     => '',
								'placeholder'       => '',
								'prepend'           => '',
								'append'            => '',
								'maxlength'         => '',
							),
							array(
								'key'               => 'field_5c0a07a3ce633',
								'label'             => 'Link Target',
								'name'              => 'link_target',
								'type'              => 'radio',
								'instructions'      => '',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'choices'           => array(
									'_self'  => 'Current Tab',
									'_blank' => 'New Tab',
								),
								'allow_null'        => 0,
								'other_choice'      => 1,
								'save_other_choice' => 0,
								'default_value'     => '_self',
								'layout'            => 'horizontal',
								'return_format'     => 'value',
							),
							array(
								'key'               => 'field_5c0a3404d27fb',
								'label'             => 'Link Text',
								'name'              => 'link_text',
								'type'              => 'text',
								'instructions'      => '',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'default_value'     => '',
								'placeholder'       => 'Text for the Link Button',
								'prepend'           => '',
								'append'            => '',
								'maxlength'         => '',
							),
						),
					),
					array(
						'key'               => 'field_5c0a891327197',
						'label'             => 'Lab Test Analyzer CTA',
						'name'              => 'lab_test_analyzer_cta',
						'type'              => 'clone',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'clone'             => array(
							0 => 'field_5c0a069bce62e',
						),
						'display'           => 'seamless',
						'layout'            => 'block',
						'prefix_label'      => 1,
						'prefix_name'       => 1,
					),
					array(
						'key'               => 'field_5c0a9ef3bb362',
						'label'             => 'Self Decode CTA',
						'name'              => 'self_decode_cta',
						'type'              => 'clone',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'clone'             => array(
							0 => 'field_5c0a069bce62e',
						),
						'display'           => 'seamless',
						'layout'            => 'block',
						'prefix_label'      => 1,
						'prefix_name'       => 1,
					),
					array(
						'key'               => 'field_5c0e16d7747ad',
						'label'             => 'Contact Us',
						'name'              => 'self_contact_us',
						'type'              => 'clone',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'clone'             => array(
							0 => 'field_5c0a069bce62e',
						),
						'display'           => 'seamless',
						'layout'            => 'block',
						'prefix_label'      => 1,
						'prefix_name'       => 1,
					),
				),
				'location'              => array(
					array(
						array(
							'param'    => 'options_page',
							'operator' => '==',
							'value'    => 'selfhacked-newspaper-appearance-options',
						),
					),
				),
				'menu_order'            => 0,
				'position'              => 'normal',
				'style'                 => 'default',
				'label_placement'       => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen'        => '',
				'active'                => 1,
				'description'           => '',
			) );

		endif;
	}

	/**
	 * Manage Fields in User Profile
	 */
	public function update_user_profile_fields() {
		if ( function_exists( 'acf_add_local_field_group' ) ):

			acf_add_local_field_group( array(
				'key'                   => 'group_5c170bb491585',
				'title'                 => 'User Profile - Extra fields',
				'fields'                => array(
					array(
						'key'               => 'field_5c1711d71037d',
						'label'             => 'Alt Avatar',
						'name'              => 'alt_avatar',
						'type'              => 'image',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'return_format'     => 'array',
						'preview_size'      => 'thumbnail',
						'library'           => 'all',
						'min_width'         => '',
						'min_height'        => '',
						'min_size'          => '',
						'max_width'         => '',
						'max_height'        => '',
						'max_size'          => '',
						'mime_types'        => '',
					),
					array(
						'key'               => 'field_5c1711a91037a',
						'label'             => 'Credentials',
						'name'              => 'credentials',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
						'prepend'           => '',
						'append'            => '',
						'maxlength'         => '',
					),
					array(
						'key'               => 'field_5c170c112e144',
						'label'             => 'Summary',
						'name'              => 'summary',
						'type'              => 'wysiwyg',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'tabs'              => 'all',
						'toolbar'           => 'full',
						'media_upload'      => 1,
						'delay'             => 0,
					),
					array(
						'key'               => 'field_5c1711ca1037b',
						'label'             => 'Favorite Biohacks',
						'name'              => 'favorite_biohacks',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
						'prepend'           => '',
						'append'            => '',
						'maxlength'         => '',
					),
					array(
						'key'               => 'field_5c1711d11037c',
						'label'             => 'Roles',
						'name'              => 'roles',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
						'prepend'           => '',
						'append'            => '',
						'maxlength'         => '',
					),
				),
				'location'              => array(
					array(
						array(
							'param'    => 'user_form',
							'operator' => '==',
							'value'    => 'edit',
						),
					),
				),
				'menu_order'            => 0,
				'position'              => 'normal',
				'style'                 => 'default',
				'label_placement'       => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen'        => '',
				'active'                => 1,
				'description'           => '',
			) );

		endif;
	}
}
