<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 10/22/18
 * Time: 4:00 PM
 */

namespace SelfHacked\SelfHackedNewspaper\Component;


use Enpii\Wp\EnpiiBase\Wp;
use SelfHacked\SelfHackedNewspaper\Component\WpTheme\Acf;
use SelfHacked\SelfHackedNewspaper\Component\WpTheme\Admin;
use SelfHacked\SelfHackedNewspaper\Component\WpTheme\DislikeReason;
use SelfHacked\SelfHackedNewspaper\Component\WpTheme\NewspaperModule;
use SelfHacked\SelfHackedNewspaper\Component\WpTheme\Shortcode;
use SelfHacked\SelfHackedNewspaper\Component\WpTheme\Widget;

class WpTheme extends \Enpii\Wp\EnpiiBase\Component\WpTheme {
	/**
	 * @property Acf $acf Advanced Custom Fields object to initialize all ACF fields
	 */
	public $acf = null;

	/**
	 * @property Admin $admin Object for Admin Panel to initialize all admin hooks
	 */
	public $admin = null;

	/**
	 * @property Widget $widget Object for theme widgets
	 */
	public $widget = null;

	/**
	 * @property Shortcode $widget Object for theme widgets
	 */
	public $shortcode = null;

	/**
	 * @property DislikeReason $dislike_reason Object for theme widgets
	 */
	public $dislike_reason = null;

	/**
	 * @property string $newspaper_module Name of class for to work with Newspaper Modules
	 */
	public $newspaper_module = null;

	/**
	 * @var string $subscribe_url Destination URL for subscribe form submit
	 */
	public $subscribe_url = null;

	/**
	 * Initialize theme with hooks
	 * Should be override by child class
	 */
	public function initialize() {
		$this->init_dependencies();
		$this->init_hooks();
	}

	/**
	 * Initialize all dependencies
	 */
	public function init_dependencies() {
		$action        = isset( $_REQUEST['action'] ) ? $_REQUEST['action'] : '';
		$td_theme_name = isset( $_REQUEST['td_theme_name'] ) ? $_REQUEST['td_theme_name'] : '';

		/** @var Acf $component_acf */
		$component_acf = $this->get_component( $this->acf );
		$component_acf->initialize();

		// Only do Admin things when that's the Admin screen
		if ( defined( 'WP_ADMIN' ) && WP_ADMIN ) {
			/** @var Admin $component_admin */
			$component_admin = $this->get_component( $this->admin );
			$component_admin->initialize();
		}

		/** @var Shortcode $component_widget */
		$component_shortcode = $this->get_component( $this->shortcode );
		$component_shortcode->initialize();

		/** @var DislikeReason $component_dislike_reason */
		$component_dislike_reason = $this->get_component( $this->dislike_reason );
		$component_dislike_reason->initialize();

		/** @var NewspaperModule $component_newspaper_module */
		$component_newspaper_module = $this->get_component( $this->newspaper_module );
		$component_newspaper_module->initialize();

		if ( defined( 'DOING_AJAX' ) && DOING_AJAX && $td_theme_name == 'Newspaper' && ( 'td_ajax_search' === $action || 'td_ajax_block' === $action ) ) {

		} else {
			/** @var Widget $component_widget */
			$component_widget = $this->get_component( $this->widget );
			$component_widget->initialize();
		}

		$this->init_image_size();
	}

	/**
	 * Init all hooks used in the theme
	 */
	public function init_hooks() {
		// For frontend
		add_action( 'safe_style_css', [ $this, 'add_safe_style_css' ] );
		add_filter( 'body_class', [ $this, 'add_site_id_to_body_class' ] );

		add_action( 'wp_enqueue_scripts', [ $this, 'process_styles_scripts' ], 1001 );

		add_action( 'wp_footer', [ $this, 'append_google_fonts_loader' ], 1001 );

		add_action( 'wp_ajax_nopriv_td_ajax_search', [ $this, 'ajax_search' ], 5 );
		add_action( 'wp_ajax_td_ajax_search', [ $this, 'ajax_search' ], 5 );
		add_action( 'wp_ajax_nopriv_td_ajax_search', [ $this, 'ajax_search' ], 5 );
		add_action( 'wp_ajax_td_ajax_search', [ $this, 'ajax_search' ], 5 );

		add_action( 'wp_ajax_nopriv_ac_subscribe', [ $this, 'ac_subscribe' ], 5 );
		add_action( 'wp_ajax_ac_subscribe', [ $this, 'ac_subscribe' ], 5 );

		add_action( 'pre_get_posts', [ $this, 'adjust_for_primary_category' ], 50 );

		// For both
		add_action( 'upload_mimes', [ $this, 'allow_svg_upload' ] );

		register_sidebar(array(
			'name'=>'Footer 4',
			'id' => 'td-footer-4',
			'before_widget' => '<aside class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<div class="block-title"><span>',
			'after_title' => '</span></div>'
		));

		add_action( 'wp_enqueue_scripts', [ $this, 'register_styles' ] );

		// For fixing wp-postratings image extension
		add_action( 'wp_postratings_image_extension', function () {
			return 'png';
		} );
	}

	/**
	 * Init all image sizes used in the theme
	 */
	public function init_image_size() {
		add_image_size( 'most-recent-top-thumb', 540, 170, true );
		add_image_size( 'most-recent-bottom-thumb', 340, 240, true );
	}

	/**
	 * Process styles and scripts using in theme
	 */
	public function process_styles_scripts() {
		/** For styles */
		// Dequeue Google fonts to use WebFont Loader
		wp_dequeue_style( 'google-fonts-style' );

		wp_enqueue_style( 'td-theme', $this->base_url . '/style.css', '', TD_THEME_VERSION, 'all' );
		wp_enqueue_style( 'td-theme-child', $this->child_base_url . '/assets/dist/css/main.css', array( 'td-theme' ), WP_DEBUG ? date( 'YmdHis' ) : TD_THEME_VERSION, 'screen' );

		/** For scripts */
		wp_enqueue_script( 'td-theme-child', $this->child_base_url . '/assets/dist/js/' . ( WP_DEBUG ? 'main.js' : 'main.min.js' ), [ 'td-site-min' ], WP_DEBUG ? date( 'YmdHis' ) : TD_THEME_VERSION, true );

		wp_localize_script( 'td-theme-child', 'common_var',
			[ 'ajax_url' => admin_url( 'admin-ajax.php' ) ] );
	}

	/**
	 * Print out script to load Google Fonts using Web Font loader
	 */
	public function append_google_fonts_loader() {
		echo '<script async>' . <<<JS

    WebFontConfig = {
        google: {
            families: ['Poppins:400,400i,500,600,600i,700'],
            subset: {
            }
        }
    };
    (function () {
        var wf = document.createElement('script');
        wf.src =
            '//ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
JS
		     . '</script>';

	}

	/**
	 * Do subscribe to Active Campaign (Ajax with json returned)
	 */
	public function ac_subscribe() {
		$subscribe_form_data = isset( $_REQUEST['subscribe_form_data'] ) ? $_REQUEST['subscribe_form_data'] : null;

		$response_data = [];

		$email         = isset( $subscribe_form_data['email'] ) ? $subscribe_form_data['email'] : '';
		$first_name    = isset( $subscribe_form_data['first_name'] ) ? $subscribe_form_data['first_name'] : '';
		$last_name     = isset( $subscribe_form_data['last_name'] ) ? $subscribe_form_data['last_name'] : '';
		$interest_data = isset( $subscribe_form_data['interest'] ) ? $subscribe_form_data['interest'] : '';
		$tags          = isset( $subscribe_form_data['tags'] ) ? $subscribe_form_data['tags'] : 'StickyNewsletterSignup';

		$interest = '';
		if ( isset( $interest_data[0] ) ) {
			$interest = (string) $interest_data[0];
		}

		$list_ids = defined( 'AC_SUBSCRIBE_LIST_IDS' ) ? explode( ',', AC_SUBSCRIBE_LIST_IDS ) : [ 31 ];

		if ( is_array( $subscribe_form_data ) ) {
			$ac = new \ActiveCampaign( 'https://selfhacked.api-us1.com', '779f7b1dd0107ade8c166982bb9fb9f49118ba3a7ddef68156e44b9358b12694986df8df' );

			// Adjust the default cURL timeout
			$ac->set_curl_timeout( 10 );

			$contact = [
				"email"      => $email,
				"first_name" => $first_name,
				"last_name"  => $last_name,
				"tags"       => $tags,
			];

			foreach ( (array) $list_ids as $list_key => $list_id ) {
				$contact["p[{$list_id}]"]      = $list_id;
				$contact["status[{$list_id}]"] = 1; // "Active" status
			}

			$contact['field'] = [
				// Need to use '||' to combine multiple values
				'%SELFHACKED_INTEREST%,0' => str_replace( ',', '||', $interest ),
			];

			$contact_sync = $ac->api( "contact/sync", $contact );

			if ( ! (int) $contact_sync->success ) {
				// request failed
				$response_data['status'] = 'fail';
				$response_data['error']  = $contact_sync->error;
			} else {
				$response_data['status']       = 'success';
				$response_data['contact_sync'] = $contact_sync;
			}
		}

		die( json_encode( $response_data ) );
	}

	/**
	 * Handle ajax search for Newspaper theme
	 */
	public function ajax_search() {
		$buffy     = '';
		$buffy_msg = '';

		//the search string
		if ( ! empty( $_POST['td_string'] ) ) {
			$td_string = esc_html( $_POST['td_string'] );
		} else {
			$td_string = '';
		}

		//get the data
		/* @var \WP_Query $td_query */
		$td_query = &\td_data_source::get_wp_query_search( $td_string ); //by ref  do the query

		//build the results
		if ( ! empty( $td_query->posts ) ) {
			foreach ( $td_query->posts as $post ) {
				/* @var \WP_Post $post */
				$post_content       = Wp::get_post_content( $post->post_content );
				$post_content       = str_replace( '<', ' <', $post_content );
				$post_content       = strip_tags( $post_content );
				$post->post_excerpt = Wp::get_keyword_highlighted_text( $post_content, $td_string );
				$post->post_title   = Wp::highlight_keyword( $post->post_title, $td_string );

				$td_module_instant_search = new \td_module_mx2( $post );

				$buffy .= Wp::get_template_part( 'parts/td-module/instant-search-item', [
					'td_module' => $td_module_instant_search,
					'post'      => $post,
				] );
			}
		}

		if ( count( $td_query->posts ) == 0 ) {
			//no results
			$buffy = '<div class="result-msg no-result">' . __td( 'No results', TD_THEME_NAME ) . '</div>';
		} else {
			//show the results
			/**
			 * @note:
			 * we use esc_url(home_url( '/' )) instead of the WordPress @see get_search_link function because that's what the internal
			 * WordPress widget it's using and it was creating duplicate links like: yoursite.com/search/search_query and yoursite.com?s=search_query
			 *
			 * also note that esc_url - as of today strips spaces (WTF) https://core.trac.wordpress.org/ticket/23605 so we used urlencode - to encode the query param with + instead of %20 as rawurlencode does
			 */

			$buffy_msg .= '<div class="result-msg"><a href="' . home_url( '/?s=' . urlencode( $td_string ) ) . '">' . __td( 'View all results', TD_THEME_NAME ) . '</a></div>';
			//add wrap
			$buffy = '<div class="td-aj-search-results">' . $buffy . '</div>' . $buffy_msg;
		}

		//prepare array for ajax
		$buffyArray = array(
			'td_data'          => $buffy,
			'td_total_results' => 2,
			'td_total_in_list' => count( $td_query->posts ),
			'td_search_query'  => $td_string,
			//'td_search_query'=> strip_tags ($td_string)
		);

		// Return the String
		die( json_encode( $buffyArray ) );
	}

	/**
	 * Handle the SQL clauses to choose the posts with primary category on top
	 *
	 * @param \WP_Query $query
	 */
	public function adjust_for_primary_category( $query ) {
		// It's always better to look for negative then returning
		// Makes our code cleaner and with less indentation
		if ( ! $query->is_main_query() || ! $query->is_category ) {
			return; // Actions on WordPress require no Return
		}
		$this->repair_sql_clauses_for_primary_category();
	}

	/**
	 * Adjust sql clause to have primary category sorted first
	 */
	public function repair_sql_clauses_for_primary_category() {
		add_filter( 'posts_fields', [ $this, 'add_primary_category_to_select' ] );
		add_filter( 'posts_join', [ $this, 'add_primary_category_to_join' ] );
		add_filter( 'posts_orderby', [ $this, 'add_primary_category_to_orderby' ] );

		// Need to remove all primary category filter after being applied
		add_filter( 'posts_request', function ( $input ) {
			remove_filter( 'posts_fields', [ $this, 'add_primary_category_to_select' ] );
			remove_filter( 'posts_join', [ $this, 'add_primary_category_to_join' ] );
			remove_filter( 'posts_orderby', [ $this, 'add_primary_category_to_orderby' ] );

			return $input;
		} );
	}

	/**
	 * Add primary_category_id meta_value to select
	 *
	 * @param $select
	 *
	 * @return string
	 */
	public function add_primary_category_to_select( $select ) {
		return $select . ', wppostmeta4cate.meta_value primary_category_id';
	}

	/**
	 * Join primary category from meta to sql
	 *
	 * @param $join
	 *
	 * @return string
	 */
	public function add_primary_category_to_join( $join ) {
		global $wpdb, $wp_query;

		$category_id = $wp_query->get_queried_object_id();

		return $join . " LEFT JOIN $wpdb->postmeta wppostmeta4cate ON (wppostmeta4cate.meta_key = '_yoast_wpseo_primary_category' AND wppostmeta4cate.post_id = wp_posts.ID  AND wppostmeta4cate.meta_value = " . esc_sql( $category_id ) . ")";
	}

	/**
	 * Add order by primary category found
	 *
	 * @param $orderby
	 *
	 * @return string
	 */
	public function add_primary_category_to_orderby( $orderby ) {
		return "primary_category_id DESC, " . $orderby;
	}

	/**
	 * Register theme styles
	 */
	public function register_styles() {
		wp_register_style(
			'fontawesome',
			'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css',
			[],
			'4.7.0'
		);
		wp_enqueue_style( 'fontawesome' );
	}
}
