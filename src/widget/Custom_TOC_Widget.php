<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 11/6/18
 * Time: 5:07 PM
 */

namespace SelfHacked\SelfHackedNewspaper\Widget;


class Custom_TOC_Widget extends \WP_Widget {
	public $text_domain = null;

	private $tic = null;
	private $options = [];

	function __construct( $config = null ) {
		$widget_options  = array(
			'classname'   => 'Custom_TOC_Widget',
			'description' => 'Display the table of contents in the sidebar with this widget'
		);
		$control_options = array(
			'width'   => 250,
			'height'  => 350,
			'id_base' => 'custom-toc-widget'
		);
		parent::__construct( 'custom-toc-widget', 'Custom TOC+', $widget_options, $control_options );

		$this->tic     = new \toc();
		$this->options = $this->tic->get_options();
	}

	public function __call( $method, $args ) {
		if ( method_exists( $this->tic, $method ) ) {
			return call_user_func_array( [ $this->tic, $method ], $args );
		}
	}

	/**
	 * This function extracts headings from the html formatted $content.  It will pull out
	 * only the required headings as specified in the options.  For all qualifying headings,
	 * this function populates the $find and $replace arrays (both passed by reference)
	 * with what to search and replace with.
	 *
	 * Returns a html formatted string of list items for each qualifying heading.  This
	 * is everything between and NOT including <ul> and </ul>
	 */
	public function extract_headings( &$find, &$replace, $content = '' ) {
		$matches = array();
		$anchor  = '';
		$items   = false;

		// reset the internal collision collection as the_content may have been triggered elsewhere
		// eg by themes or other plugins that need to read in content such as metadata fields in
		// the head html tag, or to provide descriptions to twitter/facebook
		$this->collision_collector = array();

		if ( is_array( $find ) && is_array( $replace ) && $content ) {
			// get all headings
			// the html spec allows for a maximum of 6 heading depths
			if ( preg_match_all( '/(<h([1-6]{1})[^>]*>).*<\/h\2>/msuU', $content, $matches, PREG_SET_ORDER ) ) {

				// remove undesired headings (if any) as defined by heading_levels
				if ( count( $this->options['heading_levels'] ) != 6 ) {
					$new_matches = array();
					for ( $i = 0; $i < count( $matches ); $i ++ ) {
						if ( in_array( $matches[ $i ][2], $this->options['heading_levels'] ) ) {
							$new_matches[] = $matches[ $i ];
						}
					}
					$matches = $new_matches;
				}

				// remove specific headings if provided via the 'exclude' property
				if ( $this->options['exclude'] ) {
					$excluded_headings = explode( '|', $this->options['exclude'] );
					if ( count( $excluded_headings ) > 0 ) {
						for ( $j = 0; $j < count( $excluded_headings ); $j ++ ) {
							// escape some regular expression characters
							// others: http://www.php.net/manual/en/regexp.reference.meta.php
							$excluded_headings[ $j ] = str_replace(
								array( '*' ),
								array( '.*' ),
								trim( $excluded_headings[ $j ] )
							);
						}

						$new_matches = array();
						for ( $i = 0; $i < count( $matches ); $i ++ ) {
							$found = false;
							for ( $j = 0; $j < count( $excluded_headings ); $j ++ ) {
								if ( @preg_match( '/^' . $excluded_headings[ $j ] . '$/imU', strip_tags( $matches[ $i ][0] ) ) ) {
									$found = true;
									break;
								}
							}
							if ( ! $found ) {
								$new_matches[] = $matches[ $i ];
							}
						}
						if ( count( $matches ) != count( $new_matches ) ) {
							$matches = $new_matches;
						}
					}
				}

				// remove empty headings
				$new_matches = array();
				for ( $i = 0; $i < count( $matches ); $i ++ ) {
					if ( trim( strip_tags( $matches[ $i ][0] ) ) != false ) {
						$new_matches[] = $matches[ $i ];
					}
				}
				if ( count( $matches ) != count( $new_matches ) ) {
					$matches = $new_matches;
				}

				// check minimum number of headings
				if ( count( $matches ) >= $this->options['start'] ) {

					for ( $i = 0; $i < count( $matches ); $i ++ ) {
						// get anchor and add to find and replace arrays
						$anchor    = $this->url_anchor_target( $matches[ $i ][0] );
						$find[]    = $matches[ $i ][0];
						$replace[] = str_replace(
							array(
								$matches[ $i ][1],                // start of heading
								'</h' . $matches[ $i ][2] . '>'    // end of heading
							),
							array(
								$matches[ $i ][1] . '<span id="' . $anchor . '">',
								'</span></h' . $matches[ $i ][2] . '>'
							),
							$matches[ $i ][0]
						);

						// assemble flat list
						if ( ! $this->options['show_heirarchy'] ) {
							$items .= '<li><a href="#' . $anchor . '">';
							if ( $this->options['ordered_list'] ) {
								$items .= count( $replace ) . ' ';
							}
							$items .= strip_tags( $matches[ $i ][0] ) . '</a></li>';
						}
					}

					// build a hierarchical toc?
					// we could have tested for $items but that var can be quite large in some cases
					if ( $this->options['show_heirarchy'] ) {
						$items = $this->build_hierarchy( $matches );
					}

				}
			}
		}

		return $items;
	}

	/**
	 * Widget output to the public
	 */
	function widget( $args, $instance ) {
		global $tic, $wp_query;
		$items = $custom_toc_position = '';
		$find  = $replace = array();

		$toc_options         = $tic->get_options();
		$post                = get_post( $wp_query->post->ID );
		$custom_toc_position = strpos( $post->post_content, '[toc]' );    // at this point, shortcodes haven't run yet so we can't search for <!--TOC-->

		if ( $tic->is_eligible( $custom_toc_position ) ) {

			extract( $args );

			$items = $this->extract_headings( $find, $replace, wptexturize( $post->post_content ) );
			$title = ( array_key_exists( 'title', $instance ) ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
			if ( strpos( $title, '%PAGE_TITLE%' ) !== false ) {
				$title = str_replace( '%PAGE_TITLE%', get_the_title(), $title );
			}
			if ( strpos( $title, '%PAGE_NAME%' ) !== false ) {
				$title = str_replace( '%PAGE_NAME%', get_the_title(), $title );
			}
			$hide_inline = $toc_options['show_toc_in_widget_only'];

			$css_classes = '';
			// bullets?
			if ( $toc_options['bullet_spacing'] ) {
				$css_classes .= ' have_bullets';
			} else {
				$css_classes .= ' no_bullets';
			}

			if ( $items ) {
				// before widget (defined by themes)
				echo $before_widget;

				// display the widget title if one was input (before and after titles defined by themes)
				if ( $title ) {
					echo $before_title . $title . $after_title;
				}

				// display the list
				echo '<ul class="toc_widget_list' . $css_classes . '">' . $items . '</ul>';

				echo '<script>'
				     . '    jQuery(document).ready(function($){'
				     . '        var clickObj = {'
				     . '            clickPlus: function() {'
				     . '                $(this).parent().children("ul").css("display","block");'
				     . '                $(this).removeClass("fa-plus").addClass("fa-minus");'
				     . '            },'
				     . '            clickMinus: function() {'
				     . '                if($(this).parent().has("ul").length){'
				     . '                    $(this).parent().children("ul").css("display","none");'
				     . '                    $(this).removeClass("fa-minus").addClass("fa-plus");'
				     . '                }'
				     . '            }'
				     . '        };'
				     . '        $(".toc_widget_list > li").each(function(){'
				     . '            if(!$(this).has("ul").length) {'
				     . '                $(this).children("i.fa-plus").removeClass("fa-plus").addClass("fa-minus");;'
				     . '            }'
				     . '        });'
				     . '        $(".toc_widget_list li").on("click", "i.fa-plus", clickObj.clickPlus);'
				     . '        $(".toc_widget_list li").on("click", "i.fa-minus", clickObj.clickMinus);'
				     . '    });'
				     . '</script>';

				// after widget (defined by themes)
				echo $after_widget;
			}
		}
	}


	/**
	 * Update the widget settings
	 */
	function update( $new_instance, $old_instance ) {
		global $tic;

		$instance = $old_instance;

		// strip tags for title to remove HTML (important for text inputs)
		$instance['title'] = strip_tags( trim( $new_instance['title'] ) );

		// no need to strip tags for the following
		//$instance['hide_inline'] = $new_instance['hide_inline'];
		$tic->set_show_toc_in_widget_only( $new_instance['hide_inline'] );
		$tic->set_show_toc_in_widget_only_post_types( (array) $new_instance['show_toc_in_widget_only_post_types'] );

		return $instance;
	}


	/**
	 * Displays the widget settings on the widget panel.
	 */
	function form( $instance ) {
		global $tic;
		$toc_options = $tic->get_options();

		$defaults = array(
			'title' => $toc_options['heading_text']
		);
		$instance = wp_parse_args( (array) $instance, $defaults );

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'table-of-contents-plus' ); ?>
				:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>"
			       name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>"
			       style="width:100%;"/>
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $toc_options['show_toc_in_widget_only'], 1 ); ?>
			       id="<?php echo $this->get_field_id( 'hide_inline' ); ?>"
			       name="<?php echo $this->get_field_name( 'hide_inline' ); ?>" value="1"/>
			<label
				for="<?php echo $this->get_field_id( 'hide_inline' ); ?>"> <?php _e( 'Show the table of contents only in the sidebar', 'table-of-contents-plus' ); ?></label>
		</p>

		<div class="show_toc_in_widget_only_post_types"
		     style="margin: 0 0 25px 25px; display: <?php echo ( $toc_options['show_toc_in_widget_only'] == 1 ) ? 'block' : 'none'; ?>;">
			<p><?php _e( 'For the following content types:', 'table-of-contents-plus' ); ?></p>

			<?php
			foreach ( get_post_types() as $post_type ) {
				// make sure the post type isn't on the exclusion list
				if ( ! in_array( $post_type, $tic->get_exclude_post_types() ) ) {
					echo '<input type="checkbox" value="' . $post_type . '" id="' . $this->get_field_id( 'show_toc_in_widget_only_post_types_' . $post_type ) . '" name="' . $this->get_field_name( "show_toc_in_widget_only_post_types" ) . '[]"';
					if ( in_array( $post_type, $toc_options['show_toc_in_widget_only_post_types'] ) ) {
						echo ' checked="checked"';
					}
					echo ' /><label for="' . $this->get_field_id( 'show_toc_in_widget_only_post_types_' . $post_type ) . '"> ' . $post_type . '</label><br />';
				}
			}

			?></div>

		<script type="text/javascript">
			jQuery(document).ready(function ($) {
				$('#<?php echo $this->get_field_id( 'hide_inline' ); ?>').click(function () {
					$(this).parent().siblings('div.show_toc_in_widget_only_post_types').toggle('fast');
				});
			});
		</script>
		<?php
	}


	/**
	 * Returns a clean url to be used as the destination anchor target
	 */
	private function url_anchor_target( $title ) {
		$return = false;

		if ( $title ) {
			$return = trim( strip_tags( $title ) );

			// convert accented characters to ASCII
			$return = remove_accents( $return );

			// replace newlines with spaces (eg when headings are split over multiple lines)
			$return = str_replace( array( "\r", "\n", "\n\r", "\r\n" ), ' ', $return );

			// remove &amp;
			$return = str_replace( '&amp;', '', $return );

			// remove non alphanumeric chars
			$return = preg_replace( '/[^a-zA-Z0-9 \-_]*/', '', $return );

			// convert spaces to _
			$return = str_replace(
				array( '  ', ' ' ),
				'_',
				$return
			);

			// remove trailing - and _
			$return = rtrim( $return, '-_' );

			// lowercase everything?
			if ( $this->options['lowercase'] ) {
				$return = strtolower( $return );
			}

			// if blank, then prepend with the fragment prefix
			// blank anchors normally appear on sites that don't use the latin charset
			if ( ! $return ) {
				$return = ( $this->options['fragment_prefix'] ) ? $this->options['fragment_prefix'] : '_';
			}

			// hyphenate?
			if ( $this->options['hyphenate'] ) {
				$return = str_replace( '_', '-', $return );
				$return = str_replace( '--', '-', $return );
			}
		}

		if ( array_key_exists( $return, $this->collision_collector ) ) {
			$this->collision_collector[ $return ] ++;
			$return .= '-' . $this->collision_collector[ $return ];
		} else {
			$this->collision_collector[ $return ] = 1;
		}

		return apply_filters( 'toc_url_anchor_target', $return );
	}

	private function build_hierarchy( &$matches ) {
		$current_depth      = 100;    // headings can't be larger than h6 but 100 as a default to be sure
		$html               = '';
		$numbered_items     = array();
		$numbered_items_min = null;

		// reset the internal collision collection
		$this->collision_collector = array();

		// find the minimum heading to establish our baseline
		for ( $i = 0; $i < count( $matches ); $i ++ ) {
			if ( $current_depth > $matches[ $i ][2] ) {
				$current_depth = (int) $matches[ $i ][2];
			}
		}

		$numbered_items[ $current_depth ] = 0;
		$numbered_items_min               = $current_depth;

		for ( $i = 0; $i < count( $matches ); $i ++ ) {

			if ( $current_depth == (int) $matches[ $i ][2] ) {
				$html .= '<li>';
			}

			// start lists
			if ( $current_depth != (int) $matches[ $i ][2] ) {
				for ( $current_depth; $current_depth < (int) $matches[ $i ][2]; $current_depth ++ ) {
					$numbered_items[ $current_depth + 1 ] = 0;
					$html                                 .= '<ul><li>';
				}
			}

			// list item
			if ( in_array( $matches[ $i ][2], $this->options['heading_levels'] ) ) {
				if ( $current_depth == 1 ) {
					$html .= '<i class="fa fa-plus"></i>';
				}
				$html .= '<p class="toc-item"><a href="#' . $this->url_anchor_target( $matches[ $i ][0] ) . '">';
				if ( $this->options['ordered_list'] ) {
					// attach leading numbers when lower in hierarchy
					$html .= '<span class="toc_number toc_depth_' . ( $current_depth - $numbered_items_min + 1 ) . '">';
					for ( $j = $numbered_items_min; $j < $current_depth; $j ++ ) {
						$number = ( $numbered_items[ $j ] ) ? $numbered_items[ $j ] : 0;
						$html   .= $number . '.';
					}

					$html .= ( $numbered_items[ $current_depth ] + 1 ) . '</span> ';
					$numbered_items[ $current_depth ] ++;
				}
				$html .= strip_tags( $matches[ $i ][0] ) . '</a></p>';
			}


			// end lists
			if ( $i != count( $matches ) - 1 ) {
				if ( $current_depth > (int) $matches[ $i + 1 ][2] ) {
					for ( $current_depth; $current_depth > (int) $matches[ $i + 1 ][2]; $current_depth -- ) {
						$html                             .= '</li></ul>';
						$numbered_items[ $current_depth ] = 0;
					}
				}

				if ( $current_depth == (int) @$matches[ $i + 1 ][2] ) {
					$html .= '</li>';
				}
			} else {
				// this is the last item, make sure we close off all tags
				for ( $current_depth; $current_depth >= $numbered_items_min; $current_depth -- ) {
					$html .= '</li>';
					if ( $current_depth != $numbered_items_min ) {
						$html .= '</ul>';
					}
				}
			}
		}

		return $html;
	}

}
