<?php

namespace Selfhacked\SelfhackedNewspaper;

/**
 * Class Util
 * @package staticHacked\staticHackedNewspaper
 */
class Util {

    /**
     * @var array $properties Array of global shortcode properties to encode
     */
    private static $properties = [
        'tdc_css' => true
    ];

    /**
     * @var array $shortcode_properties Array of shortcode specific properties to encode
     */
    private static $shortcode_properties = [
        'tdm_block_inline_text' => [
            'description' => true
        ],
        'vc_raw_html'           => [
            'content' => true
        ]
    ];

    /**
     * @param string|array $content
     *
     * @return string
     */
    public static function encode_content( $content ) {
        // in order to match the encoding/decoding used in visual composer and td composer
        // the function needs to be able to encode both strings and arrays
        if ( is_array( $content ) ) {
            return strip_tags( base64_encode( json_encode( $content ) ) );
        }

        return strip_tags( base64_encode( rawurlencode( $content ) ) );
    }

    /**
     * @param string $content
     * @param bool   $is_json
     *
     * @return string|array
     */
    public static function decode_content( $content, $is_json = false ) {
        if ( $is_json ) {
            return json_decode( base64_decode( strip_tags( $content ) ) );
        }

        return rawurldecode( base64_decode( strip_tags( $content ) ) );
    }

    /**
     * Shortcode builder
     *
     * @param string      $shortcode
     * @param array       $args
     * @param string|null $content
     *
     * @return string
     */
    public static function generate_shortcode( $shortcode, $args = [], $content = null ) {
        // check if the contents of this shortcode should be encoded
        $encode_content = isset( static::$shortcode_properties[ $shortcode ]['content'] ) && static::$shortcode_properties[ $shortcode ]['content'] ? true : false;

        if ( ! empty( $args ) ) {
            // set the shortcode format to include arguments
            $format = is_null( $content ) ? '[%1$s %2$s]' : '[%1$s %2$s]%3$s[/%1$s]';

            // build the shortcode arguments array
            foreach ( $args as $k => $v ) {
                // check if this property should be encoded
                $global_check = isset( static::$properties[ $k ] ) && static::$properties[ $k ] ? true : false;
                $local_check  = isset( static::$shortcode_properties[ $shortcode ][ $k ] ) && static::$shortcode_properties[ $shortcode ][ $k ] ? true : false;
                $encode       = $global_check || $local_check ? true : false;

                if ( $encode ) {
                    $v = static::encode_content( $v );
                }

                $args[ $k ] = $k . '="' . $v . '"';
            }
        } else {
            // set the shortcode format to exclude arguments
            $format = is_null( $content ) ? '[%1$s]' : '[%1$s]%3$s[/%1$s]';
        }

        $args = implode( ' ', $args );

        if ( $encode_content && is_string( $content ) ) {
            $content = static::encode_content( $content );
        }

        return sprintf( $format, $shortcode, $args, $content );
    }
    
    public static function build_data_attr( $key, $data ) {
	    return sprintf('data-%s="%s"',$key,htmlentities(json_encode($data),ENT_QUOTES,'UTF-8'));
    }

}