<?php
/*  ----------------------------------------------------------------------------
    the author template
 */

use Enpii\Wp\EnpiiBase\Wp;

get_header();

//set the template id, used to get the template specific settings
$template_id = 'author';

//prepare the loop variables
global $loop_module_id, $loop_sidebar_position, $part_cur_auth_obj;
$loop_module_id = td_util::get_option('tds_' . $template_id . '_page_layout', 1); //module 1 is default
$loop_sidebar_position = td_util::get_option('tds_' . $template_id . '_sidebar_pos'); //sidebar right is default (empty)

// sidebar position used to align the breadcrumb on sidebar left + sidebar first on mobile issue
$td_sidebar_position = '';
if($loop_sidebar_position == 'sidebar_left') {
	$td_sidebar_position = 'td-sidebar-left';
}
//read the current author object - used by here in title and by /parts/author-header.php
$part_cur_auth_obj = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author'));


//set the global current author object, used by widgets (author widget)
td_global::$current_author_obj = $part_cur_auth_obj;

$text_domain = \Enpii\Wp\EnpiiBase\Base\WpApp::instance()->get_wp_theme()->text_domain;

$alt_avatar = get_user_meta( $part_cur_auth_obj->ID, 'alt_avatar', true );
$credentials = get_user_meta( $part_cur_auth_obj->ID, 'credentials', true );
$summary = get_user_meta( $part_cur_auth_obj->ID, 'summary', true );
$favorite_biohacks = get_user_meta( $part_cur_auth_obj->ID, 'favorite_biohacks', true );
$roles = get_user_meta( $part_cur_auth_obj->ID, 'roles', true );
$facebook = get_user_meta( $part_cur_auth_obj->ID, 'facebook', true );
$linkedin = get_user_meta( $part_cur_auth_obj->ID, 'linkedin', true );
$biography = get_user_meta( $part_cur_auth_obj->ID, 'description', true );

$block_limit = 6;
$block_page = get_query_var('paged' );
$block_offset = $block_page == 0 ? 0 : ($block_limit * $block_page) - $block_limit;

?>

<div class="td-main-content-wrap td-container-wrap">
    <div class="td-container <?php echo $td_sidebar_position; ?>">
        <div class="td-crumb-container">
            <?php echo td_page_generator::get_author_breadcrumbs($part_cur_auth_obj); // generate the breadcrumbs ?>
        </div>
        <div class="td-pb-row">
            <?php
            switch ($loop_sidebar_position) {

                /*  ----------------------------------------------------------------------------
                    This is the default option
                    If you set the author template with the right sidebar the theme will use this part
                */
                default:
                    ?>
                        <div class="td-pb-span8 td-main-content">
                            <div class="td-ss-main-content">
                                <div class="td-page-header">
                                    <h1 class="entry-title td-page-title">
                                        <span><?php echo $part_cur_auth_obj->display_name; ?></span>
                                    </h1>
                                </div>

                                <?php
                                //load the author box located in - parts/page-author-box.php - can be overwritten by the child theme
                                locate_template('parts/page-author-box.php', true);
                                ?>

                                <?php locate_template('loop.php', true);?>

                                <?php td_page_generator::get_pagination(); // the pagination?>
                            </div>
                        </div>
                        <div class="td-pb-span4 td-main-sidebar">
                            <div class="td-ss-main-sidebar">
                                <?php get_sidebar(); ?>
                            </div>
                        </div>
                    <?php
                    break;



                /*  ----------------------------------------------------------------------------
                    If you set the author template with sidebar left the theme will render this part
                */
                case 'sidebar_left':
                    ?>
                        <div class="td-pb-span8 td-main-content <?php echo $td_sidebar_position; ?>-content">
                            <div class="td-ss-main-content">
                                <div class="td-page-header">
                                    <h1 class="entry-title td-page-title">
                                        <span><?php echo $part_cur_auth_obj->display_name; ?></span>
                                    </h1>
                                </div>

                                <?php
                                //load the author box located in - parts/page-author-box.php - can be overwritten by the child theme
                                locate_template('parts/page-author-box.php', true);
                                ?>

                                <?php locate_template('loop.php', true);?>

                                <?php td_page_generator::get_pagination(); // the pagination?>
                            </div>
                        </div>
		                <div class="td-pb-span4 td-main-sidebar">
			                <div class="td-ss-main-sidebar">
				                <?php get_sidebar(); ?>
			                </div>
		                </div>
                    <?php
                    break;



                /*  ----------------------------------------------------------------------------
                    If you set the author template with no sidebar the theme will use this part
                */
                case 'no_sidebar':
                    ?>
	                <div class="td-pb-span4 td-main-sidebar">
		                <?php if ( $alt_avatar ): ?>
			                <?php
			                echo wp_get_attachment_image( $alt_avatar, 'medium' );
			                ?>
		                <?php else: ?>
			                <?php echo get_avatar( $part_cur_auth_obj->user_email ); ?>
		                <?php endif; ?>
	                </div>
                    <div class="td-pb-span8 td-main-content">
                        <div class="td-ss-main-content">
                            <div class="td-page-header">
                                <h1 class="entry-title td-page-title name">
                                    <span>
                                        <?php echo $part_cur_auth_obj->display_name; ?>
                                        <?php if ( $facebook ): ?>
                                            <a href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                                        <?php endif; ?>
                                        <?php if ( $linkedin ): ?>
                                            <a href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                                        <?php endif; ?>
                                    </span>
                                </h1>

                                <div class="credentials">
                                    <?php echo $credentials; ?>
                                </div>
                                <div class="summary">
                                    <?php echo Wp::esc_editor_field($summary); ?>
                                </div>
                                <div class="biography">
                                    <?php echo $biography; ?>
                                </div>
                                <div class="details">
                                    <div class="detail-content">
                                        <span class="detail-name">Favorite biohacks:</span>
                                        <?php echo $favorite_biohacks; ?>
                                    </div>

                                    <div class="detail-content">
                                        <span class="detail-name">Roles:</span>
                                        <?php echo $roles; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="td-pb-span12">
                        <?php echo Wp::get_template_part( 'parts/shortcodes/author-page-block', [
                            'text_domain' => $text_domain,
                            'block_limit' => $block_limit,
                            'block_offset' => $block_offset,
                            'author_id'    => $part_cur_auth_obj->ID,
                            'display_name' => $part_cur_auth_obj->display_name
                        ] ); ?>
                        <?php td_page_generator::get_pagination(); // the pagination?>
                        <?php echo Wp::get_template_part( 'parts/shortcodes/checkout-selfdecode', [ 'text_domain' => $text_domain ] ); ?>
                    </div>
                    <?php
                    break;
            }
            ?>
        </div> <!-- /.td-pb-row -->
    </div> <!-- /.td-container -->
</div> <!-- /.td-main-content-wrap -->
<?php
get_footer();
?>
