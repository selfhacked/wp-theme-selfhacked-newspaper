<?php

use Enpii\Wp\EnpiiBase\Wp;

class td_category_template_9 extends td_category_template {

	private $text_domain = '';

	public function __construct() {
		parent::__construct();
		$this->text_domain = td_api_base::get_key( __CLASS__, 'text_domain' );
	}

    function render() {
        $category    = $this->get_category_obj();
        $breadcrumbs = parent::get_breadcrumbs();
        $description = parent::get_description();
        $pull_down   = parent::get_pull_down();
        $args        = [
	        'text_domain' => $this->text_domain,
	        'breadcrumbs' => $breadcrumbs,
	        'description' => $description,
	        'category_id' => $category->term_id,
	        'parent_id'   => $category->parent,
	        'pull_down'   => $pull_down
        ];

        if ( $category->parent == 0 ) {
        	$args['title']      = parent::get_title();
            $args['categories'] = $this->get_sub_categories( $category->term_id );
            echo Wp::get_template_part( 'parts/category_templates/td_category_template_9', $args );
        } else {
        	$parent             = $this->get_category_obj( $category->parent );
	        $args['title']      = $parent->name;
	        $args['categories'] = $this->get_sub_categories( $parent->term_id );
	        echo Wp::get_template_part( 'parts/category_templates/td_category_template_9', $args );
        }
    }

    /**
     * @return WP_Term
     */
    function get_category_obj( $cat_id = null ) {
        if ( isset( $cat_id ) ) {
            return get_term( $cat_id );
        }
        return get_term( td_global::$current_category_obj->cat_ID );
    }

    function get_sub_categories( $cat_id ) {
        return get_categories( [ 'parent' => $cat_id ] );
    }
}
