<?php

class td_module_selfhacked_2 extends td_module {

	private $text_domain = '';

	public function __construct($post, $module_atts = []) {
		parent::__construct($post, $module_atts);
		$this->text_domain = td_api_base::get_key( __CLASS__, 'text_domain' );
	}

    function render() {
        ob_start();
        ?>
        <div class="<?php echo $this->get_module_classes(); ?>">
            <div class="td-module-image">
                <?php echo $this->get_image( 'td_240x360', true ); ?>
            </div>

            <?php echo $this->get_quotes_on_blocks(); ?>

        </div>
        <?php return ob_get_clean();
    }


    function get_rating() {
        // <i class="fa fa-star" aria-hidden="true"></i>
        $post_ratings_data = get_post_custom( $this->post->ID );
        $rating            = round( is_array( $post_ratings_data ) && array_key_exists( 'ratings_average', $post_ratings_data ) ? (float) $post_ratings_data['ratings_average'][0] : 0, 1 );

        return '<div class="rating"><i class="fa fa-star" aria-hidden="true"></i>' . $rating . '</div>';
    }

    function get_comments() {
        // <i class="far fa-comment-dots"></i>
        $buffy = '';
        if ( td_util::get_option( 'tds_m_show_comments' ) != 'hide' ) {
            $buffy .= '<a class="comments" href="' . get_comments_link( $this->post->ID ) . '"><i class="fa fa-commenting" aria-hidden="true"></i>';
            $buffy .= get_comments_number( $this->post->ID );
            $buffy .= '</a>';
        }

        return $buffy;
    }

    function get_image( $thumbType, $css_image = false ) {
        $buffy                               = ''; //the output buffer
        $tds_hide_featured_image_placeholder = td_util::get_option( 'tds_hide_featured_image_placeholder' );
        //retina image
        $srcset_sizes = '';

        // do we have a post thumb or a placeholder?
        if ( ! is_null( $this->post_thumb_id ) or ( $tds_hide_featured_image_placeholder != 'hide_placeholder' ) ) {

            if ( ! is_null( $this->post_thumb_id ) ) {
                //if we have a thumb
                // check to see if the thumb size is enabled in the panel, we don't have to check for the default wordpress
                // thumbs (the default ones are already cut and we don't have  a panel setting for them)
                if ( td_util::get_option( 'tds_thumb_' . $thumbType ) != 'yes' and $thumbType != 'thumbnail' ) {
                    //the thumb is disabled, show a placeholder thumb from the theme with the "thumb disabled" message
                    global $_wp_additional_image_sizes;

                    if ( empty( $_wp_additional_image_sizes[ $thumbType ]['width'] ) ) {
                        $td_temp_image_url[1] = '';
                    } else {
                        $td_temp_image_url[1] = $_wp_additional_image_sizes[ $thumbType ]['width'];
                    }

                    if ( empty( $_wp_additional_image_sizes[ $thumbType ]['height'] ) ) {
                        $td_temp_image_url[2] = '';
                    } else {
                        $td_temp_image_url[2] = $_wp_additional_image_sizes[ $thumbType ]['height'];
                    }

                    // For custom wordpress sizes (not 'thumbnail', 'medium', 'medium_large' or 'large'), get the image path using the api (no_image_path)
                    $thumb_disabled_path = td_global::$get_template_directory_uri;
                    if ( strpos( $thumbType, 'td_' ) === 0 ) {
                        $thumb_disabled_path = td_api_thumb::get_key( $thumbType, 'no_image_path' );
                    }
                    $td_temp_image_url[0] = $thumb_disabled_path . '/images/thumb-disabled/' . $thumbType . '.png';

                    $attachment_alt   = 'alt=""';
                    $attachment_title = '';

                } else {
                    // the thumb is enabled from the panel, it's time to show the real thumb
                    $td_temp_image_url = wp_get_attachment_image_src( $this->post_thumb_id, $thumbType );
                    $attachment_alt    = get_post_meta( $this->post_thumb_id, '_wp_attachment_image_alt', true );
                    $attachment_alt    = 'alt="' . esc_attr( strip_tags( $attachment_alt ) ) . '"';
                    $attachment_title  = ' title="' . esc_attr( strip_tags( $this->title ) ) . '"';

                    if ( empty( $td_temp_image_url[0] ) ) {
                        $td_temp_image_url[0] = '';
                    }

                    if ( empty( $td_temp_image_url[1] ) ) {
                        $td_temp_image_url[1] = '';
                    }

                    if ( empty( $td_temp_image_url[2] ) ) {
                        $td_temp_image_url[2] = '';
                    }

                    //retina image
                    //don't display srcset_sizes on DEMO - it messes up Pagespeed score (8 March 2017)
                    if ( TD_DEPLOY_MODE != 'demo' ) {
                        $srcset_sizes = td_util::get_srcset_sizes( $this->post_thumb_id, $thumbType, $td_temp_image_url[1], $td_temp_image_url[0] );
                    }

                } // end panel thumb enabled check


            } else {
                //we have no thumb but the placeholder one is activated
                global $_wp_additional_image_sizes;

                if ( empty( $_wp_additional_image_sizes[ $thumbType ]['width'] ) ) {
                    $td_temp_image_url[1] = '';
                } else {
                    $td_temp_image_url[1] = $_wp_additional_image_sizes[ $thumbType ]['width'];
                }

                if ( empty( $_wp_additional_image_sizes[ $thumbType ]['height'] ) ) {
                    $td_temp_image_url[2] = '';
                } else {
                    $td_temp_image_url[2] = $_wp_additional_image_sizes[ $thumbType ]['height'];
                }

                /**
                 * get thumb height and width via api
                 * first we check the global in case a custom thumb is used
                 *
                 * The api thumb is checked only for additional sizes registered and if at least one of the settings (width or height) is empty.
                 * This should be enough to avoid getting a non existing id using api thumb.
                 */
                if ( ! empty( $_wp_additional_image_sizes ) && array_key_exists( $thumbType, $_wp_additional_image_sizes ) && ( $td_temp_image_url[1] == '' || $td_temp_image_url[2] == '' ) ) {
                    $td_thumb_parameters  = td_api_thumb::get_by_id( $thumbType );
                    $td_temp_image_url[1] = $td_thumb_parameters['width'];
                    $td_temp_image_url[2] = $td_thumb_parameters['height'];
                }

                // For custom wordpress sizes (not 'thumbnail', 'medium', 'medium_large' or 'large'), get the image path using the api (no_image_path)
                $no_thumb_path = td_global::$get_template_directory_uri;
                if ( strpos( $thumbType, 'td_' ) === 0 ) {
                    $no_thumb_path = rtrim( td_api_thumb::get_key( $thumbType, 'no_image_path' ), '/' );
                }
                $td_temp_image_url[0] = $no_thumb_path . '/images/no-thumb/' . $thumbType . '.png';

                $attachment_alt   = 'alt=""';
                $attachment_title = '';
            } //end    if ($this->post_has_thumb) {


            $buffy .= '<div class="td-module-thumb">';
            if ( current_user_can( 'edit_published_posts' ) ) {
                $buffy .= '<a class="td-admin-edit" href="' . get_edit_post_link( $this->post->ID ) . '">edit</a>';
            }


            $buffy .= '<a href="' . $this->href . '" rel="bookmark" class="td-image-wrap" title="' . $this->title_attribute . '">';

            // css image
            if ( $css_image === true ) {
                // retina image
                if ( td_util::get_option( 'tds_thumb_' . $thumbType . '_retina' ) == 'yes' && ! empty( $td_temp_image_url[1] ) ) {
                    $retina_url = wp_get_attachment_image_src( $this->post_thumb_id, $thumbType . '_retina' );
                    if ( ! empty( $retina_url[0] ) ) {
                        $td_temp_image_url[0] = $retina_url[0];
                    }
                }
                $buffy .= '<div class="entry-thumb td-thumb-css" style="background-image: url(' . $td_temp_image_url[0] . ')">'
                          . '<div class="td-module-meta-info">'
                          . $this->get_category()
                          . $this->get_rating()
                          . $this->get_comments()
                          . $this->get_title()
                          . '</div>'
                          . '</div>';

                // normal image
            } else {
                $buffy .= '<img width="' . $td_temp_image_url[1] . '" height="' . $td_temp_image_url[2] . '" class="entry-thumb" src="' . $td_temp_image_url[0] . '"' . $srcset_sizes . ' ' . $attachment_alt . $attachment_title . '/>';
            }

            // on videos add the play icon
            if ( get_post_format( $this->post->ID ) == 'video' ) {

                $use_small_post_format_icon_size = false;
                // search in all the thumbs for the one that we are currently using here and see if it has post_format_icon_size = small
                foreach ( td_api_thumb::get_all() as $thumb_from_thumb_list ) {
                    if ( $thumb_from_thumb_list['name'] == $thumbType and $thumb_from_thumb_list['post_format_icon_size'] == 'small' ) {
                        $use_small_post_format_icon_size = true;
                        break;
                    }
                }

                // load the small or medium play icon
                if ( $use_small_post_format_icon_size === true ) {
                    $buffy .= '<span class="td-video-play-ico td-video-small"><img width="20" height="20" class="td-retina" src="' . td_global::$get_template_directory_uri . '/images/icons/video-small.png' . '" alt="video"/></span>';
                } else {
                    $buffy .= '<span class="td-video-play-ico"><img width="40" height="40" class="td-retina" src="' . td_global::$get_template_directory_uri . '/images/icons/ico-video-large.png' . '" alt="video"/></span>';
                }
            } // end on video if

            $buffy .= '</a>';
            $buffy .= '</div>'; //end wrapper

            return $buffy;
        }
    }

    function get_category() {

        $buffy                      = '';
        $selected_category_obj      = '';
        $selected_category_obj_id   = '';
        $selected_category_obj_name = '';

        $current_post_type  = get_post_type( $this->post->ID );
        $builtin_post_types = get_post_types( [ '_builtin' => true ] );

        if ( array_key_exists( $current_post_type, $builtin_post_types ) ) {

            // default post type

            //read the post meta to get the custom primary category
            $td_post_theme_settings = td_util::get_post_meta_array( $this->post->ID, 'td_post_theme_settings' );
            if ( ! empty( $td_post_theme_settings['td_primary_cat'] ) ) {
                //we have a custom category selected
                $selected_category_obj = get_category( $td_post_theme_settings['td_primary_cat'] );
            } else {

                //get one auto
                $categories = get_the_category( $this->post->ID );

                if ( is_category() ) {
                    foreach ( $categories as $category ) {
                        if ( $category->term_id == get_query_var( 'cat' ) ) {
                            $selected_category_obj = $category;
                            break;
                        }
                    }
                }

                if ( empty( $selected_category_obj ) and ! empty( $categories[0] ) ) {
                    if ( $categories[0]->name === TD_FEATURED_CAT and ! empty( $categories[1] ) ) {
                        $selected_category_obj = $categories[1];
                    } else {
                        $selected_category_obj = $categories[0];
                    }
                }
            }

            if ( ! empty( $selected_category_obj ) ) {
                $selected_category_obj_id   = $selected_category_obj->cat_ID;
                $selected_category_obj_name = $selected_category_obj->name;
            }

        } else {

            // custom post type

            // Validate that the current queried term is a term
            global $wp_query;
            $current_queried_term = $wp_query->get_queried_object();

            if ( $current_queried_term instanceof WP_Term ) {
                $current_term = term_exists( $current_queried_term->name, $current_queried_term->taxonomy );

                if ( $current_term !== 0 && $current_term !== null ) {
                    $selected_category_obj = $current_queried_term;
                }
            }


            // Get and validate the custom taxonomy according to the validated queried term
            if ( ! empty( $selected_category_obj ) ) {

                $taxonomy_objects       = get_object_taxonomies( $this->post, 'objects' );
                $custom_taxonomy_object = '';

                foreach ( $taxonomy_objects as $taxonomy_object ) {

                    if ( $taxonomy_object->_builtin !== 1 && $taxonomy_object->name === $selected_category_obj->taxonomy ) {
                        $custom_taxonomy_object = $taxonomy_object;
                        break;
                    }
                }

                // Invalid taxonomy
                if ( empty( $custom_taxonomy_object ) ) {
                    return $buffy;
                }

                $selected_category_obj_id   = $selected_category_obj->term_id;
                $selected_category_obj_name = $selected_category_obj->name;
            }
        }

        if ( ! empty( $selected_category_obj_id ) && ! empty( $selected_category_obj_name ) ) { //@todo catch error here
            $bg_color = td_util::get_category_option( $selected_category_obj_id, 'tdc_color' );

            if ( $bg_color ) {
                $bg_color = ' style="background-color: ' . $bg_color . '"';
            }

            $buffy .= '<a href="' . get_category_link( $selected_category_obj_id ) . '" class="td-post-category"' . $bg_color . '>' . $selected_category_obj_name . '</a>';
        }

        //return print_r($post, true);
        return $buffy;
    }
}
