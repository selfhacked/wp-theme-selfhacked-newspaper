<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 12/5/18
 * Time: 10:40 PM
 */

class td_module_mega_menu extends td_module {

	private $text_domain = '';

	public function __construct($post, $module_atts = []) {
		parent::__construct($post, $module_atts);
		$this->text_domain = td_api_base::get_key( __CLASS__, 'text_domain' );
	}

	function render() {

		return \Enpii\Wp\EnpiiBase\Wp::get_template_part( 'parts/td-module/mega-menu-post-item', [
			'css_class'  => $this->get_module_classes( array( "td_mod_mega_menu" ) ),
			'post'       => $this->post,
			'post_title' => $this->get_title(),
		] );
	}
}
