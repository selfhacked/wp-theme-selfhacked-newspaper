<?php

class td_module_top_substances extends td_module {

	private $text_domain = '';

	public function __construct($post, $module_atts = []) {
		parent::__construct($post, $module_atts);
		$this->text_domain = td_api_base::get_key( __CLASS__, 'text_domain' );
	}

    function render() {

	    if ( $substance_name = get_field( 'substance_name', $this->post->ID ) ) {
		    $this->title = $substance_name;
	    }

        ob_start();
        ?>
        <div class="<?php echo $this->get_module_classes(); ?>">
            <?php echo $this->get_title(); ?>
            <?php echo $this->get_quotes_on_blocks(); ?>
        </div>
        <?php return ob_get_clean();
    }
}
