<?php

use Enpii\Wp\EnpiiBase\Wp;

class td_module_genetics extends td_module {

	private $text_domain = '';

	public function __construct($post, $module_atts = []) {
		parent::__construct($post, $module_atts);
		$this->text_domain = td_api_base::get_key( __CLASS__, 'text_domain' );
	}

	function render() {
		return Wp::get_template_part( 'parts/modules/td_module_genetics', [
			'text_domain'    => $this->text_domain,
			'module_classes' => $this->get_module_classes(),
			'module_title'   => $this->get_title(),
		]);
	}
}