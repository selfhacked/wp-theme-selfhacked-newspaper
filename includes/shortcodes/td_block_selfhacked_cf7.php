<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 12/11/18
 * Time: 10:11 PM
 */

class td_block_selfhacked_cf7 extends td_block {
	function render( $atts, $content = null ) {
		$atts = shortcode_atts(
			[
				'id' => '',
			], $atts, 'td_block_selfhacked_cf7' );

		parent::render( $atts );

		$buffy = '<div id="'.$this->block_uid.'" class="'.$this->get_block_classes().'">'.do_shortcode('[contact-form-7 id="' . esc_attr( $atts['id'] ) . '" title="Contact Form 7"]').'</div>';

		return $buffy;
	}
}
