<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 12/7/18
 * Time: 10:02 PM
 */

class td_block_selfhacked_check_out_lta extends td_block {
	public $text_domain = 'selfhacked';

	protected $shortcode_atts = array(); //the atts used for rendering the current block

	public function __construct() {
		$this->text_domain = td_api_base::get_key( __CLASS__, 'text_domain' );
	}

	public function render( $atts, $content = null ) {

		$atts = shortcode_atts(
			[
				'extra_css_class' => '',
			], $atts, 'td_block_selfhacked_check_out_lta' );

		parent::render( $atts );

		return Enpii\Wp\EnpiiBase\Wp::get_template_part( 'parts/shortcodes/selfhacked-cta', [
			'shortcode_data' => get_field( 'lab_test_analyzer_cta_shortcode_start_here', 'options' ),
			'id'             => $this->block_uid,
			'css_class'      => $this->get_block_classes() . ' ' . 'shortcode__selfhacked-cta--check-out-lta' . ' ' . $atts['extra_css_class'],
			'text_domain'    => $this->text_domain,
		] );
	}
}
