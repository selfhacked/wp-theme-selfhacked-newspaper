<?php

use Enpii\Wp\EnpiiBase\Wp;

class td_block_selfhacked_book_ad extends td_block {

    public function get_custom_css() {
        $unique_block_class = $this->block_uid . '_rand';

        $compiled_css = '';

        $raw_css =
            "<style>
                /* @title_color */
                .$unique_block_class .content-wrapper .title {
                    color: @title_color;
                }
                /* @description_color */
                .$unique_block_class .content-wrapper .description {
                    color: @description_color;
                }
                /* @price_color */
                .$unique_block_class .content-wrapper .price {
                    color: @price_color;
                }
			</style>";


        $td_css_compiler = new td_css_compiler( $raw_css );

        $title_color = $this->get_att( 'title_color' );
        if ( $title_color != '' ) {
            $td_css_compiler->load_setting_raw( 'title_color', $title_color );
        }

        $description_color = $this->get_att( 'description_color' );
        if ( $description_color != '' ) {
            $td_css_compiler->load_setting_raw( 'description_color', $description_color );
        }

        $price_color = $this->get_att( 'price_color' );
        if ( $price_color != '' ) {
            $td_css_compiler->load_setting_raw( 'price_color', $price_color );
        }

        $compiled_css .= $td_css_compiler->compile_css();

        return $compiled_css;
    }

    function render( $atts, $content = null ) {
        parent::render( $atts );
        $image_info = tdc_util::get_image( $atts );

        return Wp::get_template_part( 'parts/shortcodes/td_block_selfhacked_book_ad', [
        	'block_css'     => $this->get_block_css(),
	        'block_classes' => $this->get_block_classes(),
	        'button_link'   => $atts['button_link'],
	        'image_url'     => $image_info['url'],
	        'title'         => $atts['title'],
	        'price'         => $atts['price'],
	        'description'   => $atts['description']
        ]);
    }
}