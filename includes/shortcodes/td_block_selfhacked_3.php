<?php

use Enpii\Wp\EnpiiBase\Wp;

class td_block_selfhacked_3 extends td_block {

	private $text_domain = '';

	public function __construct() {
		$this->text_domain = td_api_base::get_key( __CLASS__, 'text_domain' );
	}

    function render( $atts, $content = null ) {
        parent::render( $atts );
	    return Wp::get_template_part( 'parts/shortcodes/td_block', [
		    'block_js'         => $this->get_block_js(),
		    'block_css'        => $this->get_block_css(),
		    'block_classes'    => $this->get_block_classes(),
		    'block_title'      => $this->get_block_title(),
		    'block_uid'        => $this->block_uid,
		    'block_inner'      => $this->inner( $this->td_query->posts ),
		    'block_pagination' => $this->get_block_pagination(),
		    'block_filter'     => $this->get_pull_down_filter()
	    ]);
    }

    function inner( $posts, $td_column_number = '' ) {
	    return Wp::get_template_part( 'parts/shortcodes/inner/td_block_selfhacked_3', [ 'posts' => $posts ] );
    }
}