<?php

use Enpii\Wp\EnpiiBase\Wp;

class td_block_selfhacked_substances extends td_block {

	private $text_domain = '';

	private $data_store = [
		0   => [],
		1   => [],
		2   => [],
		3   => [],
		4   => [],
		5   => [],
		6   => [],
		7   => [],
		8   => [],
		9   => [],
		'A' => [],
		'B' => [],
		'C' => [],
		'D' => [],
		'E' => [],
		'F' => [],
		'G' => [],
		'H' => [],
		'I' => [],
		'J' => [],
		'K' => [],
		'L' => [],
		'M' => [],
		'N' => [],
		'O' => [],
		'P' => [],
		'Q' => [],
		'R' => [],
		'S' => [],
		'T' => [],
		'U' => [],
		'V' => [],
		'W' => [],
		'X' => [],
		'Y' => [],
		'Z' => []
	];

	public function __construct() {
		$this->text_domain     = td_api_base::get_key( __CLASS__, 'text_domain' );
	}

	function render( $atts, $content = null ) {
		$categories             = array_merge( [ $atts['cat_1'], $atts['cat_2'] ] );
		$atts['cat_1_children'] = preg_split( '/,\s?/', $atts['cat_1_children'] );
		$atts['cat_2_children'] = preg_split( '/,\s?/', $atts['cat_2_children'] );
		$categories             = array_merge( $atts['cat_1_children'], $atts['cat_2_children'], $categories );
		$atts['category_ids']   = implode( ', ', $categories );

		$atts['limit'] = 9999;

		parent::render( $atts );

		$this->build_data_store( $this->td_query->posts );

		return Wp::get_template_part( 'parts/shortcodes/td_block_selfhacked_substances', [
			'text_domain' => $this->text_domain,
			'block_js'    => $this->get_block_js(),
			'block_css'   => $this->get_block_css(),
			'block_class' => $this->get_block_classes(),
			'block_uid'   => $this->block_uid,
			'data_store'  => $this->data_store,
			'block_inner' => $this->inner( $this->td_query->posts ),
			'atts'        => $this->get_all_atts(),
		] );
	}

	function inner( $posts, $td_column_number = '' ) {
		$buffy = '';

		$buffy .= Wp::get_template_part( 'parts/shortcodes/inner/section-substances-data-row', [
			'data_store'  => $this->data_store
		] );

		return $buffy;
	}

	function get_custom_title() {
		return $this->get_att( 'custom_title' );
	}

	/**
	 * @param array $posts
	 */
	private function build_data_store( $posts ) {
		/** @var WP_Post $post */
		foreach ( (array) $posts as $post ) {
			if ( $substance_name = get_field( 'custom_mega_menu_title', $post->ID ) ) {
				array_push( $this->data_store[ strtoupper( $substance_name[0] ) ], $post );
			} else {
				array_push( $this->data_store[ strtoupper( $post->post_title[0] ) ], $post );
			}
		}
	}
}