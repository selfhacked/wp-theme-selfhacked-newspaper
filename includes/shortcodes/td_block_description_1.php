<?php

use Enpii\Wp\EnpiiBase\Wp;

class td_block_description_1 extends td_block {

	public $text_domain = '';

	public function __construct() {
		$this->text_domain = td_api_base::get_key( __CLASS__, 'text_domain' );
	}

	function render( $atts, $content = null ) {
		parent::render( $atts );

		return Wp::get_template_part( 'parts/shortcodes/td_block_description_1', [
			'block_css'     => $this->get_block_css(),
			'block_classes' => $this->get_block_classes(),
			'block_title'   => $this->get_block_title(),
			'image_url_1'   => wp_get_attachment_url( $atts['image_1'] ),
			'image_url_2'   => wp_get_attachment_url( $atts['image_2'] ),
			'image_url_3'   => wp_get_attachment_url( $atts['image_3'] ),
			'image_url_4'   => wp_get_attachment_url( $atts['image_4'] ),
			'description_1' => $atts['description_1'],
			'description_2' => $atts['description_2'],
			'description_3' => $atts['description_3'],
			'description_4' => $atts['description_4']
		]);
	}
}