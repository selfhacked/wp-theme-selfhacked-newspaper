<?php
/**
 * Created by PhpStorm.
 * User: tracnguyen
 * Date: 12/7/18
 * Time: 5:51 PM
 */

class td_block_selfhacked_start_here extends td_block {
	public $text_domain = 'selfhacked';

	protected $shortcode_atts = array(); //the atts used for rendering the current block

	public function __construct() {
		$this->text_domain = td_api_base::get_key( __CLASS__, 'text_domain' );
	}

	public function render( $atts, $content = null ) {

		$atts = shortcode_atts(
			[
				'extra_css_class' => '',
			], $atts, 'td_block_selfhacked_start_here' );

		parent::render( $atts );

		return Enpii\Wp\EnpiiBase\Wp::get_template_part( 'parts/shortcodes/selfhacked-cta', [
			'shortcode_data' => get_field( 'shortcode_start_here', 'options' ),
			'id'             => $this->block_uid,
			'css_class'      => $this->get_block_classes() . ' ' . 'shortcode__selfhacked-cta--start-here' . ' ' . $atts['extra_css_class'],
			'text_domain'    => $this->text_domain,
		] );
	}
}
