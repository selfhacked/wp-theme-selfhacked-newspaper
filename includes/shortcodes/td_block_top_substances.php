<?php

class td_block_top_substances extends td_block {

	private $text_domain = '';

	public function __construct() {
		$this->text_domain = td_api_base::get_key( __CLASS__, 'text_domain' );
	}

	function render( $atts, $content = null ) {
		$categories = array_merge( [ $atts['cat_1'], $atts['cat_2'] ] );

		if ( !$atts['category_id'] ) {
			$atts['category_ids']   = implode( ', ', $categories );
		}

		$atts['limit'] = 10;

		parent::render( $atts );$buffy = '';

		$buffy .= $this->get_block_js();
		$buffy .= $this->get_block_css();
		$buffy .= '<div class="' . $this->get_block_classes() . '">';

		$buffy .= '<div id="' . $this->block_uid . '" class="td_block_inner">';
		$buffy .= $this->inner( $this->td_query->posts );
		$buffy .= '</div>';
		$buffy .= '<div class="block-footer">';
		$buffy .= '<a href="' . $this->get_att('custom_url') . '">';
		$buffy .= __( 'SEE ALL', $this->text_domain ) . '<i class="fa fa-chevron-right" aria-hidden="true"></i>';
		$buffy .= '</a>';
		$buffy .= '</div>';
		$buffy .= '</div> <!-- ./block -->';

		return $buffy;
	}

	function inner( $posts, $td_column_number = '' ) {
		$buffy           = '';
		$td_block_layout = new td_block_layout();

		if ( ! empty( $posts ) ) {
			$buffy .= $td_block_layout->open_row();
			foreach ( $posts as $post ) {
				$selfhacked_module = new td_module_top_substances( $post );
				$buffy             .= $td_block_layout->open6();
				$buffy             .= $selfhacked_module->render();
				$buffy             .= $td_block_layout->close6();
			}
			$buffy .= $td_block_layout->close_row();
		}
		$buffy .= $td_block_layout->close_all_tags();

		return $buffy;
	}
}