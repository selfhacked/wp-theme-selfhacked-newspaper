<?php

use Enpii\Wp\EnpiiBase\Wp;

class td_block_store_item extends td_block {

	private $text_domain = '';

	public function __construct() {
		$this->text_domain = td_api_base::get_key( __CLASS__, 'text_domain' );
	}

	public function get_custom_css() {
		$unique_block_class = $this->block_uid . '_rand';

		$compiled_css = '';

		$raw_css =
			"<style>
                /* @title_color */
                .$unique_block_class .center-wrapper .title {
                    color: @title_color;
                }
                /* @description_color */
                .$unique_block_class .center-wrapper .description {
                    color: @description_color;
                }
                /* @price_color */
                .$unique_block_class .right-wrapper .price {
                    color: @price_color;
                }
                /* @button_font_color */
                .$unique_block_class .right-wrapper .button {
                    color: @button_font_color;
                }
                /* @button_bg_color */
                .$unique_block_class .right-wrapper .button {
                    background-color: @button_bg_color;
                }
                /* @content_bg_color */
                .$unique_block_class .content-wrapper {
				    background-color: @content_bg_color;
				}
			</style>";


		$td_css_compiler = new td_css_compiler( $raw_css );

		$title_color = $this->get_att( 'title_color' );
		if ( $title_color != '' ) {
			$td_css_compiler->load_setting_raw( 'title_color', $title_color );
		}

		$description_color = $this->get_att( 'description_color' );
		if ( $description_color != '' ) {
			$td_css_compiler->load_setting_raw( 'description_color', $description_color );
		}

		$price_color = $this->get_att( 'price_color' );
		if ( $price_color != '' ) {
			$td_css_compiler->load_setting_raw( 'price_color', $price_color );
		}

		$button_font_color = $this->get_att( 'button_font_color' );
		if ( $button_font_color != '' ) {
			$td_css_compiler->load_setting_raw( 'button_font_color', $button_font_color );
		}

		$button_bg_color = $this->get_att( 'button_bg_color' );
		if ( $button_bg_color != '' ) {
			$td_css_compiler->load_setting_raw( 'button_bg_color', $button_bg_color );
		}

		$content_bg_color = $this->get_att( 'content_bg_color' );
		if ( $content_bg_color != '' ) {
			$td_css_compiler->load_setting_raw( 'content_bg_color', $content_bg_color );
		}

		$compiled_css .= $td_css_compiler->compile_css();

		return $compiled_css;
	}

	function render( $atts, $content = null ) {
		$atts = shortcode_atts(
			[
				'image'              => '',
				'title'              => '',
				'description'        => '',
				'price'              => '',
				'button_text'        => 'Buy Now',
				'button_link'        => '',
				'button_link_target' => '_self',
				'title_color'        => '',
				'description_color'  => '',
				'price_color'        => '',
				'button_font_color'  => '',
				'button_bg_color'    => '',
				'content_bg_color'   => ''
			], $atts, 'td_block_store_item' );


		parent::render( $atts );

		$image_info = tdc_util::get_image( $atts );

		return Wp::get_template_part( 'parts/shortcodes/td_block_store_item', [
			'block_css'          => $this->get_block_css(),
			'block_classes'      => $this->get_block_classes(),
			'button_link'        => $atts['button_link'],
			'button_text'        => $atts['button_text'],
			'button_link_target' => $atts['button_link_target'],
			'image_url'          => $image_info['url'],
			'title'              => $atts['title'],
			'price'              => $atts['price'],
			'description'        => $atts['description']
		] );
	}
}
