<?php

use Enpii\Wp\EnpiiBase\Wp;

class td_module_single_custom extends td_module_single_base {

	private $text_domain = '';

	public function __construct($post, $module_atts = []) {
		parent::__construct($post);
		$this->text_domain = td_api_base::get_key( __CLASS__, 'text_domain' );
	}

    function get_comments() {
        $buffy = '';
        if ( td_util::get_option( 'tds_p_show_comments' ) != 'hide' ) {
            $buffy .= '<div class="td-post-comments">';
            $buffy .= '<a href="' . get_comments_link( $this->post->ID ) . '"><i class="fa fa-commenting" aria-hidden="true"></i>';
            $buffy .= get_comments_number( $this->post->ID );
            $buffy .= '</a>';
            $buffy .= '</div>';
        }

        return $buffy;
    }

    function get_rating() {
        $post_ratings_data = get_post_custom( $this->post->ID );
        $rating            = round( is_array( $post_ratings_data ) && array_key_exists( 'ratings_average', $post_ratings_data ) ? (float) $post_ratings_data['ratings_average'][0] : 0, 1 );

        $buffy = '<div class="td-post-rating">';
        $buffy .= '<i class="fa fa-star" aria-hidden="true"></i>';
        $buffy .= $rating . '<span class="rating-grey">/5</span>';
        $buffy .= '</div>';

        return $buffy;
    }

	function get_image($thumbType, $css_image = false) {
		global $page;

		if (td_api_single_template::_check_show_featured_image_on_all_pages(td_global::$cur_single_template) === false) {
			//the post is configured to show the featured image only on the first page
			if (!empty($page) and $page > 1) {
				return '';
			}
		}


		//do not show the featured image if the global setting is set to hide - show the video preview regardless of the setting
		if (td_util::get_option('tds_show_featured_image') == 'hide' and get_post_format($this->post->ID) != 'video') {
			return '';
		}

		//handle video post format
		if (get_post_format($this->post->ID) == 'video') {
			//if it's a video post...
			$td_post_video = td_util::get_post_meta_array($this->post->ID, 'td_post_video');

			//render the video if the post has a video in the featured video section of the post
			if (!empty($td_post_video['td_video'])) {
				return td_video_support::render_video($td_post_video['td_video']);
			}
		} else {
			//if it's a normal post, show the default thumb

			if (!is_null($this->post_thumb_id)) {
				//get the featured image id + full info about the 640px wide one
				$featured_image_id = get_post_thumbnail_id($this->post->ID);
				$featured_image_url = td_util::attachment_get_src($featured_image_id, 'td_696x0');

				return Wp::get_template_part( 'parts/featured_image_subscription_form', [
					'text_domain' => $this->text_domain,
					'image_url'   => $featured_image_url['src'],
					'width'       => $featured_image_url['width'],
					'height'      => $featured_image_url['height']
				]);
			} else {
				return ''; //the post has no thumb
			}
		}
	}

}