<?php
// Template 2 - post-final-2.psd - normal image + full title
//get the global sidebar position from td_single_template_vars.php

use Enpii\Wp\EnpiiBase\Wp;

$text_domain = \Enpii\Wp\EnpiiBase\Base\WpApp::instance()->get_wp_theme()->text_domain;

locate_template('includes/wp_booster/td_single_template_vars.php', true);

get_header();

global $loop_sidebar_position, $td_sidebar_position, $post;

$td_mod_single = new td_module_single_custom($post);

?>
<div class="td-main-content-wrap td-container-wrap single-wrapper">

    <div class="td-post-template-2">
        <article id="post-<?php echo $td_mod_single->post->ID;?>" class="<?php echo join(' ', get_post_class());?>" <?php echo $td_mod_single->get_item_scope();?>>
            <div class="tdc-row stretch_row_1200 td-stretch-content">
                <div class="td-block-row">
                    <div class="td-block-span12">
                        <div class="td-post-header">
                            <div class="td-crumb-container"><?php echo td_page_generator::get_single_breadcrumbs($td_mod_single->title); ?></div>
			                <?php echo $td_mod_single->get_category(); ?>
                        </div>
                    </div>
                    <div class="td-block-span8">
                        <div class="td-post-header">

                            <header class="td-post-title">
				                <?php echo $td_mod_single->get_title();?>


				                <?php if (!empty($td_mod_single->td_post_theme_settings['td_subtitle'])) { ?>
                                    <p class="td-post-sub-title"><?php echo $td_mod_single->td_post_theme_settings['td_subtitle'];?></p>
				                <?php } ?>


                                <div class="td-module-meta-info">
					                <?php if(is_plugin_active( 'selfhacked-credits/selfhacked-credits.php' )): ?>
						                <?php do_shortcode( '[selfhacked-credits id="' . get_the_ID() . '"]'); ?>
					                <?php else: ?>
						                <?php echo $td_mod_single->get_author();?>
					                <?php endif; ?>
					                <?php echo $td_mod_single->get_date(false);?>
                                </div>
                            </header>
                        </div>
                    </div>
                    <div class="td-block-span4 header-meta">
                        <div class="header-metabox">
			                <?php echo $td_mod_single->get_rating(); ?>
                        </div>
                        <div class="header-metabox">
			                <?php echo $td_mod_single->get_comments();?>
                        </div>
                    </div>
                    <div class="td-block-span12">
                        <div class="header-separator"></div>
                    </div>
                </div> <!-- /.td-pb-row -->
            </div>

            <div class="tdc-row stretch_row_1200 td-stretch-content">
                <div class="td-block-row">
                    <?php

                    //the default template
                    switch ($loop_sidebar_position) {
                        default:
                            ?>
                                <div class="td-block-span8 td-main-content" role="main">
                                    <div class="td-ss-main-content">
                                        <?php
                                        locate_template('loop-single-2.php', true);
                                        ?>
                                    </div>
                                </div>
                                <div class="td-block-span4 td-main-sidebar" role="complementary">
                                    <div class="td-ss-main-sidebar">
                                        <?php get_sidebar(); ?>
                                    </div>
                                </div>
                            <?php
                            break;

                        case 'sidebar_left':
                            ?>
                            <div class="td-block-span8 td-main-content <?php echo $td_sidebar_position; ?>-content" role="main">
                                <div class="td-ss-main-content">
                                    <?php
                                    locate_template('loop-single-2.php', true);
                                    ?>
                                </div>
                            </div>
                            <div class="td-block-span4 td-main-sidebar" role="complementary">
                                <div class="td-ss-main-sidebar">
                                    <?php get_sidebar(); ?>
                                </div>
                            </div>
                            <?php
                            break;

                        case 'no_sidebar':
                            td_global::$load_featured_img_from_template = 'td_1068x0';
                            ?>
                            <div class="td-block-span12 td-main-content" role="main">
                                <div class="td-ss-main-content">
                                    <?php
                                    locate_template('loop-single-2.php', true);
                                    ?>
                                </div>
                            </div>
                            <?php
                            break;

                    }
                    ?>
                </div> <!-- /.td-pb-row -->
            </div>
        </article> <!-- /.post -->
    </div> <!-- /.td-container -->
    <div class="comments-row">
        <div class="tdc-row stretch_row_1200 td-stretch-content">
            <div class="td-block-row">
			    <?php

			    //the default template
			    switch ($loop_sidebar_position) {
				    default:
					    ?>
                        <div class="td-block-span8">
						    <?php comments_template('', true); ?>
                        </div>
                        <div class="td-pb-span4"></div>
					    <?php
					    break;

				    case 'sidebar_left':
					    ?>
                        <div class="td-block-span8 <?php echo $td_sidebar_position; ?>-content">
						    <?php comments_template('', true); ?>
                        </div>
                        <div class="td-block-span4"></div>
					    <?php
					    break;

				    case 'no_sidebar':
					    td_global::$load_featured_img_from_template = 'td_1068x0';
					    ?>
                        <div class="td-block-span12 td-main-content">
						    <?php comments_template('', true); ?>
                        </div>
					    <?php
					    break;

			    }
			    ?>
            </div>
        </div>
    </div>
	<?php echo Wp::get_template_part( 'parts/shortcodes/checkout-selfdecode', [ 'text_domain' => $text_domain ] ); ?>
</div> <!-- /.td-main-content-wrap -->
    <div class="dislike-container">
        <form id="dislike-form" class="form-horizontal">
            <h3>Why did you dislike this article?</h3>
            <label>
                Science is not accurate
                <input type="checkbox" name="0">
                <span class="checkmark"></span>
            </label>
            <label>
                Not interesting
                <input type="checkbox" name="1">
                <span class="checkmark"></span>
            </label>
            <label>
                Not what I was looking for
                <input type="checkbox" name="2">
                <span class="checkmark"></span>
            </label>
            <label>
                Not written well
                <input type="checkbox" name="3">
                <span class="checkmark"></span>
            </label>
            <label>
                Not thorough
                <input type="checkbox" name="4">
                <span class="checkmark"></span>
            </label>
            <label>
                Grammar / Spelling
                <input type="checkbox" name="5">
                <span class="checkmark"></span>
            </label>
            <label>
                Too difficult to understand
                <input type="checkbox" name="6">
                <span class="checkmark"></span>
            </label>
            <label>
                Titles / Headings are misleading
                <input type="checkbox" name="7">
                <span class="checkmark"></span>
            </label>
            <label>
                Other
                <input id="other-checkbox" type="checkbox" name="8">
                <span class="checkmark"></span>
            </label>
            <input id="reason-input" type="text" class="form-control disabled reason hidden" placeholder="Reason">
            <button id="submit-dislike" type="submit" class="btn btn-default" data-value="<?php echo get_the_ID(); ?>">Submit</button>
            <i id="dislike-close" class="fa fa-window-close close-icon" aria-hidden="true"></i>
        </form>
    </div>
<?php
get_footer();