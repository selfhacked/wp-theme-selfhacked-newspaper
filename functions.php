<?php
/*  ----------------------------------------------------------------------------
    Newspaper V6.3+ Child theme - Please do not use this child theme with older versions of Newspaper Theme

    What can be overwritten via the child theme:
     - everything from /parts folder
     - all the loops (loop.php loop-single-1.php) etc
	 - please read the child theme documentation: http://forum.tagdiv.com/the-child-theme-support-tutorial/


     - the rest of the theme has to be modified via the theme api:
       http://forum.tagdiv.com/the-theme-api/

 */

use Enpii\Wp\EnpiiBase\Base\WpApp;

// Define the IDs for lists to be subscribed
defined( 'AC_SUBSCRIBE_LIST_IDS' ) || define( 'AC_SUBSCRIBE_LIST_IDS', env( 'AC_SUBSCRIBE_LIST_IDS' ) ?: 31 );

/* @var SelfHacked\SelfHackedNewspaper\Component\WpTheme $wp_theme */
$wp_theme = WpApp::instance()->get_wp_theme();
$wp_theme->initialize();
